//
//  Activity.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/11/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
// MARK: - ActivityElement

// MARK: - UserInfo
struct UserActivity: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription, dataa: String?
    var userData:Activity?
    
    enum CodingKeys: String, CodingKey {
        
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case userData = "Dataa"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode(Activity.self, from: data)
        userData = res
    }
}


// MARK: - ActivityElement
struct ActivityElement: Codable {
    let id: Int?
    let name: String?
    let activityTypeID,activityStatusID: Int?
    let fromDate, toDate: String?
    let timeoutID, productID: Int?
    let note: String?
    let isDeleted, remindMe: Bool?
    let activityType, timeout, product: ActivityType?
    let activityStatus: ActivityStatus?
    let entryUserID: Int?
    let entryDate: String?

    
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case activityTypeID = "ActivityTypeID"
        case fromDate = "FromDate"
        case toDate = "ToDate"
        case timeoutID = "TimeoutID"
        case productID = "ProductID"
        case note = "Note"
        case isDeleted = "IsDeleted"
        case activityStatusID = "ActivityStatusID"
        case remindMe = "RemindMe"
        case activityType = "ActivityType"
        case timeout = "Timeout"
        case product = "Product"
        case activityStatus = "ActivityStatus"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
    }
    //ActivityStatusID
    init(from decoder: Decoder) throws {
           let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String
        activityTypeID = try values.decodeIfPresent(Int?.self, forKey: .activityTypeID) as? Int
        fromDate = try values.decodeIfPresent(String?.self, forKey: .fromDate) as? String
        toDate = try values.decodeIfPresent(String?.self, forKey: .toDate) as? String
        timeoutID = try values.decodeIfPresent(Int?.self, forKey: .timeoutID) as? Int
        productID = try values.decodeIfPresent(Int?.self, forKey: .productID) as? Int
        note = try values.decodeIfPresent(String?.self, forKey: .note) as? String
        isDeleted = try values.decodeIfPresent(Bool?.self, forKey: .isDeleted) as? Bool
        activityStatusID = try values.decodeIfPresent(Int?.self, forKey: .activityStatusID) as? Int
        remindMe = try values.decodeIfPresent(Bool?.self, forKey: .remindMe) as? Bool
        activityType = try values.decodeIfPresent(ActivityType?.self, forKey: .activityType) as? ActivityType
        timeout = try values.decodeIfPresent(ActivityType?.self, forKey: .timeout) as? ActivityType
        product = try values.decodeIfPresent(ActivityType?.self, forKey: .product) as? ActivityType
        activityStatus = try values.decodeIfPresent(ActivityStatus?.self, forKey: .activityStatus) as? ActivityStatus
        entryUserID = try values.decodeIfPresent(Int?.self, forKey: .entryUserID) as? Int
        entryDate = try values.decodeIfPresent(String?.self, forKey: .entryDate) as? String

        
    }
    
}

// MARK: - ActivityStatus
struct ActivityStatus: Codable {
    let id: Int?
    let name: String?
    let isDeletable, isVisible: Bool?
    let color: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case isDeletable = "IsDeletable"
        case isVisible = "IsVisible"
        case color = "Color"
    }
    init(from decoder: Decoder) throws {
                 let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String
        isDeletable = try values.decodeIfPresent(Bool?.self, forKey: .isDeletable) as? Bool
        isVisible = try values.decodeIfPresent(Bool?.self, forKey: .isVisible) as? Bool
        color = try values.decodeIfPresent(String?.self, forKey: .color) as? String

        
    }
}

// MARK: - ActivityType
struct ActivityType: Codable {
    let id: Int?
    let name: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
    }
    init(from decoder: Decoder) throws {
              let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String

    }
    
}

typealias Activity = [ActivityElement]
