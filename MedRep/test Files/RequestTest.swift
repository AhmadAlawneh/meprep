//
//  ReuestTest.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/15/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
import UIKit

class Call: UIViewController {
    
    override func viewDidLoad() {
     
        let currentDateTime = Date()

       let dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        
        getDoctorsPlan(date: dateTime)
        getFiles(doctorID: "1 : this is doctor Id")
        getHistory(doctorID: "1 : this is doctor Id")
        getToDo(doctorID: "1 : this is doctor Id")
        getSurvey(doctorID: "1 : this is doctor Id")


    }
    
   func getDoctorsPlan(date:String) {
          
          
          let parameters = [date]
              Helper.showIndicator(view: self.view)
          let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorAssignedToMe)
          let endPoint = RequestHandler.get(url: url)
          APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanOfActionDoctors>) in
              
              guard let strongSelf = self else {return}
              DispatchQueue.main.async {
                      Helper.hideIndicator(view: strongSelf.view)
                  switch response.result {
                      
                  case .failure(let error):
                      print(error.localizedDescription)
                      
                  case .success(let value):
                      print(value)
              
                      
                  }
              }
              
              
          }
          
      }
    
    func getFiles(doctorID:String) {
           
           
           let parameters = [doctorID]
               Helper.showIndicator(view: self.view)
           let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorFiles)
           let endPoint = RequestHandler.get(url: url)
           APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanFilesModel>) in
               
               guard let strongSelf = self else {return}
               DispatchQueue.main.async {
                       Helper.hideIndicator(view: strongSelf.view)
                   switch response.result {
                       
                   case .failure(let error):
                       print(error.localizedDescription)
                       
                   case .success(let value):
                       print(value)
               
                       
                   }
               }
               
               
           }
           
       }
    
    func getHistory(doctorID:String) {
           
           
           let parameters = [doctorID]
               Helper.showIndicator(view: self.view)
           let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorActivityHistory)
           let endPoint = RequestHandler.get(url: url)
           APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanHistoryModel>) in
               
               guard let strongSelf = self else {return}
               DispatchQueue.main.async {
                       Helper.hideIndicator(view: strongSelf.view)
                   switch response.result {
                       
                   case .failure(let error):
                       print(error.localizedDescription)
                       
                   case .success(let value):
                       print(value)
               
                       
                   }
               }
               
               
           }
           
       }
    
    func getToDo(doctorID:String) {
           
           
           let parameters = [doctorID]
               Helper.showIndicator(view: self.view)
           let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorTodo)
           let endPoint = RequestHandler.get(url: url)
           APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanToDoModel>) in
               
               guard let strongSelf = self else {return}
               DispatchQueue.main.async {
                       Helper.hideIndicator(view: strongSelf.view)
                   switch response.result {
                       
                   case .failure(let error):
                       print(error.localizedDescription)
                       
                   case .success(let value):
                       print(value)
               
                       
                   }
               }
               
               
           }
           
       }
    
    
    func getSurvey(doctorID:String) {
           
           
           let parameters = [doctorID]
               Helper.showIndicator(view: self.view)
           let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorTodo)
           let endPoint = RequestHandler.get(url: url)
           APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanSurveyModel>) in
               
               guard let strongSelf = self else {return}
               DispatchQueue.main.async {
                       Helper.hideIndicator(view: strongSelf.view)
                   switch response.result {
                       
                   case .failure(let error):
                       print(error.localizedDescription)
                       
                   case .success(let value):
                       print(value)
               
                       
                   }
               }
               
               
           }
           
       }
    
}
