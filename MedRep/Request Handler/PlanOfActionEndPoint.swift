//
//  PlanOfActionEndPoint.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/18/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJWT
import SwiftyCrypto

enum PlanOfActionEndPoint : URLRequestConvertible {

    case getClassification
    // MARK: - HTTPMethod
    
    var method: HTTPMethod {
        switch self {
        case .getClassification:
            return .get
        }
    }
    
    // MARK: - Path
    
    var path: String {
        switch self {
        case .getClassification:
            return "PlanOfAction/GetClassifications"
        }
    }
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try URLs.BaseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getClassification:
            if let jwtToken = LongTermManager.getJwtToken() {
               urlRequest.addValue(jwtToken, forHTTPHeaderField: "Authentication")
            }
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        return urlRequest
    }
}

