//
//  InsertNoteEndPoint.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/22/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJWT
import SwiftyCrypto

enum InsertNoteEndPoint : URLRequestConvertible {

    case addNote(headerParams: [String:Any])
    // MARK: - HTTPMethod
    
    var method: HTTPMethod {
        switch self {
        case .addNote:
            return .post
        }
    }
    
    // MARK: - Path
    
    var path: String {
        switch self {
        case .addNote:
            return "PlanOfAction/InsertDoctorTodo"
        }
    }
    
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try URLs.BaseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .addNote(headerParams: let params):
            if let jwtToken = LongTermManager.getJwtToken() {
               urlRequest.addValue(jwtToken, forHTTPHeaderField: "Authentication")
            }
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        return urlRequest
    }
}
