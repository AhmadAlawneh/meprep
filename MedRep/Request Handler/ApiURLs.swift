//
//  ApiURLs.swift
//  EMR_Swift
//
//  Created by Ahmad Alawneh on 3/15/20.
//  Copyright © 2020 ATS. All rights reserved.
//

import Foundation
struct ApiURLs {
    // MARK: App URLS

    static let domain = "http://clexan.animiboard.com/utility/api/" //Plive

//    static let domain = "http://animiboard.com/utility/api/" //Zlive
//    static let domain = "http://beta.animiboard.com/utility/api/" //Ztesting
    static let userAuth = ""
    static let passAuth = ""
    static let login = "Authentication/Login"
    static let GetUserActivities = "Activity/GetUserActivities/"
    static let GetActivityType = "Lookup/GetActivityType"
    static let GetTimeout = "Lookup/GetTimeout"
    static let GetProduct = "Lookup/GetProduct"
    static let InsertUserActivity = "Activity/InsertUserActivity"
    static let GetDoctorAssignedToMe = "PlanOfAction/GetDoctorAssignedToMeByClassification/"
    static let GetDoctorFiles = "PlanOfAction/GetDoctorFiles/"
    static let GetDoctorActivityHistory = "PlanOfAction/GetDoctorActivityHistory/"
    static let GetDoctorTodo = "PlanOfAction/GetDoctorTodo/"
    static let GetSurveysUnCompleted = "PlanOfAction/GetSurveysUnCompleted/"
    static let Getcycle = "Lookup/Getcycle"
    static let Getchannel = "Lookup/Getchannel"
    static let GetAccompaniedBy = "Lookup/GetAccompaniedBy"
    static let InsertDoctorVisitFile = "PlanOfAction/InsertDoctorVisitFile"
    static let InsertDoctorVisit = "PlanOfAction/InsertDoctorVisit"
    static let GetIncompleteCalls = "IncompleteCalls/GetIncompleteCalls"
    static let DeleteDoctorVisit = "PlanOfAction/DeleteDoctorVisit/"
    static let UpdateDoctorVisit = "PlanOfAction/UpdateDoctorVisit"
    static let GetDoctors = "PlanOfAction/GetDoctors/"
    static let GetMajor = "Lookup/GetMajor"
    static let GetCity = "Lookup/GetCity"
    static let GetDoctorsByFilter = "PlanOfAction/GetDoctorsByFilter/"


//api/PlanOfAction/GetDoctorsByFilter/{strDate}/{MajorID}/{CityID}/{IsCPA}/{MaximumID}/{RecordsCount}/{DoctorName}
}

/*
 http://MedRepAPI.somee.com/api/Lookup/GetMajor

 {"IsSuccess":true,"StatusCode":0,"StatusDescription":"Success","Data":"[{\"ID\":1,\"Name\":\"Major Alias\"}]"}

 http://MedRepAPI.somee.com/api/Lookup/GetCity

 {"IsSuccess":true,"StatusCode":0,"StatusDescription":"Success","Data":"[{\"ID\":1,\"Name\":\"Amman Alias\"}]"}

 */
struct URLs {
    static let BaseURL = "http://clexan.animiboard.com/utility/api/" //Plive

//    static let BaseURL = "http://animiboard.com/utility/api/" //Zlive
    
//    static let BaseURL = "http://beta.animiboard.com/utility/api/" //Ztesting
}

