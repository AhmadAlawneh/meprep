//
//  RequestHandler.swift
//  EMR_Swift
//
//  Created by Ahmad Alawneh on 3/15/20.
//  Copyright © 2020 ATS. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJWT
import SwiftyCrypto

enum RequestHandler: URLRequestConvertible {
    
    //MARK:- Helping Method

    
    
    case get(url:String)
    case post(parmeters:[String:Any],url:String)
    
    // MARK: - HTTPMethod
    
    var method: HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        }
        
    }
    
    // MARK: - Path
    
    var path: String {
        switch self {
        case .get(let url):
            return url
            
        case .post( _, let url):
            return url
        }
    }
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: ApiURLs.domain)!
  
  

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
 
        
        switch self {
            
        case .get(url: _):
            urlRequest.addValue(LongTermManager.getJwtToken() ?? "", forHTTPHeaderField: "Authentication")
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            return urlRequest
        case .post(let parmeters, _):
            urlRequest.httpMethod = "POST"
            urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: parmeters, options: .prettyPrinted)
            urlRequest.addValue(LongTermManager.getJwtToken() ?? "", forHTTPHeaderField: "Authentication")
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            return urlRequest
        }
    }
}

extension StringProtocol {
    var data: Data { .init(utf8) }
    var bytes: [UInt8] { .init(utf8) }
}

extension Data {
    func hex(separator:String = "") -> String {
        return (self.map { String(format: "%02X", $0) }).joined(separator: separator)
    }
}
