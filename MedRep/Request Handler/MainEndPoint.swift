//
//  MainEndPoint.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/12/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJWT
import SwiftyCrypto

enum MainEndPoint : URLRequestConvertible {

    case currentMonthVisits
    case userCoverage
    // MARK: - HTTPMethod
    
    var method: HTTPMethod {
        switch self {
        case .currentMonthVisits,.userCoverage:
            return .get
        }
    }
    
    // MARK: - Path
    
    var path: String {
        switch self {
        case .currentMonthVisits:
            return "Home/GetCurrentMonthVisits"
        case .userCoverage:
            return "Home/GetUserCoverage"
        }
    }
    
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try URLs.BaseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .currentMonthVisits, .userCoverage:
            if let jwtToken = LongTermManager.getJwtToken() {
               urlRequest.addValue(jwtToken, forHTTPHeaderField: "Authentication")
            }
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        return urlRequest
    }
}
