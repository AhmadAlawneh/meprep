//
//  LoginEndPoint.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/10/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJWT
import SwiftyCrypto

enum LoginEndPoint : URLRequestConvertible {

    case login(headerParams: [String:Any])
    
    // MARK: - HTTPMethod
    
    var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        }
    }
    
    // MARK: - Path
    
    var path: String {
        switch self {
        case .login:
            return "Authentication/Login"
        }
    }
    
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try URLs.BaseURL.asURL()
        
        let alg = JWTAlgorithm.hs256("401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1")
        let headerWithKeyId = JWTHeader.init(keyId: "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1")

        var payload = JWTPayload()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {

            
        case .login(headerParams: let params):
            
            urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            let jsonData = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            
            let jsonString = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            
            payload.customFields = ["Authentication": EncodableValue(value: jsonString)]
            
            
            let jwtWithKeyId =  JWT.init(payload: payload, algorithm: alg, header: headerWithKeyId)
            let jwtToken = (jwtWithKeyId?.rawString ?? "")
            LongTermManager.setJwtToken(jwtToken: jwtToken, key: Keys.jwtTokenFromLogin)
//            urlRequest.httpBody = jsonData
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue(jwtToken, forHTTPHeaderField: "Authentication")
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        return urlRequest
    }
}

//urlRequest.httpMethod = "POST"
//urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: parmeters, options: .prettyPrinted)
//let jsonData = try JSONSerialization.data(withJSONObject: parmeters, options: .prettyPrinted)
//let jsonString = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
//
//// here "jsonData" is the dictionary encoded in JSON data
//payload.customFields = ["Authentication": EncodableValue(value: jsonString)]
//let jwtWithKeyId =  JWT.init(payload: payload, algorithm: alg, header: headerWithKeyId)
//let jwtToken = (jwtWithKeyId?.rawString ?? "")
//urlRequest.addValue(jwtToken, forHTTPHeaderField: "Authentication")
//urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
//return urlRequest
