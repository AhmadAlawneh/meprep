//
//  ChangeRequestEndPoint.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/25/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJWT
import SwiftyCrypto


//    http://med.animiboard.com/Utility/api/Lookup/GetMajor
//    http://med.animiboard.com/Utility/api/Lookup/GetCity


enum ChangeRequestEndPoint : URLRequestConvertible {
    
    case addDoctor(headerParams: [String:Any])
    case getMajor
    case getCity
    case getClassification
    // MARK: - HTTPMethod
    
    var method: HTTPMethod {
        switch self {
        case .addDoctor:
            return .post
        case .getMajor,.getCity,.getClassification:
            return .get
        }
    }
    
    // MARK: - Path
    
    var path: String {
        switch self {
        case .addDoctor:
            return "ChangeRequest/InsertChangeRequest"
        case .getMajor:
            return "Lookup/GetMajor"
        case .getCity:
            return "Lookup/GetCity"
        case .getClassification:
            return "Lookup/GetClassification"
        }
    }

    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try URLs.BaseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .addDoctor(headerParams: let params):
            if let jwtToken = LongTermManager.getJwtToken() {
                urlRequest.addValue(jwtToken, forHTTPHeaderField: "Authentication")
            }
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
            
        case .getMajor, .getCity ,.getClassification:
            if let jwtToken = LongTermManager.getJwtToken() {
                urlRequest.addValue(jwtToken, forHTTPHeaderField: "Authentication")
            }
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        
        }
        return urlRequest
    }
}

