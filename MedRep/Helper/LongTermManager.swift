//
//  LongTermManager.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/11/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
import UIKit

struct Keys {
    static let userInfo = "userInfo"
    static let jwtTokenFromLogin = "jwtTokenFromLogin"
}

struct LongTermManager {
    
    // save user Info
    
    static func setUserInfo(userObject: UserInfo? , key: String){
        if let data = try? JSONEncoder().encode(userObject) {
            UserDefaults.standard.set(data, forKey: key)
        }
    }

    // get User Info
    
    static func getUserInfo() -> UserInfo? {
        if let data = UserDefaults.standard.value(forKey: Keys.userInfo) as? Data,
            let configuration = try? JSONDecoder().decode(UserInfo.self, from: data) {
            return configuration
        }
        return nil
    }
    
    
    static func setJwtToken(jwtToken: String , key: String) {
        UserDefaults.standard.set(jwtToken, forKey: "jwtTokenFromLogin")
    }
    
    
    static func getJwtToken() -> String? {
        if let jwtToken = UserDefaults.standard.string(forKey: Keys.jwtTokenFromLogin) {
            return jwtToken
        }
        return nil
    }
    
}
