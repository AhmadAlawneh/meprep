//
//  UserInfo.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/8/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - UserInfo
struct UserInfo: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription, dataa: String?
    var userData:PurpleData?
    
    enum CodingKeys: String, CodingKey {
        
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case userData = "Dataa"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode(PurpleData.self, from: data)
        userData = res
    }
}


// MARK: - PurpleData

struct PurpleData: Codable {
    let id: Int?
    let username, name, mobileNo, email: String?
    let address: String?
    let gender: Gender?
    let imgrURL: String?
    let functionUser: [FunctionUser]?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case username = "Username"
        case name = "Name"
        case mobileNo = "MobileNo"
        case email = "Email"
        case address = "Address"
        case gender = "Gender"
        case imgrURL = "ImgrUrl"
        case functionUser = "FunctionUser"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        username = try values.decodeIfPresent(String?.self, forKey: .username) as? String
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String
        mobileNo = try values.decodeIfPresent(String?.self, forKey: .mobileNo) as? String
        email = try values.decodeIfPresent(String?.self, forKey: .email) as? String
        address = try values.decodeIfPresent(String?.self, forKey: .address) as? String
        gender = try values.decodeIfPresent(Gender?.self, forKey: .gender) as? Gender
        imgrURL = try values.decodeIfPresent(String?.self, forKey: .imgrURL) as? String
        functionUser = try values.decodeIfPresent([FunctionUser]?.self, forKey: .functionUser) as? [FunctionUser]
    }
    
}

// MARK: - FunctionUser
struct FunctionUser: Codable {
    
    let id, userID, functionID: Int?
    let enabled: Bool?
    let user: String?
    let function: Gender?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case userID = "UserID"
        case functionID = "FunctionID"
        case enabled = "Enabled"
        case user = "User"
        case function = "Function"
    }
}

// MARK: - Gender

struct Gender: Codable {
    let id: Int?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
    }
}
