//
//  GetCityModel.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/25/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - GetCity
struct GetCity: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription: String?
    let dataa: String?
    let cityData: [CityList]?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([CityList].self, from: data)
        cityData = res
        
    }
}

// MARK: - Datum
struct CityList: Codable {
    
    let id: Int?
    let name: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String
    }
}
