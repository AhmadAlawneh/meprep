//
//  DoctorListModel.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/26/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - GetCity
struct DoctorInfo: Codable {
    
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription: String?
    let dataa: String?
    let doctorData: [DoctorList]?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([DoctorList].self, from: data)
        doctorData = res
        
    }
    
    
}

// MARK: - Datum
struct DoctorList: Codable {
    let id: Int?
    let nameFirst, nameSecond, nameThird, nameLast: String?
    let mobileNo, identityID: String?
    let majorID, cityID, classificationID, visitsCount: Int?
    let imgURL: String?
    let isDeleted: Bool?
    let entryUserID: Int?
    let entryDate, modifyUserID, modifyDate: String?
    let city: City?
    let classification: Classification?
    let major: City?
    let entryUser, modifyUser: String?
    let completedVisitsCount: Int?
    let doctorNote: String?
    let doctorEmail: String?
    let doctorFiles: [String]?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case nameFirst = "NameFirst"
        case nameSecond = "NameSecond"
        case nameThird = "NameThird"
        case nameLast = "NameLast"
        case mobileNo = "MobileNo"
        case identityID = "IdentityID"
        case majorID = "MajorID"
        case cityID = "CityID"
        case classificationID = "ClassificationID"
        case visitsCount = "VisitsCount"
        case imgURL = "ImgUrl"
        case isDeleted = "IsDeleted"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
        case modifyUserID = "ModifyUserID"
        case modifyDate = "ModifyDate"
        case city = "City"
        case classification = "Classification"
        case major = "Major"
        case entryUser = "EntryUser"
        case modifyUser = "ModifyUser"
        case completedVisitsCount = "CompletedVisitsCount"
        case doctorFiles = "DoctorFiles"
        case doctorNote = "Note"
        case doctorEmail = "Email"
    }
    
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
            nameFirst = try values.decodeIfPresent(String?.self, forKey: .nameFirst) as? String
            nameSecond = try values.decodeIfPresent(String?.self, forKey: .nameSecond) as? String
            nameThird = try values.decodeIfPresent(String?.self, forKey: .nameThird) as? String
            nameLast = try values.decodeIfPresent(String?.self, forKey: .nameLast) as? String
            mobileNo = try values.decodeIfPresent(String?.self, forKey: .mobileNo) as? String
            identityID = try values.decodeIfPresent(String?.self, forKey: .identityID) as? String
            majorID = try values.decodeIfPresent(Int?.self, forKey: .majorID) as? Int
            cityID = try values.decodeIfPresent(Int?.self, forKey: .cityID) as? Int
            classificationID = try values.decodeIfPresent(Int?.self, forKey: .classificationID) as? Int
            visitsCount = try values.decodeIfPresent(Int?.self, forKey: .visitsCount) as? Int
            imgURL = try values.decodeIfPresent(String?.self, forKey: .imgURL) as? String
            isDeleted = try values.decodeIfPresent(Bool?.self, forKey: .isDeleted) as? Bool
            entryUserID = try values.decodeIfPresent(Int?.self, forKey: .entryUserID) as? Int
            entryDate = try values.decodeIfPresent(String?.self, forKey: .entryDate) as? String
            modifyUserID = try values.decodeIfPresent(String?.self, forKey: .modifyUserID) as? String
            modifyDate = try values.decodeIfPresent(String?.self, forKey: .modifyDate) as? String
            city = try values.decodeIfPresent(City?.self, forKey: .city) as? City
            classification = try values.decodeIfPresent(Classification?.self, forKey: .classification) as? Classification
            major = try values.decodeIfPresent(City?.self, forKey: .major) as? City
            entryUser = try values.decodeIfPresent(String?.self, forKey: .entryUser) as? String
            modifyUser = try values.decodeIfPresent(String?.self, forKey: .modifyUser) as? String
            completedVisitsCount = try values.decodeIfPresent(Int?.self, forKey: .completedVisitsCount) as? Int
            doctorFiles = try values.decodeIfPresent([String]?.self, forKey: .doctorFiles) as? [String]
            doctorNote = try values.decodeIfPresent(String?.self, forKey: .doctorNote) as? String
            doctorEmail = try values.decodeIfPresent(String?.self, forKey: .doctorEmail) as? String
        }
    
    
}

//// MARK: - City
//struct City: Codable {
//    let id: Int?
//    let name: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id = "ID"
//        case name = "Name"
//    }
//}
//
//// MARK: - Classification
//struct Classification: Codable {
//    let id: Int?
//    let name: String?
//    let visitsCount: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case id = "ID"
//        case name = "Name"
//        case visitsCount = "VisitsCount"
//    }
//}
