//
//  DoctorFilterHome.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 10/3/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
// MARK: - GetClassification
struct DoctorFilterHome: Codable {
    
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription: String?
    let dataa:String?
    var doctorsFilterData: [PDoctor]?
    
    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case doctorsFilterData = "Dataa"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([PDoctor].self, from: data)
        doctorsFilterData = res
        
    }
    
}
