//
//  AddDoctoerModel.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/25/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - AddDoctoer

struct AddDoctoer: Codable {
    
    let id: Int?
    let changeRequestTypeID: Int?
    let changeRequestStatusID: Int?
    let senderUserID: Int?
    let receiverUserID:Int?
    let doctorID: Int?
    let nameFirst: String?
    let nameSecond: String?
    let nameThird: String?
    let nameLast: String?
    let identityID: String?
    let majorID: Int?
    let cityID: Int?
    let classificationID: Int?
    let visitsCount: Int?
    let imgURL: String?
    let username: String?
    let name: String?
    let password: String?
    let mobileNo, email, address, startDate: String?
    let endDate, representativeCount, userGroupID, genderID: String?
    let isActive, parentUserID, newRepresentativeCount, fromDate: String?
    let toDate: String?
    let entryUserID: Int?
    let entryDate, modifyUserID, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case changeRequestTypeID = "ChangeRequestTypeID"
        case changeRequestStatusID = "ChangeRequestStatusID"
        case senderUserID = "SenderUserID"
        case receiverUserID = "ReceiverUserID"
        case doctorID = "DoctorID"
        case nameFirst = "NameFirst"
        case nameSecond = "NameSecond"
        case nameThird = "NameThird"
        case nameLast = "NameLast"
        case identityID = "IdentityID"
        case majorID = "MajorID"
        case cityID = "CityID"
        case classificationID = "ClassificationID"
        case visitsCount = "VisitsCount"
        case imgURL = "ImgUrl"
        case username = "Username"
        case name = "Name"
        case password = "Password"
        case mobileNo = "MobileNo"
        case email = "Email"
        case address = "Address"
        case startDate = "StartDate"
        case endDate = "EndDate"
        case representativeCount = "RepresentativeCount"
        case userGroupID = "UserGroupID"
        case genderID = "GenderID"
        case isActive = "IsActive"
        case parentUserID = "ParentUserID"
        case newRepresentativeCount = "NewRepresentativeCount"
        case fromDate = "FromDate"
        case toDate = "ToDate"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
        case modifyUserID = "ModifyUserID"
        case modifyDate = "ModifyDate"
    }
}
