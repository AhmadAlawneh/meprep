//
//  InsertVisit.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/21/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
// MARK: - InsertVisit
struct InsertVisit: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription, data: String?
    
    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case data = "Data"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        data = try values.decodeIfPresent(String?.self, forKey: .data) as? String
        
    }
    
}
/*
 {
 "IsSuccess": true,
 "StatusCode": 0,
 "StatusDescription": "Success",
 "Data": "14"
 }
 */
