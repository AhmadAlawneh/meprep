//
//  IncompleteCalls.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/21/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
// MARK: - IncompleteCall
struct InCalles: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription, dataa: String?
    var incompleteCalls:IncompleteCalls?
    
    enum CodingKeys: String, CodingKey {
        
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case incompleteCalls = "Dataa"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode(IncompleteCalls.self, from: data)
        incompleteCalls = res
    }
}







// MARK: - IncompleteCall
struct IncompleteCall: Codable {
    let visitDate: String?
    let doctorVisits: [DoctorVisit]?
    
    enum CodingKeys: String, CodingKey {
        case visitDate = "VisitDate"
        case doctorVisits = "DoctorVisits"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        visitDate = try values.decodeIfPresent(String?.self, forKey: .visitDate) as? String
        doctorVisits = try values.decodeIfPresent([DoctorVisit]?.self, forKey: .doctorVisits) as? [DoctorVisit]
        
    }
}
/*
 let id, doctorID, accompaniedByID, representativeUserID: Int?
 let fromDate, toDate, address, note: String?
 let productID, visitStatusID: Int?
 let isDeleted: Bool?
 let cycleID, entryUserID: Int?
 let entryDate: String?
 let modifyUserID, modifyDate: JSONNull?
 let doctor: Doctor?
 let visitStatus: VisitStatus?
 let product, cycle, accompaniedBy, representativeUser: AccompaniedBy?

 enum CodingKeys: String, CodingKey {
     case id
     case doctorID
     case accompaniedByID
     case representativeUserID
     case fromDate
     case toDate
     case address
     case note
     case productID
     case visitStatusID
     case isDeleted
     case cycleID
     case entryUserID
     case entryDate
     case modifyUserID
     case modifyDate
     case doctor
     case visitStatus
     case product
     case cycle
     case accompaniedBy
     case representativeUser
 }
 */

// MARK: - DoctorVisit
struct DoctorVisit: Codable {
  let id, doctorID, accompaniedByID, representativeUserID: Int?
    let fromDate, toDate, address, note: String?
    let productID, channelID,visitStatusID: Int?
    let isDeleted: Bool?
    let cycleID, entryUserID: Int?
    let entryDate: String?
    let modifyUserID:Int?
    let modifyDate: String?
    let doctor: Doctor?
    let visitStatus: VisitStatus?
    let product, cycle, accompaniedBy,channel, representativeUser: AccompaniedBy?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case doctorID = "DoctorID"
        case accompaniedByID = "AccompaniedByID"
        case representativeUserID = "RepresentativeUserID"
        case fromDate = "FromDate"
        case toDate = "ToDate"
        case address = "Address"
        case note = "Note"
        case productID = "ProductID"
        case channelID = "ChannelID"
        case visitStatusID = "VisitStatusID"
        case isDeleted = "IsDeleted"
        case cycleID = "CycleID"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
        case modifyUserID = "ModifyUserID"
        case modifyDate = "ModifyDate"
        case doctor = "Doctor"
        case visitStatus = "VisitStatus"
        case cycle = "Cycle"
        case accompaniedBy = "AccompaniedBy"
        case product = "Product"
        case channel = "Channel"
        case representativeUser = "RepresentativeUser"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        doctorID = try values.decodeIfPresent(Int?.self, forKey: .doctorID) as? Int
        accompaniedByID = try values.decodeIfPresent(Int?.self, forKey: .accompaniedByID) as? Int
        fromDate = try values.decodeIfPresent(String?.self, forKey: .fromDate) as? String
        toDate = try values.decodeIfPresent(String?.self, forKey: .toDate) as? String
        address = try values.decodeIfPresent(String?.self, forKey: .address) as? String
        note = try values.decodeIfPresent(String?.self, forKey: .note) as? String
        visitStatusID = try values.decodeIfPresent(Int?.self, forKey: .visitStatusID) as? Int
        isDeleted = try values.decodeIfPresent(Bool?.self, forKey: .isDeleted) as? Bool
        cycleID = try values.decodeIfPresent(Int?.self, forKey: .cycleID) as? Int
        entryUserID = try values.decodeIfPresent(Int?.self, forKey: .entryUserID) as? Int
        entryDate = try values.decodeIfPresent(String?.self, forKey: .entryDate) as? String
        modifyUserID = try values.decodeIfPresent(Int?.self, forKey: .modifyUserID) as? Int
        modifyDate = try values.decodeIfPresent(String?.self, forKey: .modifyDate) as? String
        doctor = try values.decodeIfPresent(Doctor?.self, forKey: .doctor) as? Doctor
        visitStatus = try values.decodeIfPresent(VisitStatus?.self, forKey: .visitStatus) as? VisitStatus
        cycle = try values.decodeIfPresent(AccompaniedBy?.self, forKey: .cycle) as? AccompaniedBy
        accompaniedBy = try values.decodeIfPresent(AccompaniedBy?.self, forKey: .accompaniedBy) as? AccompaniedBy
        product =  try values.decodeIfPresent(AccompaniedBy?.self, forKey: .product) as? AccompaniedBy
        representativeUser =  try values.decodeIfPresent(AccompaniedBy?.self, forKey: .representativeUser) as? AccompaniedBy
        channel =  try values.decodeIfPresent(AccompaniedBy?.self, forKey: .channel) as? AccompaniedBy
        representativeUserID = try values.decodeIfPresent(Int?.self, forKey: .representativeUserID) as? Int
        productID = try values.decodeIfPresent(Int?.self, forKey: .productID) as? Int
        channelID = try values.decodeIfPresent(Int?.self, forKey: .channelID) as? Int

        
    }
}

// MARK: - AccompaniedBy
struct AccompaniedBy: Codable {
    let id: Int?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String

    }
}


typealias IncompleteCalls = [IncompleteCall]
