//
//  PlanHistoryModel.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/15/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
// MARK: - PlanOfActionHistory
struct PlanHistoryModel: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription : String?
    let dataa: String?
    var planHistory:[HistoryElement]?
    
    enum CodingKeys: String, CodingKey {
        
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case planHistory = "Dataa"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([HistoryElement].self, from: data)
        planHistory = res
    }
}

// MARK: - HistoryElement
struct HistoryElement: Codable {
    let id, doctorID, managerUserID: Int?
    let fromDate, toDate, address, note: String?
    let visitStatusID: Int?
    let isDeleted: Bool?
    let cycleID, entryUserID: Int?
    let entryDate: String?
    let modifyUserID: Int?
    let modifyDate: String?
    let doctor: Doctor?
    let visitStatus: VisitStatus?
    let cycle: Cycle?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case doctorID = "DoctorID"
        case managerUserID = "ManagerUserID"
        case fromDate = "FromDate"
        case toDate = "ToDate"
        case address = "Address"
        case note = "Note"
        case visitStatusID = "VisitStatusID"
        case isDeleted = "IsDeleted"
        case cycleID = "CycleID"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
        case modifyUserID = "ModifyUserID"
        case modifyDate = "ModifyDate"
        case doctor = "Doctor"
        case visitStatus = "VisitStatus"
        case cycle = "Cycle"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
    doctorID = try values.decodeIfPresent(Int?.self, forKey: .doctorID) as? Int
        managerUserID = try values.decodeIfPresent(Int?.self, forKey: .managerUserID) as? Int
        fromDate = try values.decodeIfPresent(String?.self, forKey: .fromDate) as? String
        toDate = try values.decodeIfPresent(String?.self, forKey: .toDate) as? String
        address = try values.decodeIfPresent(String?.self, forKey: .address) as? String
        note = try values.decodeIfPresent(String?.self, forKey: .note) as? String
        visitStatusID = try values.decodeIfPresent(Int?.self, forKey: .visitStatusID) as? Int
        isDeleted = try values.decodeIfPresent(Bool?.self, forKey: .isDeleted) as? Bool
        cycleID = try values.decodeIfPresent(Int?.self, forKey: .cycleID) as? Int
        entryUserID = try values.decodeIfPresent(Int?.self, forKey: .entryUserID) as? Int
        entryDate = try values.decodeIfPresent(String?.self, forKey: .entryDate) as? String
        modifyUserID = try values.decodeIfPresent(Int?.self, forKey: .modifyUserID) as? Int
        modifyDate = try values.decodeIfPresent(String?.self, forKey: .modifyDate) as? String
        doctor = try values.decodeIfPresent(Doctor?.self, forKey: .doctor) as? Doctor
        visitStatus = try values.decodeIfPresent(VisitStatus?.self, forKey: .visitStatus) as? VisitStatus
        cycle = try values.decodeIfPresent(Cycle?.self, forKey: .cycle) as? Cycle

    }
}




typealias History = [HistoryElement]
