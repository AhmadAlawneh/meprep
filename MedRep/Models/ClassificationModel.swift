//
//  ClassificationModel.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/18/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - Classification

struct ClassificationModel: Codable {
    
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription: String?
    let strData: String?
    let classificationData: [ClassificationData]?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case strData = "Data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        strData = try values.decodeIfPresent(String?.self, forKey: .strData) as? String
        let data = strData?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([ClassificationData].self, from: data)
        classificationData = res
    }
}


// MARK: - Classification Data

struct ClassificationData: Codable {
    
    let id: Int?
    let name: String?
    let visitsCount: Int?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case visitsCount = "VisitsCount"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String
        visitsCount = try values.decodeIfPresent(Int?.self, forKey: .visitsCount) as? Int
        
    }
}
