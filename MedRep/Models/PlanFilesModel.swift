//
//  PlanFilesModel.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/15/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - PlanOfActionFiles
struct PlanFilesModel: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription, dataa: String?
    var planFiles:PlanFiles?
    
    enum CodingKeys: String, CodingKey {
        
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case planFiles = "Dataa"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode(PlanFiles.self, from: data)
        planFiles = res
    }
}

// MARK: - PlanFile
struct PlanFile: Codable {
    let id, doctorID, fileID: Int?
    let file: File?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case doctorID = "DoctorID"
        case fileID = "FileID"
        case file = "File"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        doctorID = try values.decodeIfPresent(Int?.self, forKey: .doctorID) as? Int
        fileID = try values.decodeIfPresent(Int?.self, forKey: .fileID) as? Int
        file = try values.decodeIfPresent(File?.self, forKey: .file) as? File
        
    }
}

// MARK: - File
struct File: Codable {
    let id: Int?
    let name: String?
    let url: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case url = "Url"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String
        url = try values.decodeIfPresent(String?.self, forKey: .url) as? String
        
    }
}

typealias PlanFiles = [PlanFile]
