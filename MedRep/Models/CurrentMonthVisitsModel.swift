//
//  CurrentMonthVisitsModel.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/13/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
import UIKit

// MARK: - CurrentMonthVisits
struct CurrentMonthVisits: Codable {
    
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription: String?
    let dataa: String?
    let currentVisits:[Datum]?
    
    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case currentVisits = "DataC"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([Datum].self, from: data)
        currentVisits = res
        
    }
    
}

// MARK: - Datum
struct Datum: Codable {
    let id, doctorID, managerUserID: Int?
    let fromDate, toDate, address, note: String?
    let visitStatusID: Int?
    let isDeleted: Bool?
    let cycleID, entryUserID: Int?
    let entryDate: String?
    let modifyUserID: Int?
    let modifyDate: String?
    let doctor: Doctor?
    let visitStatus: VisitStatus?
    let cycle: Cycle?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case doctorID = "DoctorID"
        case managerUserID = "ManagerUserID"
        case fromDate = "FromDate"
        case toDate = "ToDate"
        case address = "Address"
        case note = "Note"
        case visitStatusID = "VisitStatusID"
        case isDeleted = "IsDeleted"
        case cycleID = "CycleID"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
        case modifyUserID = "ModifyUserID"
        case modifyDate = "ModifyDate"
        case doctor = "Doctor"
        case visitStatus = "VisitStatus"
        case cycle = "Cycle"
    }
    
    init(from decoder: Decoder) throws {
        
//        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.id) as? Int
        doctorID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.doctorID) as? Int
        managerUserID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.managerUserID) as? Int
        fromDate = try container.decodeIfPresent(String.self, forKey: CodingKeys.fromDate) 
        toDate = try container.decodeIfPresent(String?.self, forKey: CodingKeys.toDate) as? String
        address = try container.decodeIfPresent(String?.self, forKey: CodingKeys.address) as? String
        note = try container.decodeIfPresent(String?.self, forKey: CodingKeys.note) as? String
        visitStatusID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.visitStatusID) as? Int
        isDeleted = try container.decodeIfPresent(Bool?.self, forKey: CodingKeys.isDeleted) as? Bool
        cycleID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.cycleID) as? Int
        entryUserID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.entryUserID) as? Int
        entryDate = try container.decodeIfPresent(String?.self, forKey: CodingKeys.entryDate) as? String
        modifyUserID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.modifyUserID) as? Int
        modifyDate = try container.decodeIfPresent(String?.self, forKey: CodingKeys.modifyDate) as? String
        doctor = try container.decodeIfPresent(Doctor?.self, forKey: CodingKeys.doctor) as? Doctor
        visitStatus = try container.decodeIfPresent(VisitStatus?.self, forKey: CodingKeys.visitStatus) as? VisitStatus
        cycle = try container.decodeIfPresent(Cycle?.self, forKey: CodingKeys.cycle) as? Cycle
    }
}

// MARK: - Cycle
struct Cycle: Codable {
    let id: Int?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.id) as? Int
        name = try container.decodeIfPresent(String?.self, forKey: CodingKeys.name) as? String
    }
}

// MARK: - Doctor
struct Doctor: Codable {
    let id: Int?
    let nameFirst, nameSecond, nameThird, nameLast: String?
    let identityID: String?
    let majorID, cityID, classificationID, visitsCount: Int?
    let imgURL: String?
    let entryUserID: Int?
    let entryDate, modifyDate: String?
    let modifyUserID: Int?
    let city: Cycle?
    let classification: Classification?
    let major: Cycle?
    let entryUser, modifyUser: String?
    let completedVisitsCount: Int?
    let doctorFiles: String?
    let mobileNo:String?
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case nameFirst = "NameFirst"
        case nameSecond = "NameSecond"
        case nameThird = "NameThird"
        case nameLast = "NameLast"
        case identityID = "IdentityID"
        case majorID = "MajorID"
        case cityID = "CityID"
        case classificationID = "ClassificationID"
        case visitsCount = "VisitsCount"
        case imgURL = "ImgUrl"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
        case modifyUserID = "ModifyUserID"
        case modifyDate = "ModifyDate"
        case city = "City"
        case classification = "Classification"
        case major = "Major"
        case entryUser = "EntryUser"
        case modifyUser = "ModifyUser"
        case completedVisitsCount = "CompletedVisitsCount"
        case doctorFiles = "DoctorFiles"
        case mobileNo = "MobileNo"

    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.id) as? Int
        nameFirst = try container.decodeIfPresent(String?.self, forKey: CodingKeys.nameFirst) as? String
        nameSecond = try container.decodeIfPresent(String?.self, forKey: CodingKeys.nameSecond) as? String
        nameThird = try container.decodeIfPresent(String?.self, forKey: CodingKeys.nameThird) as? String
        nameLast = try container.decodeIfPresent(String?.self, forKey: CodingKeys.nameLast) as? String
        identityID = try container.decodeIfPresent(String?.self, forKey: CodingKeys.identityID) as? String
        majorID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.majorID) as? Int
        cityID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.cityID) as? Int
        classificationID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.classificationID) as? Int
        visitsCount = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.visitsCount) as? Int
        imgURL = try container.decodeIfPresent(String?.self, forKey: CodingKeys.imgURL) as? String
        entryUserID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.entryUserID) as? Int
        entryDate = try container.decodeIfPresent(String?.self, forKey: CodingKeys.entryDate) as? String
        modifyUserID = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.modifyUserID) as? Int
        modifyDate = try container.decodeIfPresent(String?.self, forKey: CodingKeys.modifyDate) as? String
        city = try container.decodeIfPresent(Cycle?.self, forKey: CodingKeys.city) as? Cycle
        classification = try container.decodeIfPresent(Classification?.self, forKey: CodingKeys.classification) as? Classification
        major = try container.decodeIfPresent(Cycle?.self, forKey: CodingKeys.major) as? Cycle
        entryUser = try container.decodeIfPresent(String?.self, forKey: CodingKeys.entryUser) as? String
        modifyUser = try container.decodeIfPresent(String?.self, forKey: CodingKeys.modifyUser) as? String
        mobileNo = try container.decodeIfPresent(String?.self, forKey: CodingKeys.mobileNo) as? String
        completedVisitsCount = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.completedVisitsCount) as? Int
        doctorFiles = try container.decodeIfPresent(String?.self, forKey: CodingKeys.doctorFiles) as? String
    }
    
    
}

// MARK: - Classification

struct Classification: Codable {
    let id: Int?
    let name: String?
    let visitsCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case visitsCount = "VisitsCount"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.id) as? Int
        name = try container.decodeIfPresent(String?.self, forKey: CodingKeys.name) as? String
        visitsCount = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.visitsCount) as? Int
    }
    
}

// MARK: - VisitStatus
struct VisitStatus: Codable {
    let id: Int?
    let name: String?
    let isEditable: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case isEditable = "IsEditable"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int?.self, forKey: CodingKeys.id) as? Int
        name = try container.decodeIfPresent(String?.self, forKey: CodingKeys.name) as? String
        isEditable = try container.decodeIfPresent(Bool?.self, forKey: CodingKeys.isEditable) as? Bool
    }
}

