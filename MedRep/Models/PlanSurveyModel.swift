//
//  PlanSurveyModel.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/15/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
// MARK: - PlanOfActionServey
struct PlanSurveyModel: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription:String?
    let dataa: String?
    var surveys:[Surveys]?
    
    enum CodingKeys: String, CodingKey {
        
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case surveys = "Dataa"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([Surveys].self, from: data)
        surveys = res
    }
}

// MARK: - Survey
struct Surveys: Codable {
    let id: Int?
    let name: String?
    let majorID: Int?
    let expiryDate, major: String?
    let surveyQuestions: [SurveyQuestion]?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case majorID = "MajorID"
        case expiryDate = "ExpiryDate"
        case major = "Major"
        case surveyQuestions = "SurveyQuestions"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String
        majorID = try values.decodeIfPresent(Int?.self, forKey: .majorID) as? Int
        expiryDate = try values.decodeIfPresent(String?.self, forKey: .expiryDate) as? String
        major = try values.decodeIfPresent(String?.self, forKey: .major) as? String
        surveyQuestions = try values.decodeIfPresent([SurveyQuestion]?.self, forKey: .surveyQuestions) as? [SurveyQuestion]


    }
}

// MARK: - SurveyQuestion

struct SurveyQuestion: Codable {
    let id, surveyID: Int?
    let question: String?
    let surveyQuestionAnswers: [SurveyQuestionAnswer]?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case surveyID = "SurveyID"
        case question = "Question"
        case surveyQuestionAnswers = "SurveyQuestionAnswers"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        surveyID = try values.decodeIfPresent(Int?.self, forKey: .surveyID) as? Int
        question = try values.decodeIfPresent(String?.self, forKey: .question) as? String
        surveyQuestionAnswers = try values.decodeIfPresent([SurveyQuestionAnswer]?.self, forKey: .surveyQuestionAnswers) as? [SurveyQuestionAnswer]

    }
}

// MARK: - SurveyQuestionAnswer
struct SurveyQuestionAnswer: Codable {
    let id, surveyQuestionID: Int?
    let answer: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case surveyQuestionID = "SurveyQuestionID"
        case answer = "Answer"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        surveyQuestionID = try values.decodeIfPresent(Int?.self, forKey: .surveyQuestionID) as? Int
        answer = try values.decodeIfPresent(String?.self, forKey: .answer) as? String

    }
}

//typealias Surveys = [Survey]
