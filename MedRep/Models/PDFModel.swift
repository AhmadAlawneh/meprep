//
//  PDFModel.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 10/6/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - PDFModel
struct PDFModel: Codable {
    let data: [PDfData]?
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent([PDfData].self, forKey: .data)
    }
}

// MARK: - Datum
struct PDfData: Codable {
    let id: Int?
    let name: String?
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
}
