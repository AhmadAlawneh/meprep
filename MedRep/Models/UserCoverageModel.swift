//
//  UserCoverageModel.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/14/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - UserCoverage

struct UserCoverage: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription: String?
    let dataa: String?
    let currentVisits:DataClass?
    
    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
    }
    
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode(DataClass.self, from: data)
        currentVisits = res
        
    }
    
    
    
}

// MARK: - DataClass
struct DataClass: Codable {
    let completedVisitsCount, unCompletedVisitsCount, visitsCount: Int?
    let fromDate, toDate: String?
    let percent: Float64?
    
    enum CodingKeys: String, CodingKey {
        case completedVisitsCount = "CompletedVisitsCount"
        case unCompletedVisitsCount = "UnCompletedVisitsCount"
        case visitsCount = "VisitsCount"
        case fromDate = "FromDate"
        case toDate = "ToDate"
        case percent = "Percent"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        completedVisitsCount = try values.decodeIfPresent(Int?.self, forKey: .completedVisitsCount) as? Int
        unCompletedVisitsCount = try values.decodeIfPresent(Int?.self, forKey: .unCompletedVisitsCount) as? Int
        visitsCount = try values.decodeIfPresent(Int?.self, forKey: .visitsCount) as? Int
        fromDate = try values.decodeIfPresent(String?.self, forKey: .fromDate) as? String
        toDate = try values.decodeIfPresent(String?.self, forKey: .toDate) as? String
        percent = try values.decodeIfPresent(Float64?.self, forKey: .percent) as? Float64
    }
}
