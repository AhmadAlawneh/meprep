//
//  PlanToDoModel.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/15/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
// MARK: - PlanOfActionToDo
struct PlanToDoModel: Codable {
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription, dataa: String?
    var toDo:[ToDoElement]?
    
    enum CodingKeys: String, CodingKey {
        
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
        case toDo = "Dataa"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([ToDoElement].self, from: data)
        toDo = res
    }
}

// MARK: - ToDoElement
struct ToDoElement: Codable {
    let id, doctorID: Int?
    let note: String?
    let isSeen, isDeleted: Bool?
    let entryUserID: Int?
    let entryDate: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case doctorID = "DoctorID"
        case note = "Note"
        case isSeen = "IsSeen"
        case isDeleted = "IsDeleted"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        doctorID = try values.decodeIfPresent(Int?.self, forKey: .doctorID) as? Int
        note = try values.decodeIfPresent(String?.self, forKey: .note) as? String
        isSeen = try values.decodeIfPresent(Bool?.self, forKey: .isSeen) as? Bool
        isDeleted = try values.decodeIfPresent(Bool?.self, forKey: .isDeleted) as? Bool
        entryUserID = try values.decodeIfPresent(Int?.self, forKey: .entryUserID) as? Int
        entryDate = try values.decodeIfPresent(String?.self, forKey: .entryDate) as? String
        
    }
}

//typealias ToDo = [ToDoElement]
