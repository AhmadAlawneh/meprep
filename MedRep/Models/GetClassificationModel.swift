//
//  GetClassificationModel.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/26/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation


// MARK: - GetClassification
struct GetClassification: Codable {
    
    let isSuccess: Bool?
    let statusCode: Int?
    let statusDescription: String?
    let dataa:String?
    let classificationData: [ClassificationList]?

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case dataa = "Data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool?.self, forKey: .isSuccess) as? Bool
        statusCode = try values.decodeIfPresent(Int?.self, forKey: .statusCode) as? Int
        statusDescription = try values.decodeIfPresent(String?.self, forKey: .statusDescription) as? String
        dataa = try values.decodeIfPresent(String?.self, forKey: .dataa) as? String
        let data = dataa?.data(using: .utf8) ?? Data()
        let res =   try JSONDecoder().decode([ClassificationList].self, from: data)
        classificationData = res
        
    }
    
}

// MARK: - Datum
struct ClassificationList: Codable {
    let id: Int?
    let name: String?
    let visitsCount: Int?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case visitsCount = "VisitsCount"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int?.self, forKey: .id) as? Int
        name = try values.decodeIfPresent(String?.self, forKey: .name) as? String
        visitsCount = try values.decodeIfPresent(Int?.self, forKey: .visitsCount) as? Int
    }
}
