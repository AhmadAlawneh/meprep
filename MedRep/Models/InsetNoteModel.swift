//
//  InsetNoteModel.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/22/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation

// MARK: - Inset Note

struct InsetNote: Codable {
    let id, doctorID: Int?
    let note: String?
    let isSeen, isDeleted: Bool?
    let entryUserID: Int?
    let entryDate: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case doctorID = "DoctorID"
        case note = "Note"
        case isSeen = "IsSeen"
        case isDeleted = "IsDeleted"
        case entryUserID = "EntryUserID"
        case entryDate = "EntryDate"
    }
}
