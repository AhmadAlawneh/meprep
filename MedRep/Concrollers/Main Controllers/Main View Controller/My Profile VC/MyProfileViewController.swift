//
//  MyProfileViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/28/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import FAPickerView

class MyProfileViewController: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var fName: UILabel!
    @IBOutlet weak var lName: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var logout: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        Helper.addBorderView(view: mainView, borderWidth: 2)
        Helper.addBorderView(view: infoView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: logout, borderWidth: 1, cornerRadius: 5)
        setDataView()
    }
    
    
    
    //MARK:- Helper Mothods
    
    func setDataView(){
        if let userInfo = LongTermManager.getUserInfo() {
            self.fName.text = userInfo.userData?.name ?? ""
            self.lName.text = userInfo.userData?.username ?? ""
            self.mobileNumber.text = userInfo.userData?.mobileNo ?? ""
            self.email.text = userInfo.userData?.email ?? ""
            self.location.text = userInfo.userData?.address ?? ""
        }
    }
    
    
    
    //    static let userInfo = "userInfo"
    //    static let jwtTokenFromLogin = "jwtTokenFromLogin"
    
//    func singOutLaundryApp(){
//
//        FAPickerView.setMainColor(UIColor.mainColor)
//        FAPickerView.showAlert(message: "Are you sure about that",
//                               headerTitle: "Alert",
//                               confirmTitle: "Logout",
//                               cancelTitle: "Cancel") { (button:FAPickerAlertButton) in
//                                if button.rawValue == 1 {
//                                    var rootVC : UIViewController?
//                                    rootVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewControllerID")
//                                    UserDefaults.standard.removeObject(forKey: Keys.userInfo)
//                                    UserDefaults.standard.removeObject(forKey: Keys.jwtTokenFromLogin)
//                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                                    appDelegate.window?.rootViewController = rootVC
//                                }else{
//                                    print("cancel")
//                                }
//        }
//    }
    
    
    
    
    //MARK:- Action && Selectoer
    
    @IBAction func popActionView(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func logouActionButton(_ sender: Any) {
        Helper.singOutMedRepApp()
    }
    
}
