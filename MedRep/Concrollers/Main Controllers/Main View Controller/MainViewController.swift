//
//  MainViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/7/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import FSCalendar
class MainViewController: UIViewController, FSCalendarDataSource {
    
    
    fileprivate let servicesCell = "ServicesCell"
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var coverageValueLabel: UILabel! //
    @IBOutlet weak var coverageTitleLabel: UILabel!
    @IBOutlet weak var coverageDateLabel: UILabel! //
    @IBOutlet weak var customerNotSeenValueLabel: UILabel! //
    @IBOutlet weak var customerNotSeenTitleLabel: UILabel!
    @IBOutlet weak var customerNotSeenDateLabel: UILabel! //
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var containerSearchView: UIView!
    @IBOutlet weak var repNameLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var calenderView: FSCalendar!
    
    var servicesList = [FunctionUser]()
    var currentVisits = [Datum]()
    var fromDate = [String]()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        return formatter
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calenderView.customizeCalenderAppearance()
        setupMainViewControlelr()
        setupTopCollectionViewCell()
        collectionView.dataSource = self
        collectionView.delegate = self
        containerSearchView.layer.cornerRadius = 5
        calenderView.layer.cornerRadius = 10
        if let userInfo = LongTermManager.getUserInfo()?.userData {
            repNameLabel.text = "Welcome , \(userInfo.name ?? "")"
            self.servicesList = userInfo.functionUser ?? []
            self.servicesList = self.servicesList.sorted(by: {$0.functionID ?? 0 < $1.functionID ?? 0})

        }
        
        var servicesListAfterFilter = [FunctionUser]()

        for item  in self.servicesList{
            if item.enabled ?? false {
                servicesListAfterFilter.append(item)
            }
        }
        self.servicesList = servicesListAfterFilter
        
        getUserCoverageCallApi()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Helping Methods
    
    func setupMainViewControlelr(){
        Helper.addBorderView(view: mainView, borderWidth: 5)
        mainView.layer.borderWidth = 5
        mainView.layer.borderColor = UIColor.mainColor.cgColor
        topView.backgroundColor = UIColor.mainColor
        bottomView.backgroundColor = UIColor.secondaryColor
        collectionView.register(UINib(nibName: servicesCell, bundle: nil), forCellWithReuseIdentifier: servicesCell)
    }
    
    
    func setupTopCollectionViewCell(){
        
        let flowLayout = UICollectionViewFlowLayout()
        //         let screenWidth = UIScreen.main.bounds.width
        //let coolectionWidth = screenWidth
        flowLayout.itemSize = CGSize(width: 165 , height: 223)
        flowLayout.scrollDirection = .vertical
        //         flowLayout.minimumLineSpacing = 0
        //         flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets(top: 10 , left: 0, bottom: 10, right: 0)
        //UIEdgeInsets.zero
        collectionView.isScrollEnabled = true
        collectionView.bounces = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.collectionViewLayout = flowLayout
        collectionView.backgroundColor = UIColor.clear
        collectionView.contentInset = UIEdgeInsets.zero
        collectionView.backgroundColor = UIColor.clear
        
    }
    
    
    //MARK:- Action && Selector
    
    @IBAction func findDoctorActionButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let doctorsVc = storyboard.instantiateViewController(withIdentifier: "FindDoctorsViewController") as! FindDoctorsViewController
        self.navigationController?.pushViewController(doctorsVc, animated: true)
    }
    
    
    
    @IBAction func viewProfileButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "MyProfile", bundle: nil)
        let doctorsVc = storyboard.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        self.navigationController?.pushViewController(doctorsVc, animated: true)
    }
    
    
    //MARK:- API Call's
    
    // Get Current Month Visits
    
    func getCurrentMonthVisits()  {
        Helper.showIndicator(view: self.view)
        let endPoint = MainEndPoint.currentMonthVisits
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<CurrentMonthVisits>) in
            guard let strongSelf = self else {return}
            Helper.hideIndicator(view: strongSelf.view)
            DispatchQueue.main.async {
                
                switch response.result {
                case .success(let value):
                    strongSelf.currentVisits = value.currentVisits ?? []
                    let getDate = strongSelf.currentVisits.compactMap({$0.fromDate})
                    for stringDate in getDate {
                        if let stringDate = strongSelf.getDate(date: stringDate) {
                            print(stringDate)
                            strongSelf.fromDate.append(stringDate)
                        }
                    }
                    strongSelf.calenderView.reloadData()
                    print(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                         Helper.singOutCallApi()
                     }else{
                        Helper.alertOneButton(message: "\(res?.statusDescription ?? "")", alertTitle: "Alert", titleButton: "Ok")
                     }
                }
            }
        }
    }
    
    
    func getUserCoverageCallApi()  {
        Helper.showIndicator(view: self.view)
        let endPoint = MainEndPoint.userCoverage
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<UserCoverage>) in
            guard let strongSelf = self else {return}
            Helper.hideIndicator(view: strongSelf.view)
            DispatchQueue.main.async {
                
                switch response.result {
                case .success(let value):
                    print(value)    
                    
                    if let object = value.currentVisits {
                        
                        let x = Double(object.percent ?? Float64())
                        
                        strongSelf.coverageValueLabel.text = String(format: "%.2f", x)
                        
                        let fromDate = strongSelf.getDate(date: object.fromDate ?? "")
                        let toDate = strongSelf.getDate(date: object.toDate ?? "")
                        strongSelf.coverageDateLabel.text = "\(fromDate ?? "") - \(toDate ?? "")"
                        strongSelf.customerNotSeenValueLabel.text = "\(object.unCompletedVisitsCount ?? -999)"
                        strongSelf.customerNotSeenDateLabel.text = "\(fromDate ?? "") - \(toDate ?? "")"
                    }
                    strongSelf.getCurrentMonthVisits()
                case .failure(let error):
                    print(error.localizedDescription)
                    
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                         Helper.singOutCallApi()
                     }else{
                        Helper.alertOneButton(message: "\(res?.statusDescription ?? "")", alertTitle: "Alert", titleButton: "Ok")
                     }
                }
            }
        }
    }

    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        let dateString = self.dateFormatter2.string(from: date)
        
        if self.fromDate.contains(dateString) {
            return 1
        }
        return 0
    }
    
    func getDate(date:String) -> String? {
        let dateFormatter = DateFormatter()
        
        if date.contains(".") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        let dateT = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM-dd-yyyy"
        return  dateFormatter.string(from: dateT ?? Date())
    }
}


extension MainViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return servicesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: servicesCell, for: indexPath) as? ServicesCell else {return UICollectionViewCell()}
        
        let object = self.servicesList[indexPath.row]
        cell.serviceObject = object
        return cell
    }
    
}

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            let storyboard = UIStoryboard(name: "Planner", bundle: nil)
            let doctorsVc = storyboard.instantiateViewController(withIdentifier: "PlannerViewController") as! PlannerViewController
            self.navigationController?.pushViewController(doctorsVc, animated: true)
            break
        case 1:
            print(indexPath.row)
            let storyboard = UIStoryboard(name: "ChangeRequest", bundle: nil)
            let doctorsVc = storyboard.instantiateViewController(withIdentifier: "ChangeRequestServicesVC") as! ChangeRequestServicesVC
            self.navigationController?.pushViewController(doctorsVc, animated: true)
            break
        case 2:
            print(indexPath.row)
            let storyboard = UIStoryboard(name: "PlanOfAction", bundle: nil)
            let doctorsVc = storyboard.instantiateViewController(withIdentifier: "PlanOfActionViewController") as! PlanOfActionViewController
            self.navigationController?.pushViewController(doctorsVc, animated: true)
            break
        case 3:
            let storyboard = UIStoryboard(name: "IncompleteCalles", bundle: nil)
            let doctorsVc = storyboard.instantiateViewController(withIdentifier: "IncompleteCallsViewControllerID") as! IncompleteCallsViewController
            self.navigationController?.pushViewController(doctorsVc, animated: true)
            
            break
        case 4:
            Helper.alertOneButton(message: "This service not active right now", alertTitle: "Alert", titleButton: "Ok")
            print(indexPath.row)
            break
        default:
            print("any")
        }
    }
}


extension UIColor {
    static let mainColor = UIColor(red: 0.30, green: 0.52, blue: 1.00, alpha: 1.00)
    static let secondaryColor = UIColor(red: 0.84, green: 0.91, blue: 0.99, alpha: 1.00)
}

extension FSCalendar {
    func customizeCalenderAppearance() {
        self.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesSingleUpperCase]
        
        self.appearance.headerTitleFont      = UIFont.init(name: "Roboto-Bold", size: 25)
        self.appearance.weekdayFont          = UIFont.init(name: "Roboto-Light", size: 25)
        self.appearance.titleFont            = UIFont.init(name: "Roboto-Light", size: 25)
        //
        //        self.appearance.headerTitleFont  = UIFont.init(name: <#T##String#>, size: <#T##CGFloat#>)
        //        self.appearance.headerTitleColor     = Colors.NavTitleColor
        //        self.appearance.weekdayTextColor     = Colors.topTabBarSelectedColor
        //        self.appearance.eventDefaultColor    = Colors.NavTitleColor
        //        self.appearance.selectionColor       = Colors.purpleColor
        //        self.appearance.titleSelectionColor  = Colors.NavTitleColor
        //        self.appearance.todayColor           = Colors.purpleColor
        //        self.appearance.todaySelectionColor  = Colors.purpleColor
        
        self.appearance.headerMinimumDissolvedAlpha = 0.0 // Hide Left Right Month Name
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
