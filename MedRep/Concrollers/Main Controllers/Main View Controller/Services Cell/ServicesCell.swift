//
//  ServicesCell.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/8/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class ServicesCell: UICollectionViewCell {
    

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    
    
    var serviceObject: FunctionUser? {
        didSet{
            if let object = serviceObject {
                self.setupServicesCell(object: object)
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 5
    }
    
    //MARK: - Helping Methods
    
    func setupServicesCell(object:FunctionUser){
       if let serviceType = object.function?.name {
            serviceName.text = serviceType
        }
        serviceImage.image = UIImage(named: "\(object.functionID ?? 0)")
    }

}
