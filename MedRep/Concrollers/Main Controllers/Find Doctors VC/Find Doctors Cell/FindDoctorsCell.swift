//
//  FindDoctorsCell.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/8/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class FindDoctorsCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var majorLAbel: UILabel!
    @IBOutlet var location: UILabel!
    @IBOutlet var mobileNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
