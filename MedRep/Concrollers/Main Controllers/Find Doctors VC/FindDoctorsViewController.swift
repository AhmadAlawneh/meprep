//
//  FindDoctorsViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/8/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

protocol FilterData {
    func selectedData(doctorFilter:[PDoctor],city:String,major:String,selectedCPA:String)
}


class FindDoctorsViewController: UIViewController,UISearchBarDelegate {
    
    
    fileprivate let doctorsCell = "FindDoctorsCell"
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet var searchBar: UISearchBar!
    var searchActive : Bool = false
    var doctorFilter:[PDoctor]?
    var filtered:[PDoctor]?
    var city = "-1"
    var major = "-1"
    var selectedCPA = "true"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
        searchBar.backgroundColor = UIColor.mainColor
        searchBar.placeholder = "Search"
        searchBar.backgroundImage = UIImage()

        
 
        
//        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
//        self.navigationItem.rightBarButtonItem = leftNavBarButton
        let currentDateTime = Date()
        
        let dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        getDoctors(date: dateTime)
        
        setupFindDoctorsViewController()
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).clearButtonMode = .never

        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    
    //MARK:- Helping Methods
    
    func setupFindDoctorsViewController(){
        Helper.MakeRoundedView(view: filterView, borderWidth: 1)
        Helper.addBorderView(view: containerView, borderWidth: 2, cornerRadius: 10)
        //      Helper.addBorderView(view: mainView)
        tableView.register(UINib(nibName: doctorsCell, bundle: nil), forCellReuseIdentifier: doctorsCell)
    }
    
    
    //MARK:- Action && Selector
    
    @IBAction func filterActionButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let customFilter = storyboard.instantiateViewController(withIdentifier: "DoctorsFilterViewController") as! DoctorsFilterViewController
        customFilter.providesPresentationContextTransitionStyle = true
        customFilter.definesPresentationContext = true
        customFilter.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customFilter.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customFilter.selectedData = self
        self.present(customFilter, animated: true, completion: nil)
    }
    
    
}

extension FindDoctorsViewController{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if filtered?.isEmpty ?? true {
            searchActive = false;
        }else{
            searchActive = true;
            
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        filtered = doctorFilter?.filter({ (text) -> Bool in
            let name  = "\(text.nameLast ?? ""), \(text.nameFirst ?? "") \(text.nameSecond ?? "") \(text.nameThird ?? "")"
            let tmp: NSString = name.lowercased() as NSString
            let range = tmp.range(of: searchText.lowercased(), options: .caseInsensitive)
            return range.location != NSNotFound
        })
        if searchText.isEmptyOrWhitespace {
            searchActive = false
        }else{
            searchActive = true
            
        }
        
        self.tableView.reloadData()
    }
    
}

//MARK:- UITable View Data Source

extension FindDoctorsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let currentDateTime = Date()
        let dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        
        if searchActive {
            if indexPath.row + 1 == filtered?.count {
                
                getDoctorsByLast(date: dateTime, majorID: city, cityID: major, selectedClobalOrCPA: selectedCPA, maxID: "\(filtered?[indexPath.row].id ?? 0)", recCount: "100", doctorName: "--\(searchBar.text ?? "")--")
            }
        }else{
            if indexPath.row + 1 == doctorFilter?.count {
                
                getDoctorsByLast(date: dateTime, majorID: city, cityID: major, selectedClobalOrCPA: selectedCPA, maxID: "\(doctorFilter?[indexPath.row].id ?? 0)", recCount: "100", doctorName: "--\("all")--")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            let count = filtered?.count ?? 0
            if count == 0 {
                tableView.setEmptyView(title: "No Data found", message: nil)
            }else{
                tableView.restore()
            }
            return count
        }else{
            
            
            let count = doctorFilter?.count ?? 0
            if count == 0 {
                tableView.setEmptyView(title: "No Data found", message: nil)
            }else{
                tableView.restore()
            }
            return count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        guard let cell = tableView.dequeueReusableCell(withIdentifier: doctorsCell, for: indexPath) as? FindDoctorsCell else {return UITableViewCell()}
        if searchActive {
            cell.nameLabel.text = "\(filtered?[indexPath.row].nameLast ?? ""), \(filtered?[indexPath.row].nameFirst ?? "") \(filtered?[indexPath.row].nameSecond ?? "") \(filtered?[indexPath.row].nameLast ?? "")"
            cell.majorLAbel.text = filtered?[indexPath.row].major?.name ?? ""
            cell.location.text = filtered?[indexPath.row].city?.name ?? ""
            cell.mobileNumber.text = filtered?[indexPath.row].mobileNo ?? ""
        }else{
            cell.nameLabel.text = "\(doctorFilter?[indexPath.row].nameLast ?? ""), \(doctorFilter?[indexPath.row].nameFirst ?? "") \(doctorFilter?[indexPath.row].nameSecond ?? "") \(doctorFilter?[indexPath.row].nameLast ?? "")"
            cell.majorLAbel.text = doctorFilter?[indexPath.row].major?.name ?? ""
            cell.location.text = doctorFilter?[indexPath.row].city?.name ?? ""
            cell.mobileNumber.text = doctorFilter?[indexPath.row].mobileNo ?? ""
        }
        cell.selectionStyle = .none
        return cell
    }
}


//MARK:- UITable View Delagete

extension FindDoctorsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchActive {
            let selectedDoctorInfo = filtered?[indexPath.row]
            let storyboard = UIStoryboard(name: "DoctorServices", bundle: nil)
            let doctorsVc = storyboard.instantiateViewController(withIdentifier: "DoctorServicesViewController") as! DoctorServicesViewController
            doctorsVc.doctorInfo = selectedDoctorInfo
            self.navigationController?.pushViewController(doctorsVc, animated: true)
        }else{
            let selectedDoctorInfo = doctorFilter?[indexPath.row]
            let storyboard = UIStoryboard(name: "DoctorServices", bundle: nil)
            let doctorsVc = storyboard.instantiateViewController(withIdentifier: "DoctorServicesViewController") as! DoctorServicesViewController
            doctorsVc.doctorInfo = selectedDoctorInfo
            self.navigationController?.pushViewController(doctorsVc, animated: true)
        }
    }
}


extension FindDoctorsViewController{
    
    func getDoctors(date:String) {
        
        
        let parameters = [date]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctors)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<DoctorFilterHome>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    print(value)
                    
                    strongSelf.doctorFilter = value.doctorsFilterData
                    strongSelf.tableView.reloadData()
                    
                }
            }
            
            
        }
        
    }
    
    
    func getDoctorsByLast(date:String,majorID:String,cityID:String,selectedClobalOrCPA:String,maxID:String,recCount:String,doctorName:String) {
        
        
        let parameters = [date,cityID,majorID,selectedClobalOrCPA,maxID,recCount,doctorName]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorsByFilter)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<DoctorFilter>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    print(value)
                    
                    for item in value.doctorsFilterData?.doctors ?? [PDoctor]() {
                        strongSelf.doctorFilter?.append(item)
                        
                        
                        
                    }
                    if value.doctorsFilterData?.doctors?.count != 0 {
                        strongSelf.tableView.reloadData()

                    }
                    
                    
                }
            }
            
            
        }
        
    }
    
}


extension FindDoctorsViewController:FilterData{
    func selectedData(doctorFilter: [PDoctor],city:String,major:String,selectedCPA:String) {
        self.doctorFilter = doctorFilter
        self.city = city
        self.major = major
        self.selectedCPA = selectedCPA
        tableView.reloadData()
    }
    
    
}
