//
//  DoctorsFilterViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/9/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import FAPickerView

class DoctorsFilterViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var globalDoctorButton: UIButton!
    @IBOutlet weak var cpaDoctoreButton: UIButton!
    @IBOutlet weak var typeOneTextFiled: UIView!
    @IBOutlet weak var typeTwoTextField: UIView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var majorLabel: UILabel!
    
    var selectedData:FilterData?
    
    var selectedTypeItem = FAPickerItem()
    var selectedMajorItem = FAPickerItem()
    var major = "-1"
    var city = "-1"
    
    var items = Array<FAPickerItem>()
    var selectedCPAOrGlobal = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animateView()
        Helper.addBorderView(view: globalDoctorButton, borderWidth: 0, cornerRadius: 15)
        Helper.addBorderView(view: cpaDoctoreButton, borderWidth: 0, cornerRadius: 15)
        Helper.addBorderView(view: filterButton, borderWidth: 0, cornerRadius: 10)
        Helper.addBorderView(view: typeOneTextFiled, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: typeTwoTextField, borderWidth: 1, cornerRadius: 5)
        containerView.layer.cornerRadius = 10
        //        Helper.roundCorners(view: cpaDoctoreButton, corners: [.topRight, .bottomRight , .topLeft , .bottomLeft], radius: 20)
        //        Helper.roundCorners(view: globalDoctorButton, corners: [.topRight, .bottomRight , .topLeft , .bottomLeft], radius: 20)
        
        if selectedCPAOrGlobal {
         selectedCPAOrGlobal = false
                globalDoctorButton.backgroundColor = .yellow
                cpaDoctoreButton.backgroundColor = UIColor(displayP3Red: 255, green: 255, blue: 255, alpha: 0.5)
        }else{
        cpaDoctoreButton.backgroundColor = .yellow
                 globalDoctorButton.backgroundColor = UIColor(displayP3Red: 255, green: 255, blue: 255, alpha: 0.5)
                 selectedCPAOrGlobal = true
       
        }
        
    }
    
    @IBAction func globalPressedd(_ sender: Any) {
        selectedCPAOrGlobal = false
        globalDoctorButton.backgroundColor = .yellow
                  cpaDoctoreButton.backgroundColor = UIColor(displayP3Red: 255, green: 255, blue: 255, alpha: 0.5)
    }
    @IBAction func cpaPressed(_ sender: Any) {
        selectedCPAOrGlobal = true

        cpaDoctoreButton.backgroundColor = .yellow
                 globalDoctorButton.backgroundColor = UIColor(displayP3Red: 255, green: 255, blue: 255, alpha: 0.5)
    }
    @IBAction func selectCity(_ sender: Any) {
        items.removeAll()
        getData(string: ApiURLs.GetCity)
    }
    
    @IBAction func selectMAjor(_ sender: Any) {
        items.removeAll()
             getData(string: ApiURLs.GetMajor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    
    //MARK:- Helping Methods
    
    func animateView() {
        containerView.alpha = 0;
        self.containerView.frame.origin.y = self.containerView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.containerView.alpha = 1.0;
            self.containerView.frame.origin.y = self.containerView.frame.origin.y - 50
        })
    }
    
    
    func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Action && Selector
    
    @IBAction func dismissButton(_ sender: UIButton) {
        dismissView()
    }

    @IBAction func cancelButton(_ sender: UIButton) {
        dismissView()
    }
    
    @IBAction func filterPressed(_ sender: Any) {
        var selected = "true"
        if selectedCPAOrGlobal {
            selected = "true"
        }else{
            selected = "false"
        }
        let currentDateTime = Date()

        let dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        print(selectedTypeItem.id)

      
        
        getDoctors(date: dateTime, majorID: city, cityID: major, selectedClobalOrCPA: selected,maxID: "0",recCount: "100",doctorName: "--all--")

    }
}



extension DoctorsFilterViewController{
    func getData(string:String) {
        
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        
        let url = conectURL(parameters: parameters, url: string)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<AddPlanner>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    print(value)
                    if string == ApiURLs.GetCity {
                        for val in value.timeOut ?? TimeOutData() {
                            strongSelf.items.append(FAPickerItem(id: "\(val.id)", title: val.name))
                        }
                        
                        
                        FAPickerView.setMainColor(UIColor.init(red: 0.000, green: 0.357, blue: 0.675, alpha: 1.00))
                        FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                          selectedItem: strongSelf.selectedMajorItem,
                                                          filter: true,
                                                          headerTitle: "Select Area",
                                                          complete: { (item:FAPickerItem?) in
                                                            if let item = item {
                                                                strongSelf.selectedMajorItem = item
                                                                strongSelf.city = item.id
                                                                strongSelf.cityLabel.text = item.title
                                                                
                                                            }
                        }, cancel: {
                            
                        })
                    }else if string == ApiURLs.GetMajor{
                        for val in value.timeOut ?? TimeOutData() {
                            strongSelf.items.append(FAPickerItem(id: "\(val.id)", title: val.name))
                        }
                        
                        
                        FAPickerView.setMainColor(UIColor.init(red: 0.000, green: 0.357, blue: 0.675, alpha: 1.00))
                        FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                          selectedItem: strongSelf.selectedTypeItem,
                                                          filter: true,
                                                          headerTitle: "Select Major",
                                                          complete: { (item:FAPickerItem?) in
                                                            if let item = item {
                                                                strongSelf.selectedTypeItem = item
                                                                strongSelf.major = item.id
                                                                strongSelf.majorLabel.text = item.title
                                                            }
                        }, cancel: {
                            
                        })
                    }
                    
                    
                    
                }
            }
            
            
        }
        
    }
    
}

extension DoctorsFilterViewController{
    //api/PlanOfAction/GetDoctorsByFilter/{strDate}/{MajorID}/{CityID}/{IsCPA}/{MaximumID}/{RecordsCount}/{DoctorName}
    
    func getDoctors(date:String,majorID:String,cityID:String,selectedClobalOrCPA:String,maxID:String,recCount:String,doctorName:String) {
        
        
        let parameters = [date,cityID,majorID,selectedClobalOrCPA,maxID,recCount,doctorName]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorsByFilter)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<DoctorFilter>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    print(value)
                    strongSelf.dismiss(animated: true, completion: {
                        var selected = "true"
                        if strongSelf.selectedCPAOrGlobal {
                                 selected = "true"
                             }else{
                                 selected = "false"
                             }
                        strongSelf.selectedData?.selectedData(doctorFilter: value.doctorsFilterData?.doctors ?? [PDoctor](),city: strongSelf.city,major: strongSelf.major,selectedCPA: selected)
                        
                    })
                    
                    
                    
                }
            }
            
            
        }
        
    }
}
