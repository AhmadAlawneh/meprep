//
//  UpDateDoctorInfoViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/25/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import FAPickerView


class UpDateDoctorInfoViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var allViews: [UIView]!
    @IBOutlet weak var selectDoctorName: UILabel!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var majorName: UILabel!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var secondName: UITextField!
    @IBOutlet weak var thirdName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var doctorIdentityId: UITextField!
    @IBOutlet weak var classificationId: UITextField!
    @IBOutlet weak var visitsCount: UITextField!
    @IBOutlet weak var docPhone: UITextField!
    @IBOutlet weak var selectCityButton: UIButton!
    @IBOutlet weak var selectMajorButton: UIButton!
    @IBOutlet weak var selectClassificationButton: UIButton!
    
    @IBOutlet weak var docEmail: UITextField!
    @IBOutlet weak var docNote: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    
    let currentDateTime = Date()
    
    var doctorList = [DoctorList]()
    var selectedDoctor = FAPickerItem()
    var selectedDoctorId = -1
    
    
    var cityList = [CityList]()
    var selectedCity = FAPickerItem()
    var selectedCityId = -1
    
    
    var majorList = [MajorList]()
    var selectedMajor = FAPickerItem()
    var selectedMajorId = -1
    
    
    var clasificationList = [ClassificationList]()
    var selectedClassification = FAPickerItem()
    var selectedClassificationId = -1
    var visitsCountId = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        navigationController?.navigationBar.barTintColor = UIColor.white
        title = "UpDate Doctor"
        //doctorIdentityId.keyboardType = .asciiCapableNumberPad
        doctorIdentityId.delegate = self
        docPhone.delegate = self
        self.selectDoctorName.text = "Select Doctor"
        self.cityName.text = "Select Area"
        self.majorName.text = "Select Speciality"
        setupUpDateDoctorInfoViewController()
        getDoctorsPlan()
        
    }
    
    
    
    func setupUpDateDoctorInfoViewController(){
        for view in allViews {
            Helper.addBorderView(view: view, borderWidth: 1, cornerRadius: 5)
        }
        
        Helper.addBorderView(view: submitButton, borderWidth: 1, cornerRadius: 5)
    }
    
    
    
    func showDoctorListPickerView(){
        
        if doctorList.count == 0 {
            Helper.alertOneButton(message: "No doctor found\n Please try again", alertTitle: "Alert", titleButton: "Ok")
            return
        }else{
            var items = Array<FAPickerItem>()
            FAPickerView.setMainColor(UIColor.mainColor)
            
            for item in doctorList {
                
                let fullName = "\(item.nameFirst ?? "") \(item.nameSecond ?? "") \(item.nameThird ?? "") \(item.nameLast ?? "")"
                
                items.append(FAPickerItem.init(id: "\(item.id ?? -1)" , title: fullName))
            }
            FAPickerView.picker().show(with: NSMutableArray(array: items),
                                       selectedItem: selectedDoctor,
                                       filter: true,
                                       headerTitle: "Select Doctor",
                                       withCompletion: { (item) in
                                        self.selectedDoctor = item ?? FAPickerItem()
                                        self.doctorName.text = self.selectedDoctor.title
                                        self.selectDoctorName.text = self.selectedDoctor.title
                                        self.selectedDoctorId = Int(self.selectedDoctor.id) ?? 0
                                        
                                        let docId = Int(self.selectedDoctor.id)
                                        let mydoc = self.doctorList.filter({$0.id ?? -99 == docId})
                                        
                                        for item in mydoc {
                                            
                                            self.firstName.text = item.nameFirst ?? ""
                                            self.secondName.text = item.nameSecond ?? ""
                                            self.thirdName.text = item.nameThird ?? ""
                                            self.lastName.text = item.nameLast ?? ""
                                            self.cityName.text = item.city?.name ?? ""
                                            self.majorName.text = item.major?.name ?? ""
                                            self.doctorIdentityId.text = item.identityID ?? ""
                                            self.docPhone.text = item.mobileNo ?? "00962"
                                            self.classificationId.text = item.classification?.name
                                            self.visitsCount.text = "\(item.classification?.visitsCount ?? -99)"
                                            self.selectedCityId = item.cityID ?? -99
                                            self.visitsCountId = item.classification?.visitsCount ?? -99
                                            self.selectedMajorId = item.major?.id ?? -99
                                            self.selectedClassificationId = item.classification?.id ?? -99
                                            self.docNote.text = item.doctorNote ?? ""
                                            self.docEmail.text = item.doctorEmail ?? ""
                                        }
                                        
            }, cancel: {
                print("cancel")
            })
        }
    }
    
    
    func showCityPickerView(){
        
        if selectedDoctorId == -1 {
            selectDoctorName.shakeView()
            return
        }
        
        if cityList.count == 0 {
            Helper.alertOneButton(message: "No areas are available\n Please try again", alertTitle: "Alert", titleButton: "Ok")
            return
        }else{
            var items = Array<FAPickerItem>()
            FAPickerView.setMainColor(UIColor.mainColor)
            
            for item in cityList {
                items.append(FAPickerItem.init(id: "\(item.id ?? -1)" , title: item.name))
            }
            FAPickerView.picker().show(with: NSMutableArray(array: items),
                                       selectedItem: selectedCity,
                                       filter: true,
                                       headerTitle: "Select Area",
                                       withCompletion: { (item) in
                                        self.selectedCity = item ?? FAPickerItem()
                                        self.cityName.text = self.selectedCity.title
                                        self.selectedCityId = Int(self.selectedCity.id) ?? 0
                                        
            }, cancel: {
                print("cancel")
            })
        }
    }
    
    func showMajorPickerView(){
        
        if selectedDoctorId == -1 {
            selectDoctorName.shakeView()
            return
        }
        
        if majorList.count == 0 {
            Helper.alertOneButton(message: "No specialties are available\n Please try again", alertTitle: "Alert", titleButton: "Ok")
            return
        }else{
            var items = Array<FAPickerItem>()
            FAPickerView.setMainColor(UIColor.mainColor)
            
            for item in majorList {
                items.append(FAPickerItem.init(id: "\(item.id ?? -1)" , title: item.name))
            }
            FAPickerView.picker().show(with: NSMutableArray(array: items),
                                       selectedItem: selectedCity,
                                       filter: true,
                                       headerTitle: "Select Speciality",
                                       withCompletion: { (item) in
                                        self.selectedMajor = item ?? FAPickerItem()
                                        self.majorName.text = self.selectedMajor.title
                                        self.selectedMajorId = Int(self.selectedMajor.id) ?? 0
                                        
            }, cancel: {
                print("cancel")
            })
        }
    }
    
    
    func showClassificationPickerView(){
        
        if selectedDoctorId == -1 {
            selectDoctorName.shakeView()
            return
        }
        
        if clasificationList.count == 0 {
            Helper.alertOneButton(message: "No classifications are available\n Please try again", alertTitle: "Alert", titleButton: "Ok")
            return
        }else{
            var items = Array<FAPickerItem>()
            FAPickerView.setMainColor(UIColor.mainColor)
            
            for item in clasificationList {
                items.append(FAPickerItem.init(id: "\(item.visitsCount ?? -1)" , title: item.name))
            }
            FAPickerView.picker().show(with: NSMutableArray(array: items),
                                       selectedItem: selectedClassification,
                                       filter: false,
                                       headerTitle: "Select Classification",
                                       withCompletion: { (item) in
                                        self.selectedClassification = item ?? FAPickerItem()
                                        self.classificationId.text = self.selectedClassification.title
                                        self.selectedClassificationId = Int(self.selectedClassification.id) ?? 0
                                        self.visitsCount.text = "\(self.selectedClassification.id ?? "-99")"
                                        self.visitsCountId = Int(self.selectedClassification.id) ?? -99
                                        
            }, cancel: {
                print("cancel")
            })
        }
    }
    
    
    
    @IBAction func selectedDoctorActionButton(_ sender: UIButton) {
        showDoctorListPickerView()
    }
    
    @IBAction func selectCityActionButton(_ sender: UIButton) {
        showCityPickerView()
    }
    
    @IBAction func selectMajorActionButton(_ sender: UIButton) {
        showMajorPickerView()
    }
    
    @IBAction func selectClassificationActionButton(_ sender: UIButton) {
        showClassificationPickerView()
    }
    
    @IBAction func submitButtonActon(_ sender: UIButton) {
        
        if selectDoctorName.text == "Select Doctor" {
            selectDoctorName.shakeView()
            allViews[0].shakeView()
            return
        }
        
        if cityName.text == "Select Area" {
            cityName.shakeView()
            return
        }
        
        if majorName.text == "Select Speciality" {
            majorName.shakeView()
            return
        }
        
        
        if firstName.text?.isEmpty ?? true {
            firstName.shakeView()
            return
        }else if firstName.text?.isReallyEmpty ?? true {
            allViews[4].shakeView()
            return
        }
        
        
        //        if secondName.text?.isEmpty ?? true {
        //            secondName.shakeView()
        //            return
        //        }else if secondName.text?.isReallyEmpty ?? true {
        //            allViews[5].shakeView()
        //            return
        //        }
        
        //        if thirdName.text?.isEmpty ?? true {
        //            thirdName.shakeView()
        //            return
        //        }else if thirdName.text?.isReallyEmpty ?? true {
        //            allViews[6].shakeView()
        //            return
        //        }
        
        
        if lastName.text?.isEmpty ?? true {
            lastName.shakeView()
            return
        }else if lastName.text?.isReallyEmpty ?? true {
            allViews[7].shakeView()
            return
        }
        
        
        if classificationId.text?.isEmptyOrWhitespace ?? true {
            classificationId.shakeView()
            return
        }
        
        //        if docPhone.text?.isEmpty ?? true {
        //            docPhone.shakeView()
        //            return
        //        }else if docPhone.text?.isReallyEmpty ?? true {
        //            allViews[8].shakeView()
        //            return
        //        }
        
        
        if doctorIdentityId.text?.isEmpty ?? true {
            doctorIdentityId.shakeView()
            return
        }else if doctorIdentityId.text?.isReallyEmpty ?? true {
            allViews[9].shakeView()
            return
        }
        
        
        if self.docNote.text.isEmpty {
            allViews[13].shakeView()
            return
        }else if self.docNote.text.isReallyEmpty {
            allViews[13].shakeView()
            return
        }
        
        addDoctoerCallApi()
        
        
    }
    
    func getDoctorsPlan() {
        
        let dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        
        let parameters = [dateTime]
        
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: "PlanOfAction/GetDoctors/")
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<DoctorInfo>) in
            
            guard let strongSelf = self else {return}
            Helper.hideIndicator(view: strongSelf.view)
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                
                switch response.result {
                    
                case .failure(let error):
                    
                    print(error.localizedDescription)
                    
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                    }else{
                        Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                    
                case .success(let value):
                    print(value)                    
                    
                    if value.doctorData?.count == 0 {
                        Helper.alertWithCompletion(message: "No data is available. Please try again later", alertTitle: "Alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                        return
                    }
                    strongSelf.doctorList = value.doctorData ?? []
                    strongSelf.getCityListCallApi()
                }
            }
        }
    }
    
    
    
    func getCityListCallApi()  {
        
        Helper.showIndicator(view: self.view)
        
        let endPoint = ChangeRequestEndPoint.getCity
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<GetCity>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    
                    if value.cityData?.count == 0 {
                        Helper.alertWithCompletion(message: "No data is available. Please try again later", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                        return
                    }
                    strongSelf.cityList = value.cityData ?? []
                    strongSelf.getMajorListCallApi()
                case .failure(let error):
                    print(error.localizedDescription)
                    
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                    }else{
                        Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    
    func getMajorListCallApi()  {
        
        Helper.showIndicator(view: self.view)
        
        let endPoint = ChangeRequestEndPoint.getMajor
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<GetMajor>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    
                    if value.majorData?.count == 0 {
                        Helper.alertWithCompletion(message: "No data is available. Please try again later", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                        return
                    }
                    strongSelf.majorList = value.majorData ?? []
                    strongSelf.getClassificationCallApi()
                case .failure(let error):
                    print(error.localizedDescription)
                    
                    
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                    }else{
                        Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }
            }
        }
    }
    
    
    func getClassificationCallApi(){
        
        Helper.showIndicator(view: self.view)
        
        let endPoint = ChangeRequestEndPoint.getClassification
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<GetClassification>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    
                    if value.classificationData?.count == 0 {
                        Helper.alertWithCompletion(message: "No data is available. Please try again later", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                        return
                    }
                    strongSelf.clasificationList = value.classificationData ?? []
                case .failure(let error):
                    print(error.localizedDescription)
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                    }else{
                        Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    
    func resetView(){
        self.selectDoctorName.text = "Select Doctor"
        self.doctorName.text = "Doctor Name"
        self.cityName.text = "Select City"
        self.majorName.text = "Select Major"
        self.firstName.placeholder = "Enter first name"
        self.secondName.placeholder = "Enter second name"
        self.thirdName.placeholder = "Enter third name"
        self.lastName.placeholder = "Enter last name"
        self.doctorIdentityId.placeholder = "Enter Doctor Id"
        self.classificationId.placeholder = "Select Classification Id"
        self.visitsCount.placeholder = "Visits count"
    }
    
    func addDoctoerCallApi()  {
        
        Helper.showIndicator(view: self.view)
        
        var parameters = [String:Any]()
        
        parameters["ID"] = ""
        parameters["ChangeRequestTypeID"] = 3
        parameters["ChangeRequestStatusID"] = 1
        parameters["SenderUserID"] = LongTermManager.getUserInfo()?.userData?.id ?? -99
        parameters["ReceiverUserID"] = ""
        parameters["DoctorID"] = selectedDoctorId //
        parameters["NameFirst"] = firstName.text!
        parameters["NameSecond"] = secondName.text ?? ""
        parameters["NameThird"] = thirdName.text ?? ""
        parameters["NameLast"] = lastName.text!
        parameters["IdentityID"] = doctorIdentityId.text!
        parameters["MajorID"] = selectedMajorId
        parameters["CityID"] = selectedCityId
        parameters["ClassificationID"] = selectedClassificationId
        parameters["VisitsCount"] = visitsCountId
        parameters["MobileNo"] = docPhone.text ?? ""
        parameters["Note"] = docNote.text ?? ""
        parameters["Email"] = docEmail.text ?? ""
        
        let endPoint = ChangeRequestEndPoint.addDoctor(headerParams: parameters)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<AddDoctoer>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    Helper.alertWithCompletion(message: "The doctor information has been successfully updated", alertTitle: "successfully", titleButton: "Ok") {
                        strongSelf.navigationController?.popViewController(animated: true)
                    }
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                    }else{
                        Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    
    
    //MARK - UITextField Delegates
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For mobile numer validation
        if textField == doctorIdentityId || textField == docPhone {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789 ")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}


extension String {
    var isReallyEmpty: Bool {
        return self.trimmingCharacters(in: .whitespaces).isEmpty
    }
}
