//
//  ChangeRequestServicesVC.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/25/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class ChangeRequestServicesVC: UIViewController {
    
    fileprivate let CRCell = "ChangeRequestCell"
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var services = [ChangeRequestModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        title = "Change Request"
        collectionView.dataSource = self
        collectionView.delegate = self
        setupChangeRequestServices()
        
        services = [
            ChangeRequestModel(id: 1, name: "ADD Doctoer", imageName: "ic_add_doctor"),
            ChangeRequestModel(id: 2, name: "UpDate Doctoer", imageName: "ic_edit_doctor")
        ]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK:- Helping Methodes
    
    func setupChangeRequestServices(){
        collectionView.register(UINib(nibName: CRCell, bundle: nil), forCellWithReuseIdentifier: CRCell)
        setupTopCollectionViewCell()
    }
    
    
    func setupTopCollectionViewCell(){
        
        let flowLayout = UICollectionViewFlowLayout()
        //         let screenWidth = UIScreen.main.bounds.width
        //let coolectionWidth = screenWidth
        flowLayout.itemSize = CGSize(width: 165 , height: 223)
        flowLayout.scrollDirection = .vertical
        //         flowLayout.minimumLineSpacing = 0
        //         flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets(top: 10 , left: 0, bottom: 10, right: 0)
        //UIEdgeInsets.zero
        collectionView.isScrollEnabled = true
        collectionView.bounces = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.collectionViewLayout = flowLayout
        collectionView.backgroundColor = UIColor.clear
        collectionView.contentInset = UIEdgeInsets.zero
        collectionView.backgroundColor = UIColor.clear
        
    }
    
}


extension ChangeRequestServicesVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CRCell, for: indexPath) as? ChangeRequestCell else {return UICollectionViewCell()}
        let object = services[indexPath.row]
        cell.serviceName.text = object.name
        cell.imageName.image = UIImage(named: "\(object.imageName ?? "")")
        
        return cell
    }
    
}


extension ChangeRequestServicesVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            let storyboard = UIStoryboard(name: "ChangeRequest", bundle: nil)
            let addDoctoerVC = storyboard.instantiateViewController(withIdentifier: "AddDoctorViewController") as! AddDoctorViewController
            self.navigationController?.pushViewController(addDoctoerVC, animated: true)
        case 1:
            let storyboard = UIStoryboard(name: "ChangeRequest", bundle: nil)
            let addDoctoerVC = storyboard.instantiateViewController(withIdentifier: "UpDateDoctorInfoViewController") as! UpDateDoctorInfoViewController
            self.navigationController?.pushViewController(addDoctoerVC, animated: true)
        default:
            print("selawe")
        }
        
    }
}


struct ChangeRequestModel: Codable {
    let id: Int?
    let name: String?
    let imageName: String?
}
