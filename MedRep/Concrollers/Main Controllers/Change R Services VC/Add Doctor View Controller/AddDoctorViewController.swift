//
//  AddDoctorViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/25/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import FAPickerView

class AddDoctorViewController: UIViewController , UITextFieldDelegate {
    
    
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var majorView: UIView!
    @IBOutlet weak var majorLabel: UILabel!
    @IBOutlet var namesView: [UIView]!
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var secondName: UITextField!
    @IBOutlet weak var thirdName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var doctorIdentityId: UITextField!
    @IBOutlet weak var classificationId: UITextField!
    @IBOutlet weak var visitCount: UITextField!
    @IBOutlet weak var docMobile: UITextField!
    @IBOutlet weak var addNote: UITextView!
    @IBOutlet weak var docEmail: UITextField!
    
    var cityList = [CityList]()
    var selectedCity = FAPickerItem()
    var selectedCityId = -1
    
    
    var majorList = [MajorList]()
    var selectedMajor = FAPickerItem()
    var selectedMajorId = -1
    
    var clasificationList = [ClassificationList]()
    var selectedClassification = FAPickerItem()
    var selectedClassificationId = -1
    var visitsCountId = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 20) ?? UIFont.systemFont(ofSize: 20)]
        title = "Add Doctor"
        cityLabel.text = "Select Area"
        majorLabel.text = "Select Speciality"
        Helper.addBorderView(view: cityView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: majorView, borderWidth: 1, cornerRadius: 5)
        for view in namesView {
            Helper.addBorderView(view: view, borderWidth: 1, cornerRadius: 5)
        }
        doctorIdentityId.delegate = self
        docMobile.delegate = self
        getCityListCallApi()
    }
    
    
    
    func showCityPickerView(){
        
        if cityList.count == 0 {
            Helper.alertOneButton(message: "select Area", alertTitle: "Alert", titleButton: "Ok")
        }else{
            var items = Array<FAPickerItem>()
            FAPickerView.setMainColor(UIColor.mainColor)
            
            for item in cityList {
                items.append(FAPickerItem.init(id: "\(item.id ?? -1)" , title: item.name))
            }
            FAPickerView.picker().show(with: NSMutableArray(array: items),
                                       selectedItem: selectedCity,
                                       filter: true,
                                       headerTitle: "Select Area",
                                       withCompletion: { (item) in
                                        self.selectedCity = item ?? FAPickerItem()
                                        self.cityLabel.text = self.selectedCity.title
                                        self.selectedCityId = Int(self.selectedCity.id) ?? 0
            }, cancel: {
                print("cancel")
            })
        }
    }
    
    func showMajorPickerView(){
        
        if majorList.count == 0 {
            Helper.alertOneButton(message: "Select Speciality", alertTitle: "Alert", titleButton: "Ok")
        }else{
            var items = Array<FAPickerItem>()
            FAPickerView.setMainColor(UIColor.mainColor)
            
            for item in majorList {
                items.append(FAPickerItem.init(id: "\(item.id ?? -1)" , title: item.name))
            }
            FAPickerView.picker().show(with: NSMutableArray(array: items),
                                       selectedItem: selectedCity,
                                       filter: true,
                                       headerTitle: "Select Speciality",
                                       withCompletion: { (item) in
                                        self.selectedMajor = item ?? FAPickerItem()
                                        self.majorLabel.text = self.selectedMajor.title
                                        self.selectedMajorId = Int(self.selectedMajor.id) ?? 0
            }, cancel: {
                print("cancel")
            })
        }
    }
    
    
    func showClassificationPickerView(){
    
        
        if clasificationList.count == 0 {
            Helper.alertOneButton(message: "select Classification", alertTitle: "Alert", titleButton: "Ok")
            return
        }else{
            var items = Array<FAPickerItem>()
            FAPickerView.setMainColor(UIColor.mainColor)
            
            for item in clasificationList {
                items.append(FAPickerItem.init(id: "\(item.visitsCount ?? 0)" , title: item.name,imageURL: "\(item.id ?? -1)", thumb: UIImage()))
            }
            FAPickerView.picker().show(with: NSMutableArray(array: items),
                                       selectedItem: selectedClassification,
                                       filter: false,
                                       headerTitle: "select Classification",
                                       withCompletion: { (item) in
                                        self.selectedClassification = item ?? FAPickerItem()
                                        self.classificationId.text = self.selectedClassification.title
                                        self.selectedClassificationId = Int(item?.imageURL ?? "0") ?? 0
                                        self.visitCount.text = "\(self.selectedClassification.id ?? "-99")"
                                        self.visitsCountId = Int(self.selectedClassification.id) ?? -99
                                        
            }, cancel: {
                print("cancel")
            })
        }
    }
    
    
    
    
    @IBAction func selectCityActionButton(_ sender: UIButton) {
        showCityPickerView()
    }
    
    
    
    @IBAction func selectedMajorActionButton(_ sender: UIButton) {
        showMajorPickerView()
    }
    
    
    
    @IBAction func selectedClassificationActionButton(_ sender: UIButton) {
        showClassificationPickerView()
    }
    
    
//    @IBOutlet weak var cityLabel: UILabel!
//    @IBOutlet weak var majorLabel: UILabel!
//
//    @IBOutlet weak var firstName: UITextField!
//    @IBOutlet weak var secondName: UITextField!
//    @IBOutlet weak var thirdName: UITextField!
//    @IBOutlet weak var lastName: UITextField!
    
//    @IBOutlet weak var doctorIdentityId: UITextField!
//    @IBOutlet weak var classificationId: UITextField!
//    @IBOutlet weak var visitCount: UITextField!
    
//
//    var selectedCityId = -1
//
//    var selectedMajorId = -1
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
        
//        if firstName.text?.isEmpty ?? true {
//             firstName.shakeView()
//             return
//         }else if firstName.text?.isReallyEmpty ?? true {
//             allViews[4].shakeView()
//             return
//         }
        
        
        if selectedCityId == -1 {
            cityLabel.shakeView()
            return
        }
        
        if selectedMajorId == -1 {
            majorLabel.shakeView()
            return
        }
        
        
        if firstName.text?.isEmpty ?? true {
            firstName.shakeView()
            return
        }else if firstName.text?.isReallyEmpty ?? true {
            self.namesView[2].shakeView()
            return
        }
        
//        if secondName.text?.isEmptyOrWhitespace ?? true {
//            secondName.shakeView()
//            return
//        }
        
        
//        if thirdName.text?.isEmptyOrWhitespace ?? true {
//            thirdName.shakeView()
//            return
//        }
        
        if lastName.text?.isEmpty ?? true {
            lastName.shakeView()
            return
        }else if lastName.text?.isReallyEmpty ?? true {
            self.namesView[5].shakeView()
            return
        }
        
        
        
//        if doctorIdentityId.text?.isEmptyOrWhitespace ?? true {
//            doctorIdentityId.shakeView()
//            return
//        }
        
        
        
        if classificationId.text?.isEmptyOrWhitespace ?? true {
            classificationId.shakeView()
            return
        }
        
        if visitCount.text?.isEmptyOrWhitespace ?? true {
            visitCount.shakeView()
            return
        }
        
        
//        if docMobile.text?.isEmpty ?? true {
//
//        }
        
        addDoctoerCallApi()
    }
    
    //    @IBOutlet weak var doctorIdentityId: UITextField!
    //    @IBOutlet weak var classificationId: UITextField!
    //    @IBOutlet weak var visitCount: UITextField!
    
    //
    //    "ID": 0,
    //    "ChangeRequestTypeID": 3,
    //    "ChangeRequestStatusID": 1,
    //    "SenderUserID": "user id" ,
    //    "ReceiverUserID": "3",
    //    "DoctorID": "",
    //    "NameFirst": "Ali",
    //    "NameSecond": "Mustafa",
    //    "NameThird": "Khalil",
    //    "NameLast": "العنبتاوي",
    //    "IdentityID": "12332143434",
    //    "MajorID": "from piker",
    //    "CityID": 1,
    //    "ClassificationID": 1,
    //    "VisitsCount": "from classification",
    
//    GetCity
    
    func getCityListCallApi()  {
        
        Helper.showIndicator(view: self.view)
        
        let endPoint = ChangeRequestEndPoint.getCity
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<GetCity>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    if value.cityData?.count == 0 {
                        Helper.alertWithCompletion(message: "No data is available. Please try again later", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                        return
                    }
                    strongSelf.cityList = value.cityData ?? []
                    strongSelf.getMajorListCallApi()
                case .failure(let error):
                    print(error.localizedDescription)

                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                         Helper.singOutCallApi()
                     }else{
                         Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                             strongSelf.navigationController?.popViewController(animated: true)
                         }
                     }
                }
            }
        }
    }
    
    
    func getMajorListCallApi()  {
        
        Helper.showIndicator(view: self.view)
        
        let endPoint = ChangeRequestEndPoint.getMajor
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<GetMajor>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    if value.majorData?.count == 0 {
                        Helper.alertWithCompletion(message: "No data is available. Please try again later", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                        return
                    }
                    strongSelf.majorList = value.majorData ?? []
                    strongSelf.getClassificationCallApi()
                case .failure(let error):
                    print(error.localizedDescription)
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                         Helper.singOutCallApi()
                     }else{
                         Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                             strongSelf.navigationController?.popViewController(animated: true)
                         }
                     }
                }
            }
        }
    }
    
    
    func getClassificationCallApi(){
        
        Helper.showIndicator(view: self.view)
        
        let endPoint = ChangeRequestEndPoint.getClassification
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<GetClassification>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    if value.classificationData?.count == 0 {
                        Helper.alertWithCompletion(message: "No data is available. Please try again later", alertTitle: "alert", titleButton: "Ok,") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                        return
                    }
                    strongSelf.clasificationList = value.classificationData ?? []
                case .failure(let error):
                    print(error.localizedDescription)
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                         Helper.singOutCallApi()
                     }else{
                         Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                             strongSelf.navigationController?.popViewController(animated: true)
                         }
                     }
                }
            }
        }
    }
    
    
 
    
    func addDoctoerCallApi()  {
        
        Helper.showIndicator(view: self.view)
        
        var parameters = [String:Any]()
        
        parameters["ID"] = ""
        parameters["ChangeRequestTypeID"] = 3
        parameters["ChangeRequestStatusID"] = 1
        parameters["SenderUserID"] = LongTermManager.getUserInfo()?.userData?.id ?? -99
        parameters["ReceiverUserID"] = ""
        parameters["DoctorID"] = "" // add doc
        parameters["NameFirst"] = firstName.text!
        parameters["NameSecond"] = secondName.text ?? ""
        parameters["NameThird"] = thirdName.text ?? ""
        parameters["NameLast"] = lastName.text!
        parameters["IdentityID"] = doctorIdentityId.text ?? ""
        parameters["MajorID"] = selectedMajorId
        parameters["CityID"] = selectedCityId
        parameters["MobileNo"] = docMobile.text
        parameters["ClassificationID"] = selectedClassificationId
        parameters["VisitsCount"] = visitsCountId
        parameters["Note"] = addNote.text ?? ""
        parameters["Email"] = docEmail.text ?? ""
        
        let endPoint = ChangeRequestEndPoint.addDoctor(headerParams: parameters)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<AddDoctoer>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    Helper.alertWithCompletion(message: "Doctor added successfully", alertTitle: "Successfully", titleButton: "Ok") {
                        strongSelf.navigationController?.popViewController(animated: true)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                         Helper.singOutCallApi()
                     }else{
                         Helper.alertWithCompletion(message: "\(error.localizedDescription)", alertTitle: "alert", titleButton: "Ok,") {
                             strongSelf.navigationController?.popViewController(animated: true)
                         }
                     }
                }
            }
        }
    }
    
    
    //MARK - UITextField Delegates
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For mobile numer validation
        if textField == doctorIdentityId  || textField == docMobile {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789 ")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    
    
}
