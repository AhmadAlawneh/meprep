//
//  ChangeRequestCell.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/25/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class ChangeRequestCell: UICollectionViewCell {
    
    
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var imageName: UIImageView!
    @IBOutlet weak var imageView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        Helper.addBorderView(view: imageView, borderWidth: 1, cornerRadius: 5)
    }

}
