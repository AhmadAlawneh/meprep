//
//  AddActivity.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/13/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
// MARK: - AddActivity
struct AddActivity: Codable {
    let isSuccess: Bool
    let statusCode: Int
    let statusDescription, data: String

    enum CodingKeys: String, CodingKey {
        case isSuccess = "IsSuccess"
        case statusCode = "StatusCode"
        case statusDescription = "StatusDescription"
        case data = "Data"
    }
}
