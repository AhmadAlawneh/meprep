//
//  AddActivityViewController.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/13/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import FAPickerView
import DateTimePicker

class AddActivityViewController: UIViewController {
 
    @IBOutlet var activityLable: UILabel!
    @IBOutlet var activityName: UITextField!
    @IBOutlet var startDateLabel: UILabel!
    @IBOutlet var endDateLable: UILabel!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var reminder: UISwitch!
    @IBOutlet var timeOutLabel: UILabel!
    @IBOutlet weak var noteText: UITextView!
    @IBOutlet var activityButton: UIButton!
    @IBOutlet var startDateButton: UIButton!
    @IBOutlet var endDateButton: UIButton!
    @IBOutlet var timeOutButton: UIButton!
    @IBOutlet var allViews: [UIView]!
    
    
    var selectedTypeItem = FAPickerItem()
    var selectedTimeOutItem = FAPickerItem()
    var switchOnOrOff = false
    var items = Array<FAPickerItem>()
    var reloadDataDelaget:ReloadData?
    var buttonTage = -1
    
    
    var startDate = Date()
    var endDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for view in allViews{
            Helper.addBorderView(view: view, borderWidth: 1, cornerRadius: 5)
        }
        userNameLabel.text = LongTermManager.getUserInfo()?.userData?.name
        startDateButton.isEnabled = false
        endDateButton.isEnabled = false
        timeOutButton.isEnabled = false
    }
    
    @IBAction func selectStartDatePressed(_ sender: Any) {
        openDatePiker(tag:1)
    }
    
    @IBAction func selectEndDatePressed(_ sender: Any) {
        openDatePiker(tag:2)
    }
    
    @IBAction func selectActivityTypePressed(_ sender: Any) {
        items.removeAll()
        getData(string: ApiURLs.GetActivityType)
        
    }
    @IBAction func selectTimeOutPressed(_ sender: Any) {
        items.removeAll()
        getData(string: ApiURLs.GetTimeout)
    }
    
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func openDatePiker(tag:Int){
        hideKeyboard()
        //        let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
        //        let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
        //        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        
        
        let min = Date().addingTimeInterval(-60 * 60 * 24 * 30)
        let max = Date().addingTimeInterval(60 * 60 * 24 * 30)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
//        let picker = DateTimePicker.create()
        picker.tag = tag
        //        picker.dateFormat = "hh:mm:ss aa dd/MM/YYYY"
        picker.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        picker.isDatePickerOnly = true
        picker.includesMonth = true
        picker.includesSecond = true
        picker.is12HourFormat = false
        //        picker.isTimePickerOnly = true
        picker.highlightColor = UIColor(red: 0.30, green: 0.52, blue: 1.00, alpha: 1.00)
        picker.doneButtonTitle = "!! Select Date !!"
        picker.doneBackgroundColor = UIColor(red: 0.30, green: 0.52, blue: 1.00, alpha: 1.00)
        picker.customFontSetting = DateTimePicker.CustomFontSetting(selectedDateLabelFont: .boldSystemFont(ofSize: 30))
        
        
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.title = formatter.string(from: date)
            picker.tag = tag
            
            if picker.tag == 1 {
                if let outDate = formatter.date(from: picker.selectedDateString) {
                    self.startDate = outDate
                    print(self.startDate)
                    self.startDateLabel.text = "\(self.startDate.year )-\(self.addZeroIf(number:self.startDate.month))-\(self.addZeroIf(number:self.startDate.day)) \(self.addZeroIf(number:self.startDate.hour)):\(self.addZeroIf(number:self.startDate.minute))"
                    self.endDateButton.isEnabled = true
                    print(self.startDate)
                }
                
            }else if picker.tag == 2{
                if let outDate = formatter.date(from: picker.selectedDateString) {
                    self.endDate = outDate
                    print(self.endDate)
                    self.endDateLable.text = "\(self.endDate.year )-\(self.addZeroIf(number:self.endDate.month))-\(self.addZeroIf(number:self.endDate.day)) \(self.addZeroIf(number:self.endDate.hour)):\(self.addZeroIf(number:self.endDate.minute))"
                    self.timeOutButton.isEnabled = true
                    print(self.startDate)
                }
            }
        }
        picker.delegate = self
        picker.show()
    }
    
    
    
    @IBAction func reminderOnOROfff(_ sender: UISwitch) {
        if sender.isOn {
            switchOnOrOff = true
            print(switchOnOrOff)
        }else{
            switchOnOrOff = false
            print(switchOnOrOff)
        }
    }
    
    
    func getData(string:String) {
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        
        let url = conectURL(parameters: parameters, url: string)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<AddPlanner>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    print(value)
                    if string == ApiURLs.GetTimeout {
                        for val in value.timeOut ?? TimeOutData() {
                            strongSelf.items.append(FAPickerItem(id: "\(val.id)", title: val.name))
                        }
                        
                        FAPickerView.setMainColor(.mainColor)
                        FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                          selectedItem: strongSelf.selectedTimeOutItem,
                                                          filter: false,
                                                          headerTitle: "Select one item",
                                                          complete: { (item:FAPickerItem?) in
                                                            if let item = item {
                                                                strongSelf.selectedTimeOutItem = item
                                                                strongSelf.timeOutLabel.text = item.title
                                                                
                                                            }
                        }, cancel: {
                            
                        })
                    }else if string == ApiURLs.GetActivityType{
                        for val in value.timeOut ?? TimeOutData() {
                            strongSelf.items.append(FAPickerItem(id: "\(val.id)", title: val.name))
                        }
                        
                        FAPickerView.setMainColor(.mainColor)
                        FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                          selectedItem: strongSelf.selectedTypeItem,
                                                          filter: false,
                                                          headerTitle: "Select one item",
                                                          complete: { (item:FAPickerItem?) in
                                                            if let item = item {
                                                                strongSelf.selectedTypeItem = item
                                                                strongSelf.activityLable.text = item.title
                                                                strongSelf.startDateButton.isEnabled = true
                                                            }
                        }, cancel: {
                            
                        })
                    }
                }
            }
        }
        
    }
    @IBAction func addActivityPressed(_ sender: Any) {
        
        if activityLable.text?.isEmpty ?? true {
            self.allViews[0].shakeView()
            return
        }else if activityLable.text?.isReallyEmpty ?? true {
            self.allViews[0].shakeView()
            return
        }
        
        if activityName.text?.isEmpty ?? true {
            self.allViews[1].shakeView()
            return
        }else if activityName.text?.isReallyEmpty ?? true {
            self.allViews[1].shakeView()
            return
        }
        
        if startDateLabel.text?.isEmpty ?? true {
            self.allViews[2].shakeView()
            return
        }else if startDateLabel.text?.isReallyEmpty ?? true {
            self.allViews[2].shakeView()
            return
        }
        
        if endDateLable.text?.isEmpty ?? true {
            self.allViews[3].shakeView()
            return
        }else if endDateLable.text?.isReallyEmpty ?? true {
            self.allViews[3].shakeView()
            return
        }
        
        addActivity()
    }
    
    
    func addActivity() {
            
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSZ"
            let now = df.string(from: Date())
            var parameters = [String:Any]()
            
            
            parameters["ID"] =  "0"
            parameters["Name"] =  activityName.text
            parameters["ActivityTypeID"] =  selectedTypeItem.id
            parameters["FromDate"] =  "\(startDate.year)-\(addZeroIf(number: startDate.month))-\(addZeroIf(number: startDate.day))T\(addZeroIf(number: startDate.hour)):\(addZeroIf(number: startDate.minute)):\(addZeroIf(number: 0))"
            parameters["ToDate"] =  "\(endDate.year)-\(addZeroIf(number: endDate.month))-\(addZeroIf(number: endDate.day))T\(addZeroIf(number: endDate.hour)):\(addZeroIf(number: endDate.minute)):\(addZeroIf(number: 0))"
            parameters["TimeoutID"] = selectedTimeOutItem.id
            parameters["ProductID"] = "1"
            parameters["Note"] = noteText.text
            parameters["IsDeleted"] = "false"
            if switchOnOrOff{
                parameters["RemindMe"] = true
            }else{
                parameters["RemindMe"] = false
            }
            parameters["EntryUserID"] = LongTermManager.getUserInfo()?.userData?.id
            parameters["ActivityStatusID"] = "1"
            parameters["EntryDate"] = "\(now)"
            
            Helper.showIndicator(view: self.view)
            let endPoint = RequestHandler.post(parmeters: parameters, url: ApiURLs.InsertUserActivity)
            APIHandler.request(endpoint: endPoint) { [weak self](response: Response<AddActivity>) in
                
                guard let strongSelf = self else {return}
                DispatchQueue.main.async {
                    switch response.result {
                        
                    case .failure( _):
                        let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                        if res?.statusCode == 1{
                            Helper.hideIndicator(view: strongSelf.view)
                            Helper.singOutCallApi()
                        }else if res?.statusCode == 3 {
                            Helper.hideIndicator(view: strongSelf.view)
                            Helper.alertWithCompletion(message: "Invalid Entered Date. \n The start date is greater OR equal than the end date , \n Please enter a valid date".localized, alertTitle: "Warning", titleButton: "Ok") {
                                
                            }
                            
                        }else if res?.statusCode == 6 {
                            Helper.hideIndicator(view: strongSelf.view)
                            strongSelf.showMsg(title: "Alert".localized, msg: "There is already visit or user activity in this timer")
                        }else{
                            Helper.hideIndicator(view: strongSelf.view)
                            Helper.alertWithCompletion(message: "wrongConnection".localized, alertTitle: "Alert".localized, titleButton: "Ok") {
                                strongSelf.dismiss(animated: true, completion: nil)
                            }
                        }
                        
                    case .success(let value):
                        print(value)
                        strongSelf.removeSpinnerWithAction {
                            strongSelf.showMsgithAction(title: "Alert".localized, msg: "addedSuccess".localized, completion: {
                                strongSelf.dismiss(animated: true, completion: {
                                    strongSelf.reloadDataDelaget?.ReloadCalendar()
                                    
                                })
                            })
                        }
                    }
                }
            }
        }
}

extension String {
    
    func twilvTo() ->String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        let date12 = dateFormatter.date(from: self)!
        
        dateFormatter.dateFormat = "hh"
        let date22 = dateFormatter.string(from: date12)
        
        print(date22)
        return date22
    }
    
}


extension AddActivityViewController: DateTimePickerDelegate {
    
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if picker.tag == 1 {
            if let outDate = formatter.date(from: picker.selectedDateString) {
                self.startDate = outDate
                print(self.startDate)
                self.startDateLabel.text = "\(self.startDate.year )-\(self.addZeroIf(number:self.startDate.month))-\(self.addZeroIf(number:self.startDate.day)) \(self.addZeroIf(number:self.startDate.hour)):\(self.addZeroIf(number:self.startDate.minute))"
                self.endDateButton.isEnabled = true
                print(self.startDate)
            } }else if picker.tag == 2{
            if let outDate = formatter.date(from: picker.selectedDateString) {
                self.endDate = outDate
                print(self.endDate)
                self.endDateLable.text = "\(self.endDate.year )-\(self.addZeroIf(number:self.endDate.month))-\(self.addZeroIf(number:self.endDate.day)) \(self.addZeroIf(number:self.endDate.hour)):\(self.addZeroIf(number:self.endDate.minute))"
                self.timeOutButton.isEnabled = true
                print(self.startDate)
            }
        }
    }
}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}
