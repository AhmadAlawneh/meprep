//
//  TodayActivityTableViewCell.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/13/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class TodayActivityTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var activityName: UILabel!
    
    var activityDay : ActivityElement? {
        didSet{
            if let activityObject = activityDay {
                self.updateView(activityObject: activityObject)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func updateView(activityObject: ActivityElement){
        activityName.text = activityObject.name
    }
    
}
