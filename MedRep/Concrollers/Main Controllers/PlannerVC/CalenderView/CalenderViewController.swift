import UIKit
import CalendarKit
import DateToolsSwift


protocol ReloadDataForCalender {
    func reloadDataForCalendar()
}

class CalenderViewController: DayViewController, DatePickerControllerDelegate,ReloadDataForCalender {
  
    
    
    var allEvents:Activity?
    var userActivitys:Activity?
    let currentDateTime = Date()
    var dateTime = ""
    
//    var timeline = TimelineStyle()

    lazy var customCalendar: Calendar = {
        let customNSCalendar = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!
        customNSCalendar.timeZone = .current
//        customNSCalendar.tim
        let calendar = customNSCalendar as Calendar
        return calendar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        getActivitys(date: dateTime, withDoctors: "true")
        dayView.autoScrollToFirstEvent = true
    }
    
    func reloadDataForCalendar() {
        getActivitys(date: dateTime, withDoctors: "true")

    }

    
    override func loadView() {
        calendar = customCalendar
        dayView = DayView(calendar: calendar)
        var calendarStyle = CalendarStyle()
        calendarStyle.timeline.verticalDiff = 300
        calendarStyle.timeline.timeIndicator.font = UIFont(name: "Roboto-Bold", size: 15) ?? UIFont.boldSystemFont(ofSize: 11)
        calendarStyle.timeline.allDayStyle.allDayFont = UIFont(name: "Roboto-Bold", size: 15) ?? UIFont.boldSystemFont(ofSize: 11)
        calendarStyle.timeline.font = UIFont(name: "Roboto-Bold", size: 15) ?? UIFont.boldSystemFont(ofSize: 11)
        dayView.updateStyle(calendarStyle)
        view = dayView
    }
    

    func getDate(date:String) -> Date {
        let dateFormatter = DateFormatter()
        
        if date.contains(".") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        dateFormatter.timeZone = .current
        let dateT = dateFormatter.date(from: date)
        if dateT == nil {
            let splitDateAndTime = date.components(separatedBy: "T")
            let dateValueArr = splitDateAndTime[0].components(separatedBy: "-")
            let timeValueArr = splitDateAndTime[1].components(separatedBy: ":")
            
            var dateComponents = DateComponents()
            dateComponents.year = Int(addZeroIf(number: Int(dateValueArr[0]) ?? 1970))
            dateComponents.month = Int(addZeroIf(number: Int(dateValueArr[1]) ?? 1))
            dateComponents.day = Int(addZeroIf(number: Int(dateValueArr[2]) ?? 1))
            dateComponents.timeZone = .current
            dateComponents.hour = Int(addZeroIf(number: Int(timeValueArr[0]) ?? 0))
            dateComponents.minute = Int(addZeroIf(number: Int(timeValueArr[1]) ?? 0))
            dateComponents.second = Int(addZeroIf(number: Int(timeValueArr[2]) ?? 0))
            // Create date from components
            let userCalendar = Calendar.current // user calendar
            let someDateTime = userCalendar.date(from: dateComponents)
            return someDateTime ?? Date()
        }else{
            
            return dateT ?? Date()
        }
    }
    func getCurrentTimeZone() -> String {
            let localTimeZoneAbbreviation: Int = TimeZone.current.secondsFromGMT()
            let items = (localTimeZoneAbbreviation / 3600)
            return "\(items)"
    }
    
    
    func getActivitys(date:String,withDoctors:String) {
        
        
        let parameters = [dateTime,withDoctors]
        if withDoctors == "true"{
            Helper.showIndicator(view: self.view)
        }
        let url = conectURL(parameters: parameters, url: ApiURLs.GetUserActivities)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<UserActivity>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                if withDoctors == "true"{
                    Helper.hideIndicator(view: strongSelf.view)
                }
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                case .success(let value):
                    print(value)
                    if withDoctors == "true"{
                        strongSelf.allEvents = value.userData
                        strongSelf.reloadData()
                    }else{
                        strongSelf.userActivitys = value.userData
                        strongSelf.getActivitys(date: strongSelf.dateTime,withDoctors: "true")
                    }
                }
            }
        }
    }
    
    
    
    func datePicker(controller: DatePickerController, didSelect date: Date?) {
        if let date = date {
            dayView.state?.move(to: date)
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: EventDataSource
    
    override func eventsForDate(_ date: Date) -> [EventDescriptor] {
        var events = [Event]()
        
   
        
        for item in allEvents ?? Activity() {
            let event = Event()
            
//            let duration = getDate(date: item.fromDate ?? "\(Date())").timeIntervalSinceReferenceDate -  getDate(date: item.toDate ?? "\(Date())").timeIntervalSinceReferenceDate
//            let datePeriod = TimePeriod(beginning: getDate(date: item.fromDate ?? "\(Date())"), duration:duration + 2000 )
            
            event.startDate = getDate(date: item.fromDate ?? "\(Date())")
            event.endDate = getDate(date: item.toDate ?? "\(Date())")
            
            
//              event.startDate = model.startDate
//              event.endDate = model.endDate
              // Add info: event title, subtitle, location to the array of Strings
//              var info = [item.title, item.location]
//              info.append("\(datePeriod.beginning!.format(with: "HH:mm")) - \(datePeriod.end!.format(with: "HH:mm"))")
            
//            .format(with: "HH.mm")
            
            
            
            let startArr = item.fromDate?.components(separatedBy: "T")
            let endArr = item.toDate?.components(separatedBy: "T")
            if item.activityTypeID == 0 {
                event.font = UIFont(name: "Roboto-Regular", size: 10)!
                event.text = item.name ?? ""
                event.text = "Doctor Name : \(item.name ?? "") -- SDate: \(startArr?[0] ?? "") - EDate: \(endArr?[0] ?? "")"
            }else{
                event.text = item.name ?? ""
                event.font = UIFont(name: "Roboto-Regular", size: 10)!
                event.text = "Activity Name : \(item.name ?? "") -- SDate: \(startArr?[0] ?? "") - EDate: \(endArr?[0] ?? "")"
            }
            
            event.color = hexStringToUIColor(hex: item.activityStatus?.color ?? "#000")
            event.isAllDay = false
            // Event styles are updated independently from CalendarStyle
            // hence the need to specify exact colors in case of Dark style
            if #available(iOS 12.0, *) {
                if traitCollection.userInterfaceStyle == .dark {
                    event.textColor = textColorForEventInDarkTheme(baseColor: event.color)
                    event.backgroundColor = event.color.withAlphaComponent(0.6)
                }
            }
            events.append(event)
            event.userInfo = item.id
        }
        return events
    }
    
    private func textColorForEventInDarkTheme(baseColor: UIColor) -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        baseColor.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s * 0.3, brightness: b, alpha: a)
    }
    
    // MARK: DayViewDelegate
    
    override func dayViewDidSelectEventView(_ eventView: EventView) {
        guard let descriptor = eventView.descriptor as? Event else {
            return
        }
        print("Event has been selected: \(descriptor) \(String(describing: descriptor.userInfo))")
        let storyboard = UIStoryboard(name: "Planner", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ShowEventDetViewControllerID") as! ShowEventDetViewController
        vc.reloadDataForCalender = self
        vc.activityElement = allEvents?.first(where: {"\($0.id ?? 0)" == "\(descriptor.userInfo ?? 0)"})
        
        self.present(vc, animated: true, completion: nil)
    }
    
    override func dayViewDidLongPressEventView(_ eventView: EventView) {
        guard let descriptor = eventView.descriptor as? Event else {
            return
        }
        print("Event has been longPressed: \(descriptor) \(String(describing: descriptor.userInfo))")
    }
    
    override func dayView(dayView: DayView, willMoveTo date: Date) {
        print("DayView = \(dayView) will move to: \(date)")
        dateTime = "\(date.year)\(addZeroIf(number: date.month))\(addZeroIf(number: date.day))\(addZeroIf(number: date.hour))\(addZeroIf(number: date.minute))\(addZeroIf(number: 0))"
        getActivitys(date: dateTime, withDoctors: "true")
        
    }
    
    override func dayView(dayView: DayView, didMoveTo date: Date) {
        print("DayView = \(dayView) did move to: \(date)")
    }
    
    override func dayView(dayView: DayView, didLongPressTimelineAt date: Date) {
        print("Did long press timeline at date \(date)")
    }
    
    
}

extension CalenderViewController{
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension Date {
    
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
    
}
