//
//  PlannerViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/10/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import CalendarKit

protocol ReloadData {
    func ReloadCalendar()
}

class PlannerViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ReloadData {
    
    
    @IBOutlet var customCalView: UIView!
    @IBOutlet var calendarUIView: UIView!
    @IBOutlet var tableView: UITableView!
    
    
    var firstTime = true
    var userActivitys:Activity?
    let currentDateTime = Date()
    var dateTime = ""
    var callOneTime = true
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        
        getActivitys(date: dateTime)
        setTableViewData()
        
        // Do any additional setup after loading the view.
    }
    func ReloadCalendar() {
        for view in calendarUIView.subviews{
            view.removeFromSuperview()
        }
        let controller = CalenderViewController()
        controller.navigationController?.isNavigationBarHidden = true
        
        addChild(controller)
        controller.view.frame = calendarUIView.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: calendarUIView.frame.size.width, height: calendarUIView.frame.size.height))
        calendarUIView.addSubview(controller.view)
        controller.didMove(toParent: self)
        getActivitys(date: dateTime)
    }
    
    
    func setTableViewData()  {
        tableView.delegate =  self
        tableView.dataSource =  self
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "TodayActivityTableViewCell", bundle: nil), forCellReuseIdentifier: "TodayActivityTableViewCell")
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        firstTime = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        customCalView.roundedLeftTopBottom()
        
    }
    
    //MARK:- Action && Selector
    
    @IBAction func homePressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addActivityPRessed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Planner", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddActivityViewControllerID") as! AddActivityViewController
        vc.reloadDataDelaget = self
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension PlannerViewController {
    
    func getStringTime(date:String) -> Date? {
        let dateFormatter = DateFormatter()
        
        if date.contains(".") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        let dateT = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "h:mma"
        return dateT ?? Date()
        
    }
    
    func timeFormatter(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "style.timeHourSystem.format"
        return formatter.string(from: date)
    }
    
    func formatter(date: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter.date(from: date) ?? Date()
    }
}

extension PlannerViewController {
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    func getActivitys(date:String) {
        
        
        let parameters = [dateTime,"false"]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetUserActivities)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<UserActivity>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    print(value)
                    let controller = CalenderViewController()
                    strongSelf.addChild(controller)
                    controller.view.frame = strongSelf.calendarUIView.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: strongSelf.calendarUIView.frame.size.width, height: strongSelf.calendarUIView.frame.size.height))
                    strongSelf.calendarUIView.addSubview(controller.view)
                    controller.didMove(toParent: self)
                    strongSelf.userActivitys = value.userData
                    strongSelf.tableView.reloadData()
                }
            }
        }
    }
}


//MARK:- Table View DataSource && Delegate
extension PlannerViewController{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = userActivitys?.count
        if count == 0 {
            tableView.setEmptyView(title: "No Activity found", message: nil)
        }else{
            tableView.restore()
        }
        return count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TodayActivityTableViewCell", for: indexPath) as? TodayActivityTableViewCell else {return UITableViewCell()}
        let activityObject = userActivitys?[indexPath.row]
        cell.activityDay = activityObject
        //        cell.textLabel?.text = userActivitys?[indexPath.row].name
        //        cell.textLabel?.textColor = UIColor.textColor
        //        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return  cell
    }
}


extension String {
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date!)
    }
}
