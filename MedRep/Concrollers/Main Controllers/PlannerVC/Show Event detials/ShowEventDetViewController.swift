//
//  ShowEventDetViewController.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/12/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
class ShowEventDetViewController: UIViewController {
    
    var activityElement:ActivityElement?
    
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var textValue: UILabel!
    @IBOutlet var startValue: UILabel!
    @IBOutlet var endValue: UILabel!
    @IBOutlet var noteValue: UILabel!
    @IBOutlet var allViews: [UIView]!
    var reloadDataForCalender:ReloadDataForCalender?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for view in allViews{
            Helper.addBorderView(view: view, borderWidth: 1, cornerRadius: 5)
        }
        
//        textValue.text = "\(activityElement?.activityType?.name ?? "")"
//        startValue.text = getStringDate(date: activityElement?.fromDate ?? "")
//        endValue.text = getStringDate(date: activityElement?.toDate ?? "")
//        noteValue.text = "\(activityElement?.note ?? "")"
        
        if activityElement?.activityTypeID == 0 {
            labelType.text = "Doctor Name"
            textValue.text = "\(activityElement?.name ?? "")"
            startValue.text = getStringDate(date: activityElement?.fromDate ?? "")
            endValue.text = getStringDate(date: activityElement?.toDate ?? "")
            noteValue.text = "\(activityElement?.note ?? "")"
            deleteButton.isHidden = false
            
//            activityElement?.name
        }else{
            labelType.text = "Activity Name"
            
            textValue.text = "\(activityElement?.activityType?.name ?? "")"
            startValue.text = getStringDate(date: activityElement?.fromDate ?? "")
            endValue.text = getStringDate(date: activityElement?.toDate ?? "")
            noteValue.text = "\(activityElement?.note ?? "")"
            deleteButton.isHidden = false

            
        }
        
        
//        let startArr = item.fromDate?.components(separatedBy: "T")
//        let endArr = item.toDate?.components(separatedBy: "T")
//        if item.activityTypeID == 0 {
//            event.text = item.name ?? ""
//            event.text = "Doctor Name : \(item.name ?? "") -- SDate: \(startArr?[0] ?? "") - EDate: \(endArr?[0] ?? "")"
//        }else{
//            event.text = item.name ?? ""
//            event.text = "Activity Name : \(item.name ?? "") -- SDate: \(startArr?[0] ?? "") - EDate: \(endArr?[0] ?? "")"
//        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deletePressed(_ sender: Any) {
        
            
            Helper.alertWithTwoButton(message: "are you sure you want to delete?", headerTitle: "Alert", confirm: "Ok", cancel: "Cancel", mainColor: UIColor.mainColor) {
                self.deleteVisit(doctorID: "\(self.activityElement?.id ?? 0)")
            }
    }
    
    func deleteVisit(doctorID:String) {
        
        
        let parameters = [doctorID]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.DeleteDoctorVisit)
        let endPoint = RequestHandler.post(parmeters: [:], url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<DeleteVisit>) in
            
            guard let strongSelf = self else {return}
            Helper.hideIndicator(view: strongSelf.view)
            DispatchQueue.main.async {
                
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data()) 
                     if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        Helper.alertOneButton(message: "wrongConnection".localized, alertTitle: "Alert", titleButton: "Ok")
                    }
                case .success(let value):
                    print(value)
                    strongSelf.removeSpinnerWithAction {
                        strongSelf.showMsgithAction(title: "Alert".localized, msg: "Remove Succssfuly", completion: {
                            strongSelf.dismiss(animated: true, completion: nil)
                            self?.reloadDataForCalender?.reloadDataForCalendar()
                        })
                    }
                }
            }
        }
    }
    
    
    
    func getStringDate(date:String) -> String? {
        let dateFormatter = DateFormatter()
        
        if date.contains(".") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        let dateT = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "'Date: ' dd-MM-yyyy 'Time: ' HH:mm:ss"
        //            return dateT
        
        if dateT == nil {
            let splited = date.components(separatedBy: "T")
            return "Date: \(splited[0]) Time: \(splited[1])"
        }else{
        return  dateFormatter.string(from: dateT ?? Date())
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
