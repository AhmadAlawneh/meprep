//
//  PlanOfActionViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/15/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import BetterSegmentedControl

class PlanOfActionViewController: UIViewController {
    
    fileprivate let classificationCell = "ClassificationCell"
    fileprivate let doctorsCell = "DoctorsCell"
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var classifcationTableView: UITableView!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var segmentedControl: BetterSegmentedControl!
    @IBOutlet weak var doctorsTableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searching = false
    
    var classificationList = [ClassificationData]()
    
    let currentDateTime = Date()
    var doctorsList =  [PDoctor]()
    var notVisitedList = [PDoctor]()
    var visitedList = [PDoctor]()
    
    var filterList = [PDoctor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMainViewControlelr()
        classifcationTableView.dataSource = self
        classifcationTableView.delegate = self
        
        doctorsTableView.dataSource = self
        doctorsTableView.delegate = self
        segmentedControl.segments = LabelSegment.segments(withTitles: ["Not Visited" , "Visited"] , normalFont: UIFont(name: "Roboto-Regular", size: 18), selectedFont: UIFont(name: "Roboto-Bold", size: 20) , selectedTextColor: UIColor.mainColor )
        segmentedControl.addTarget(self, action: #selector(self.navigationSegmentedControlValueChanged(_:)), for: .valueChanged)
        segmentedControl.setIndex(0)
        getDoctorsClassificationCallApi()
        
        searchBar.clipsToBounds = true
        searchView.backgroundColor = .clear
        searchBar.clipsToBounds = true
        searchBar.backgroundImage = UIImage()
        
        
        //        if #available(iOS 13.0, *) {
        //            let searchTextField = self.searchBar.searchTextField
        //            searchTextField.textColor = UIColor.white
        //            searchTextField.clearButtonMode = .never
        //            searchTextField.backgroundColor = UIColor.black
        //
        //        } else {
        //            // Fallback on earlier versions
        //        }
        self.searchBar.delegate = self
        self.searchBar.showsCancelButton = false
        
        
        
        
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        let indexPath = IndexPath(row: 0, section: 0)
        //        classifcationTableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
    }
    
    //MARK:- Helper Methods
    
    func reset(){
        filterList = doctorsList
        doctorsTableView.reloadData()
    }
    
    
    
    func setupMainViewControlelr(){
        Helper.addBorderView(view: mainView, borderWidth: 10)
        mainView.layer.borderColor = UIColor.mainColor.cgColor
        sideMenuView.backgroundColor = .mainColor
        superView.layer.cornerRadius = 10
        classifcationTableView.register(UINib(nibName: classificationCell, bundle: nil), forCellReuseIdentifier: classificationCell)
        doctorsTableView.register(UINib(nibName: doctorsCell, bundle: nil), forCellReuseIdentifier: doctorsCell)
    }
    
    
    
    // MARK: - Action handlers
    
    
    @IBAction func popViewController(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @objc func navigationSegmentedControlValueChanged(_ sender: BetterSegmentedControl) {
        if sender.index == 0 {
            print("Turning lights on.")
            self.doctorsList.removeAll()
            self.doctorsList = notVisitedList
            print(doctorsList)
            doctorsTableView.reloadData()
        } else {
            print("Turning lights off.")
            self.doctorsList.removeAll()
            self.doctorsList = self.visitedList
            print(doctorsList)
            doctorsTableView.reloadData()
        }
    }
    
    
    func getDoctorsClassificationCallApi() {
        Helper.showIndicator(view: self.view)
        let endPoint = PlanOfActionEndPoint.getClassification
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<ClassificationModel>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    strongSelf.classificationList = value.classificationData ?? []
                    strongSelf.classifcationTableView.reloadData()
                    
                    
                case .failure(let error):
                    print(error.localizedDescription)
                }
                strongSelf.getDoctorsPlan(calssifiaction: "1")
            }
        }
    }
    
    
    func getDoctorsPlan(calssifiaction:String) {
        
        let dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        
        let parameters = [calssifiaction,dateTime]
        //
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorAssignedToMe)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanOfActionDoctors>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    
                case .success(let value):
                    print(value)
                    let pDoctors = value.pDoctors ?? []
                    strongSelf.doctorsList.removeAll()
                    for item in pDoctors {
                        if item.completedVisitsCount ?? -99 == item.visitsCount ?? -99 {
                            strongSelf.visitedList.append(item)
                        }else{
                            strongSelf.notVisitedList.append(item)
                        }
                    }
                    strongSelf.doctorsTableView.reloadData()
                    strongSelf.segmentedControl.setIndex(strongSelf.segmentedControl.index, animated: true)
                    strongSelf.navigationSegmentedControlValueChanged(strongSelf.segmentedControl)
                }
            }
        }
    }
}

//MARK:- Table View Data Source

extension PlanOfActionViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == classifcationTableView {
            return self.classificationList.count
        }
        if tableView == doctorsTableView {
            
            if searching {
                segmentedControl.isEnabled = false
                return filterList.count
            } else {
                segmentedControl.isEnabled = true
                return doctorsList.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        
        if tableView == classifcationTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: classificationCell, for: indexPath) as? ClassificationCell else {return UITableViewCell()}
            
            let classificationObject = classificationList[indexPath.row]
            cell.classificationObject = classificationObject
            cell.selectionStyle = .none
            return cell
        }else if tableView == doctorsTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: doctorsCell, for: indexPath) as? DoctorsCell else {return UITableViewCell()}
            //            let doctorsObject = doctorsList[indexPath.row]
            
            if searching {
                segmentedControl.isEnabled = false
                cell.doctorObject = filterList[indexPath.row]
            } else {
                segmentedControl.isEnabled = true
                if !doctorsList.isEmpty {
                    cell.doctorObject = doctorsList[indexPath.row]

                }
            }
            
            //            cell.doctorObject = doctorsObject
            cell.selectionStyle = .none
            return cell
        }
        
        return UITableViewCell()
    }
}


extension PlanOfActionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == classifcationTableView {
            return 75
        }else if tableView == doctorsTableView {
            return 80
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == classifcationTableView {
            self.doctorsList.removeAll()
            self.visitedList.removeAll()
            self.notVisitedList.removeAll()
            let classificationSelected = self.classificationList[indexPath.row]
            getDoctorsPlan(calssifiaction: "\(classificationSelected.id ?? -99)")
        }
        
        if tableView == doctorsTableView {
            
            
            if searching {
                segmentedControl.isEnabled = false
                let selectedDoctorInfo = filterList[indexPath.row]
                print(selectedDoctorInfo)
                
                //let selectedDoctorInfo = doctorsList[indexPath.row]
                let storyboard = UIStoryboard(name: "DoctorServices", bundle: nil)
                let doctorsVc = storyboard.instantiateViewController(withIdentifier: "DoctorServicesViewController") as! DoctorServicesViewController
                doctorsVc.doctorInfo = selectedDoctorInfo
                self.navigationController?.pushViewController(doctorsVc, animated: true)
                
                
            } else {
                segmentedControl.isEnabled = true
                let selectedDoctorInfo = doctorsList[indexPath.row]
                let storyboard = UIStoryboard(name: "DoctorServices", bundle: nil)
                let doctorsVc = storyboard.instantiateViewController(withIdentifier: "DoctorServicesViewController") as! DoctorServicesViewController
                doctorsVc.doctorInfo = selectedDoctorInfo
                self.navigationController?.pushViewController(doctorsVc, animated: true)
                print(selectedDoctorInfo)
                
                //let selectedDoctorInfo = doctorsList[indexPath.row]
            }
            
            
            if #available(iOS 13.0, *) {
                self.searchBar.searchTextField.endEditing(true)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}



extension PlanOfActionViewController: UISearchBarDelegate {
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            filterList = doctorsList
        }
    }
    
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            doctorsList = filterList
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchBar.text == "" {
            self.reset()
            segmentedControl.isEnabled = true
            searching = false
        }else{
//            segmentedControl.isEnabled = true
            filterList = doctorsList.filter({ (text) -> Bool in
                
                let name  = "\(text.nameLast ?? ""), \(text.nameFirst ?? "") \(text.nameSecond ?? "") \(text.nameThird ?? "")"
                
                let tmp: NSString = name.lowercased() as NSString
                let range = tmp.range(of: searchText.lowercased(), options: .caseInsensitive)
                return range.location != NSNotFound
                
            })
            searching = true
            doctorsTableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        reset()
    }
}

extension UISearchBar {
    
    func change(textFont : UIFont?) {
        
        for view : UIView in (self.subviews[0]).subviews {
            
            if let textField = view as? UITextField {
                textField.font = textFont
            }
        }
    }
}
