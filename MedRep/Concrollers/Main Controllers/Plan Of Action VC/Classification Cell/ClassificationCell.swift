//
//  ClassificationCell.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/15/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class ClassificationCell: UITableViewCell {
    
    @IBOutlet weak var classifcationView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var classifcationName: UILabel!
    @IBOutlet var classificationType: UILabel!
    
    var classificationObject: ClassificationData? {
        didSet{
            if let object = classificationObject {
                self.updateView(object: object)
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 10
        mainView.clipsToBounds = true
        Helper.MakeRoundedView(view: classifcationView, borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateView(object: ClassificationData) {
        classifcationName.text = "CLASS \(object.name ?? "")"
        classificationType.text = object.name
    }
    
}
