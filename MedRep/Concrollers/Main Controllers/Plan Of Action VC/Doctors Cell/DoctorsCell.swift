//
//  DoctorsCell.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/16/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class DoctorsCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var frequencyView: UIView!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var doctorLocation: UILabel!
    @IBOutlet weak var frequencyCount: UILabel!
    @IBOutlet weak var viewTypeColor: UIView!
    
    
    var doctorObject: PDoctor? {
        didSet{
            if let doctorObject = doctorObject {
                self.updateView(object: doctorObject)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 10
        mainView.clipsToBounds = true
        leftView.clipsToBounds = true
        frequencyView.clipsToBounds = true
        frequencyView.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateView(object: PDoctor){
        
        let firstName = object.nameFirst ?? ""
        let secondName = object.nameSecond ?? ""
        let thirdMame = object.nameThird ?? ""
        let lastName = object.nameLast ?? ""
        let fullName = "\(firstName) \(secondName) \(thirdMame) \(lastName)"
        self.doctorName.text = fullName
        let doctorLocation = object.city?.name ?? ""
        self.doctorLocation.text = doctorLocation
        let totalVisitsCount = object.classification?.visitsCount ?? -99
        let completedCount = object.completedVisitsCount ?? -99
        self.frequencyCount.text = "\(completedCount) / \(totalVisitsCount)"
        
    }
    
}
