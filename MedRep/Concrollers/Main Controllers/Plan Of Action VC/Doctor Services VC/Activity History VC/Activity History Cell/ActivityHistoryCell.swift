//
//  ActivityHistoryCell.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/21/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class ActivityHistoryCell: UITableViewCell {
    
    
    @IBOutlet weak var fromDate: UILabel!
    @IBOutlet weak var toDate: UILabel!
    
    var activityObject: HistoryElement? {
        didSet{
            if let object = activityObject {
                self.updateView(object: object)
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func updateView(object: HistoryElement){
        
        let startDate = getDate(date: object.fromDate ?? "")
        let endDate = getDate(date: object.toDate ?? "")
        fromDate.text = startDate ?? ""
        toDate.text = endDate ?? ""
    }
        
    
    func getDate(date:String) -> String? {
        let dateFormatter = DateFormatter()
        
        if date.contains(".") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        let dateT = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return  dateFormatter.string(from: dateT!)
    }
    
}
