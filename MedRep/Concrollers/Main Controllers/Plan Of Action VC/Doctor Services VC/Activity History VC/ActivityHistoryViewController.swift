//
//  ActivityHistoryViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/20/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class ActivityHistoryViewController: UIViewController {
    
    
    fileprivate let activityHistoryCell = "ActivityHistoryCell"
    
    var doctorInfo:PDoctor!
    
    var activityHistoryList = [HistoryElement]()
    
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var lastCall: UILabel!
    @IBOutlet weak var nextCall: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!
    @IBOutlet weak var doctorLocation: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityHistoryViewController()
        tableView.dataSource = self
        tableView.delegate = self
        getHistory(doctorID: "\(doctorInfo.id ?? -99)")
        updateView(object: self.doctorInfo)
//        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.None
        self.tableView.tableFooterView = UIView()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func updateView(object: PDoctor){

        //TODO: set last call date and day
        
        let firstName = object.nameFirst ?? ""
        let secondName = object.nameSecond ?? ""
        let thirdMame = object.nameThird ?? ""
        let lastName = object.nameLast ?? ""
        let fullName = "\(firstName) \(secondName) \(thirdMame) \(lastName)"
        self.doctorName.text = fullName
        let doctorLocation = object.city?.name ?? ""
        self.doctorLocation.text = doctorLocation
        let totalVisitsCount = object.classification?.visitsCount ?? -99
        let completedCount = object.completedVisitsCount ?? -99
        self.frequencyLabel.text = "\(completedCount) / \(totalVisitsCount)"

    }
    
    
    func setupActivityHistoryViewController(){
        Helper.addBorderView(view: containerView, borderWidth: 1, cornerRadius: 10)
        tableView.register(UINib(nibName: activityHistoryCell, bundle: nil), forCellReuseIdentifier: activityHistoryCell)
    }
    
    
    func getHistory(doctorID:String) {
        
        let parameters = [doctorID]
//        Helper.showIndicator(view: self.view)
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorActivityHistory)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanHistoryModel>) in
            
            guard let strongSelf = self else {return}
            
            Helper.hideIndicator(view: strongSelf.view)
            DispatchQueue.main.async {
//                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .failure(let error):
                    print(error.localizedDescription)
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                          Helper.singOutCallApi()
                      }else{
                          strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                      }
                case .success(let value):
                    strongSelf.activityHistoryList = value.planHistory ?? []
                    strongSelf.tableView.reloadData()
                    print(value)
                }
            }
        }
    }
}


extension ActivityHistoryViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = activityHistoryList.count
        
        if count == 0 {
            tableView.setEmptyView(title: "No Data Found", message: nil)
        }else{
            tableView.restore()
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        guard let cell = tableView.dequeueReusableCell(withIdentifier: activityHistoryCell, for: indexPath) as? ActivityHistoryCell else {return UITableViewCell()}
        let object = activityHistoryList[indexPath.row]
        cell.activityObject = object
        cell.selectionStyle = .none
        return cell
    }
}


extension ActivityHistoryViewController: UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
