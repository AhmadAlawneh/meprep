//
//  ToDoListViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/21/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class ToDoListViewController: UIViewController {
    
    fileprivate let todoCell = "ToDoCell"
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addnoteView: UIView!
    
    var doctorInfo:PDoctor?
    
    var toDoList = [ToDoElement]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToDoListViewController()
        self.tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getToDoDoctorList(doctorId: "\(doctorInfo?.id ?? -99)")
    }
    
    func ifAddedNote(){
        getToDoDoctorList(doctorId: "\(doctorInfo?.id ?? -99)")
    }
    
    //MARK:- Helping Methods
    
    func setupToDoListViewController(){
        Helper.addBorderView(view: containerView, borderWidth: 1, cornerRadius: 10)
        Helper.addBorderView(view: addnoteView, borderWidth: 1, cornerRadius: 10)
        tableView.register(UINib(nibName: todoCell, bundle: nil), forCellReuseIdentifier: todoCell)
    }
    
    
    //MARK:- Action && Selection
    
    @IBAction func addNoteButtonAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ToDoList", bundle: nil)
        let addNoteVC = storyboard.instantiateViewController(withIdentifier: "AddNoteViewController") as! AddNoteViewController
        addNoteVC.providesPresentationContextTransitionStyle = true
        addNoteVC.definesPresentationContext = true
        addNoteVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        addNoteVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        addNoteVC.delegate = self
        addNoteVC.doctorInfo = self.doctorInfo
        self.present(addNoteVC, animated: true, completion: nil)
    }
    
    
    //MARK:- Call Api's
    
    
    func getToDoDoctorList(doctorId:String) {
        
        let parameters = [doctorId]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorTodo)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanToDoModel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    
                    
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                          Helper.singOutCallApi()
                      }else{
                          strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                      }

                    
                    
                case .success(let value):
                    print(value)
                    
                    strongSelf.toDoList = value.toDo ?? []
                    strongSelf.tableView.reloadData()
                }
            }
        }
    }
}




//MARK: - UITable View Data Source

extension ToDoListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = self.toDoList.count
        
        if count == 0 {
            tableView.setEmptyView(title: "No Data Found", message: nil)
        }else{
            tableView.restore()
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        guard let cell = tableView.dequeueReusableCell(withIdentifier: todoCell, for: indexPath) as? ToDoCell else {return UITableViewCell()}
        let object = self.toDoList[indexPath.row]
        cell.toDoObject = object
        cell.selectionStyle = .none
        
        return cell
    }
}



//MARK:- UITable View Delegate

extension ToDoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}


extension ToDoListViewController : updatePreviousView{
    func updateView(isUpdate: Bool) {
        if isUpdate{
            self.ifAddedNote()
        }else{
            print("not added any note")
        }
    }
}
