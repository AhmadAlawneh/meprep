//
//  AddNoteViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/22/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
protocol updatePreviousView: class {
    func updateView(isUpdate:Bool)
}

class AddNoteViewController: UIViewController {
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textVeiwContainerView: UIView!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var addNoteButton: UIButton!
    
    var doctorInfo: PDoctor!
    var delegate: updatePreviousView?
    override func viewDidLoad() {
        super.viewDidLoad()
        animateView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setupAddNoteViewController()
        
    }
    
    //MARK:- Helping Methods
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupAddNoteViewController(){
        //        noteTextView.text = "Placeholder"
        //        noteTextView.textColor = UIColor.lightGray
        noteTextView.textColor = UIColor.white
        Helper.addBorderView(view: containerView, borderWidth: 0, cornerRadius: 5)
        Helper.addBorderView(view: textVeiwContainerView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: addNoteButton, borderWidth: 0, cornerRadius: 5)
    }
    
    //    func textViewDidBeginEditing(_ textView: UITextView) {
    //        if textView.textColor == UIColor.lightGray {
    //            textView.text = nil
    //            textView.textColor = UIColor.black
    //        }
    //    }
    //
    //    func textViewDidEndEditing(_ textView: UITextView) {
    //        if textView.text.isEmpty {
    //            textView.text = "Placeholder"
    //            textView.textColor = UIColor.lightGray
    //        }
    //    }
    
    func animateView() {
        containerView.alpha = 0;
        self.containerView.frame.origin.y = self.containerView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.containerView.alpha = 1.0;
            self.containerView.frame.origin.y = self.containerView.frame.origin.y - 50
        })
    }
    
    
    
    @IBAction func addNoteActionButton(_ sender: UIButton) {
        
        if noteTextView.text.isEmpty {
            Helper.alertOneButton(message: "please enter notes", alertTitle: "alert", titleButton: "ok")
            return
        }else{
            addNoteCallApi(note: noteTextView.text)
        }
    }
    
    
    @IBAction func cancelActionButton(_ sender: UIButton) {
        delegate?.updateView(isUpdate: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func addNoteCallApi(note:String)  {
        
        Helper.showIndicator(view: self.view)
        
        let currentDateTime = Date()
        
        
        let dateTime = "\(currentDateTime.year)\(addZeroIf(number: currentDateTime.month))\(addZeroIf(number: currentDateTime.day))\(addZeroIf(number: currentDateTime.hour))\(addZeroIf(number: currentDateTime.minute))\(addZeroIf(number: 0))"
        
        var parameters = [String:Any]()
        parameters["Note"] = note
        parameters["DoctorID"] = self.doctorInfo.id ?? -99
        parameters["ID"] = 0
        parameters["IsSeen"] = false
        parameters["IsDeleted"] = false
        parameters["EntryDate"] = dateTime
        parameters["EntryUserID"] = LongTermManager.getUserInfo()?.userData?.id
        
        let endPoint = InsertNoteEndPoint.addNote(headerParams: parameters)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<InsetNote>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    strongSelf.noteTextView.text = ""
                    Helper.alertOneButton(message: "your note is entered successfully", alertTitle: "Done", titleButton: "Ok")
                case .failure(let error):
                    print(error.localizedDescription)
                    let res = try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                          Helper.singOutCallApi()
                      }else{
                          strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                      }
                }
            }
        }
    }
}

