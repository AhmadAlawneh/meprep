//
//  ToDoCell.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/21/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class ToDoCell: UITableViewCell {

    
    @IBOutlet weak var noteDate: UILabel!
    @IBOutlet weak var noteText: UILabel!
    
    
    var toDoObject: ToDoElement? {
        didSet{
            if let todoObject = toDoObject {
                self.updateView(todoObject: todoObject)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    func updateView(todoObject: ToDoElement){
//        EntryDate
        let dayname = getDate(date: todoObject.entryDate ?? "")
        let entryDate = getStringDate(date: todoObject.entryDate ?? "")
        
        let dateFormatter = DateFormatter()
        var weekday: String = ""
        dateFormatter.dateFormat = "cccc"
        weekday = dateFormatter.string(from: dayname ?? Date())
        print(weekday)
//        let endDate = getDate(date: object.toDate ?? "")
//        fromDate.text = startDate ?? ""
//        toDate.text = endDate ?? ""
        
        
        self.noteDate.text = "\(weekday) \(entryDate ?? "")"
        self.noteText.text = todoObject.note ?? ""
    }
    
    func getDate(date:String) -> Date? {
        let dateFormatter = DateFormatter()
        
        if date.contains(".") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        let dateT = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = .current
        return dateT
//        return  dateFormatter.string(from: dateT!)
    }
    
//wait
    //when you choose the dates from calcender shows correct on the label ?
    // answer :
    // i need the steps and loggs ok baby
    
        func getStringDate(date:String) -> String? {
            let dateFormatter = DateFormatter()
            
            if date.contains(".") {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            }else{
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            }
            let dateT = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
//            return dateT
            return  dateFormatter.string(from: dateT!)
        }
    
}
