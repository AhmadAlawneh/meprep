//
//  QuestionsViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/24/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import SelectionList

class QuestionsViewController: UIViewController {
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectionList: SelectionList!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    
    var questionObject:SurveyQuestion?
    var stringQuestion = [String]()
    
    var selectedQuestion:[SurveyQuestion]?
    var testlist:[SurveyQuestionAnswer]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionLabel.text = questionObject?.question
        let questionObj = questionObject?.surveyQuestionAnswers ?? []
        
//        selectedQuestion = questionObject?.surveyQuestionAnswer
        
        
        
//        tttt(Id: 1)
        
        
//        print("dfdf")
        for answers in questionObj {
            stringQuestion.append(answers.answer ?? "")
        }
//
//       testlist =  questionObject?.surveyQuestionAnswers
//
//
//
//        var tt = GetPosesById(Id: 1)
//
//        getQuestion(Id: 1)
        
//        selectionList.selectedIndexes = [0, 1, 4]
        
        selectionList.items = stringQuestion
        selectionList.allowsMultipleSelection = false
        selectionList.addTarget(self, action: #selector(selectionChanged), for: .valueChanged)
        selectionList.setupCell = { (cell: UITableViewCell, _: Int) in
            cell.textLabel?.textColor = .gray
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setupQuestionsViewController()
        
    }
    
    @objc func selectionChanged() {
        print(selectionList.selectedIndexes)
//        var x = GetPosesById(Id: selectionList.selectedIndex ?? -999)
        var yy = tttt(Id: (selectionList.selectedIndex ?? -999) + 1)
    }
    
    func setupQuestionsViewController(){
        Helper.addBorderView(view: containerView, borderWidth: 0, cornerRadius: 5)
        Helper.addBorderView(view: selectionList, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: submitButton, borderWidth: 0, cornerRadius: 5)
    }
    
    
    func animateView() {
        containerView.alpha = 0;
        self.containerView.frame.origin.y = self.containerView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.containerView.alpha = 1.0;
            self.containerView.frame.origin.y = self.containerView.frame.origin.y - 50
        })
    }
    
    
    @IBAction func cancelActionButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitActionButton(_ sender: UIButton) {
    }
    
    func GetPosesById(Id: Int) -> SurveyQuestion?{
        return selectedQuestion?.filter({ $0.id == Id }).first
    }
    
    
    func tttt(Id: Int) -> SurveyQuestionAnswer? {
        return selectedQuestion?.filter({$0.id == Id}).first?.surveyQuestionAnswers?.first
    }

    func getQuestion(Id: Int) -> SurveyQuestionAnswer?{
        return testlist?.filter({ $0.id == Id }).first
    }
    
}

