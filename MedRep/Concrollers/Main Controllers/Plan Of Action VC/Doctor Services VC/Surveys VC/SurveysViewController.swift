//
//  SurveysViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/23/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import BetterSegmentedControl
import WebKit

class SurveysViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet var webView: WKWebView!
    @IBOutlet var Activity: UIActivityIndicatorView!
    
    fileprivate let surveysCell = "SurveysCell"
    
    @IBOutlet weak var segmentedControl: BetterSegmentedControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    
    var doctorInfo:PDoctor?
    
    var surveyList = [SurveyQuestion]()
    var surveyexpiryDate: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        setupSurveysViewController()
        tableView.tableFooterView = UIView()
        
        guard let url = URL(string: "https://docs.google.com/forms/d/e/1FAIpQLSeJMb3miBBULuUQ6MQRbqSkIVRK1a1KeuQ3P4bOxeMAqQtC8Q/viewform") else { return }
        
        webView.translatesAutoresizingMaskIntoConstraints = false
                webView.isUserInteractionEnabled = true
                let request = URLRequest(url: url)
                webView.load(request)
                self.webView.addSubview(self.Activity)
                self.Activity.startAnimating()
                self.webView.navigationDelegate = self
                self.Activity.hidesWhenStopped = true
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            Activity.stopAnimating()
        }
        
        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
            Activity.stopAnimating()
        }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        getSurvey(doctorId: "\(doctorInfo?.id ?? -99)")
    }
    
    
    //MARK:- Helping Methids
    
    func setupSurveysViewController(){
        Helper.addBorderView(view: containerView, borderWidth: 0, cornerRadius: 10)
        tableView.register(UINib(nibName: surveysCell, bundle: nil), forCellReuseIdentifier: surveysCell)
    }
    
    
    //MARK:- Call Api's
    
    
    
    func getSurvey(doctorId:String) {
        
        
        let parameters = [doctorId]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetSurveysUnCompleted)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanSurveyModel>) in
            
            guard let strongSelf = self else {return}
            
            Helper.hideIndicator(view: strongSelf.view)
            DispatchQueue.main.async {
                
                switch response.result {
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    
                case .success(let value):
                    
                    if let survey = value.surveys {
                        for surveyQuestions in survey {
                            strongSelf.surveyexpiryDate = surveyQuestions.expiryDate
                            strongSelf.surveyList = surveyQuestions.surveyQuestions ?? []
                        }
                    }
                    strongSelf.tableView.reloadData()
                }
            }
        }
    }
}

extension SurveysViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = self.surveyList.count
        
        if count == 0 {
            tableView.setEmptyView(title: "No Survey Found", message: nil)
        }else{
            tableView.restore()
        }
        
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = .none
        guard let cell = tableView.dequeueReusableCell(withIdentifier: surveysCell, for: indexPath) as? SurveysCell else {return UITableViewCell()}
        
        let surveyObject = self.surveyList[indexPath.row]
        cell.surveyexpiryDate = self.surveyexpiryDate ?? ""
        cell.surveyQuestionObject = surveyObject
        cell.selectionStyle = .none
        return cell
    }
}

//MARK:- UITable View Delegate

extension SurveysViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Surveys", bundle: nil)
        let questionVC = storyboard.instantiateViewController(withIdentifier: "QuestionsViewController") as! QuestionsViewController
        
        let object = surveyList[indexPath.row]
        let test = surveyList
        questionVC.providesPresentationContextTransitionStyle = true
        questionVC.definesPresentationContext = true
        questionVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        questionVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        questionVC.questionObject = object
        questionVC.selectedQuestion = test
//        addNoteVC.delegate = self
//        addNoteVC.doctorInfo = self.doctorInfo
        
        self.present(questionVC, animated: true, completion: nil)
        
        
        
    }
}
