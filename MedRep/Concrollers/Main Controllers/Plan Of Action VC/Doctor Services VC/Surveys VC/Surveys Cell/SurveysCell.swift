//
//  SurveysCell.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/23/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class SurveysCell: UITableViewCell {
    
    @IBOutlet weak var surveysName: UILabel!
    @IBOutlet weak var numberOfQuestions: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    var surveyQuestionObject: SurveyQuestion? {
        didSet{
            if let surveyObject = surveyQuestionObject {
                self.updateView(object: surveyObject)
            }
        }
    }
    
    var surveyexpiryDate: String?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupSurveysCell()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    func setupSurveysCell(){
        Helper.addBorderView(view: dateView, borderWidth: 0, cornerRadius: 15)
    }
    
    
    //    @IBOutlet weak var surveysName: UILabel!
    //     @IBOutlet weak var numberOfQuestions: UILabel!
    //     @IBOutlet weak var dateView: UIView!
    //     @IBOutlet weak var dateLabel: UILabel!
    
    
    
    func updateView(object: SurveyQuestion){
        self.surveysName.text = object.question ?? ""
        self.numberOfQuestions.text = "Question - \(object.id ?? -99)"
        let date = getStringDate(date: self.surveyexpiryDate ?? "")
        self.dateLabel.text = date
    }
    
    
    func getStringDate(date:String) -> String? {
        let dateFormatter = DateFormatter()
        
        if date.contains(".") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        let dateT = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return  dateFormatter.string(from: dateT ?? Date())
    }
}
