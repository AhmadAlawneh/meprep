//
//  DoctorServicesViewController.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/16/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class DoctorServicesViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var contannerView: UIView!
    
    let names = ["essential".localized,"history".localized,"toDos".localized,"surveys".localized]
    let icons = ["information","history","list","survey"]
    let selectedValue = 0
    var doctorInfo:PDoctor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(DoctorServicesTableViewCell.nib, forCellReuseIdentifier: DoctorServicesTableViewCell.identifier)
        
        selectRowInTable(tableView: tableView, row: selectedValue)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contannerView.roundedLeftTopBottom()
    }
    
}

extension DoctorServicesViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: DoctorServicesTableViewCell.identifier) as! DoctorServicesTableViewCell
        
        cell.serviceName.text = names[indexPath.section]
        cell.serviceIcon.image = UIImage(named: icons[indexPath.section])
        
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.clipsToBounds = true
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            let storyboard = UIStoryboard(name: "Essetial", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "EssentialViewControllerID") as! EssentialViewController
            //add as a childviewcontroller
            addChild(controller)
            controller.doctorInfo = doctorInfo
            //Add the child's View as a subview
            self.contannerView.subviews.map({$0.removeFromSuperview()})
            self.contannerView.addSubview(controller.view)
            controller.view.frame = self.contannerView.bounds
            controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            controller.navC = self.navigationController
            //tell the childviewcontroller it's contained in it's parent
            controller.didMove(toParent: self)
        }else if indexPath.section == 1 {
            //ActivityHistoryViewController
            
            let storyboard = UIStoryboard(name: "ActivityHistory", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ActivityHistoryViewController") as! ActivityHistoryViewController
            //add as a childviewcontroller
            addChild(controller)
            controller.doctorInfo = doctorInfo
            // Add the child's View as a subview
            
            //containerView.subviews.map { $0.removeFromSuperview() }
            self.contannerView.subviews.map({$0.removeFromSuperview()})
            self.contannerView.addSubview(controller.view)
            controller.view.frame = self.contannerView.bounds
            controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //controller.navC = self.navigationController
            //tell the childviewcontroller it's contained in it's parent
            controller.didMove(toParent: self)
            
            
            
        }else if indexPath.section == 2 {
            
            //ToDoListViewController
            
            
            let storyboard = UIStoryboard(name: "ToDoList", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ToDoListViewController") as! ToDoListViewController
            //add as a childviewcontroller
            addChild(controller)
            controller.doctorInfo = doctorInfo
            // Add the child's View as a subview
            
            //containerView.subviews.map { $0.removeFromSuperview() }
            self.contannerView.subviews.map({$0.removeFromSuperview()})
            self.contannerView.addSubview(controller.view)
            controller.view.frame = self.contannerView.bounds
            controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //controller.navC = self.navigationController
            // tell the childviewcontroller it's contained in it's parent
            controller.didMove(toParent: self)
            
            
            
        }else {
            
//            let notice = "Surveys service not available right now,\n Surveys will be available as soon as possible"
//
//            Helper.alertWithCompletion(message: notice, alertTitle: "Notice", titleButton: "Ok") {
//                return
//            }
            
            let storyboard = UIStoryboard(name: "Surveys", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SurveysViewController") as! SurveysViewController
            //add as a childviewcontroller
            addChild(controller)
            controller.doctorInfo = doctorInfo
            // Add the child's View as a subview

            //containerView.subviews.map { $0.removeFromSuperview() }
            self.contannerView.subviews.map({$0.removeFromSuperview()})
            self.contannerView.addSubview(controller.view)
            controller.view.frame = self.contannerView.bounds
            controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //controller.navC = self.navigationController
            // tell the childviewcontroller it's contained in it's parent
            controller.didMove(toParent: self)
            
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
}
