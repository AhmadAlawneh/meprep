//
//  DoctorPDFViewController.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/18/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import PDFKit

protocol EndFromVisit {
    func endFromVisit()
}

@available(iOS 11.0, *)
class DoctorPDFViewController: UIViewController,EndFromVisit {
  
    
    
    @IBOutlet var pdfView: PDFView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var timmerLabel: UILabel!
    var startDateFromPDF = Date()

    var timer = Timer()
    var intCounter = 0
    var started = true
    
    var url:String?
    var document:PDFDocument?
    var pageCount = 0
    var doctorInfo:PDoctor!
    var planPDF:PlanFile?
    
    var startDateForEachFile = Array<[String:Date]>()
    var endDateForEachFile = Array<[String:Date]>()
    let calendar = Calendar.current
    var lastSelectFile = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.showIndicator(view: self.view)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
    }
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == .right {
            print("Swipe Right")
            if pageCount > 0 && pageCount < document?.pageCount ?? 0{
                pageCount = pageCount - 1
                pdfView.go(to: (document?.page(at: pageCount))!)
                
            }
        }
        else if gesture.direction == .left {
            print("Swipe Left")
            if pageCount <  (document?.pageCount ?? 0) - 1 {
                pageCount = pageCount + 1
                pdfView.go(to: (document?.page(at: pageCount))!)
                
            }
            
        }
        else if gesture.direction == .up {
            print("Swipe Up")
        }
        else if gesture.direction == .down {
            print("Swipe Down")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let url =  URL(fileURLWithPath: self.url ?? "")
        if let pdfDocument = PDFDocument(url: url){
            pdfView.autoScales = true
            pdfView.displayMode = .singlePage
            pdfView.displayDirection = .horizontal
            pdfView.document = pdfDocument
            document = pdfDocument
            // Specify date components
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm"
            let someDateTime = formatter.date(from: "2000-02-20 22:31")
            let filesCount = document?.pageCount ?? 0
            var count = 0
            while count < filesCount {
                
                print(someDateTime  ?? Date())
                startDateForEachFile.append(["\(count)":someDateTime ?? Date()])
                endDateForEachFile.append(["\(count)":someDateTime ?? Date()])
                count = count + 1
            }
            
            setUpCollectionView()
            Helper.hideIndicator(view: self.view)
            let indexPath:IndexPath = IndexPath(row: 0, section: 0)
            collectionView?.selectItem(at: indexPath, animated: false, scrollPosition: .top)
            startDateFromPDF = Date()
            if started {
                       timer.invalidate()
                       intCounter = 0
                       timmerLabel.isHidden = false
                       timmerLabel.text! = "00:00:00"
                       started = false
                       timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCountdown), userInfo: nil, repeats: true)
                   }
        }else{
            Helper.hideIndicator(view: self.view)
            Helper.alertWithCompletion(message: "There is no data found", alertTitle: "Alert".localized, titleButton: "ok", completion: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    @objc func updateCountdown() {
        intCounter += 1
        //Set counter in UILabel
        timmerLabel.text! = String(format: "%02d:%02d:%02d", intCounter / 3600, (intCounter % 3600) / 60, (intCounter % 3600) % 60)
    }
    
    
    func setUpCollectionView()  {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(FilesCollectionViewCell.nib, forCellWithReuseIdentifier: FilesCollectionViewCell.identifier)
        let collectionViewLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        collectionViewLayout?.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 10)
        collectionViewLayout?.invalidateLayout()
        collectionViewLayout?.itemSize = CGSize(width: 100, height: collectionView.frame.size.height) //this is for cell size
        collectionView.collectionViewLayout = collectionViewLayout ?? UICollectionViewFlowLayout()
    }
    
    @IBAction func backPressed(_ sender: Any) {
         Helper.alertWithTwoButton(message: "You are going to cancel the visit 'It Will Not Be Saved!!!' are you sure ?", headerTitle: "Alert", confirm: "Ok", cancel: "cancel", mainColor: .mainColor, completion: {
        
        self.navigationController?.popViewController(animated: true)
        })
    }
    
    func endFromVisit() {
          self.navigationController?.popViewController(animated: true)

      }
    
    @IBAction func startCallPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Essetial", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "StartCallViewControllerID") as! StartCallViewController
        
        controller.from = 1
        controller.startTimeForEachFile = endDateForEachFile
        controller.endTimeForEachFile = startDateForEachFile
        controller.doctorInfo = doctorInfo
        controller.planPDF = planPDF
        controller.endOfCallTime = Date()
        controller.startDateFromPDF = startDateFromPDF
        controller.endDelegate = self
        controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: true, completion: nil)
    }
    
}




@available(iOS 11.0, *)
extension DoctorPDFViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return document?.pageCount ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: FilesCollectionViewCell.identifier, for: indexPath) as! FilesCollectionViewCell
        
        
        if let page = document?.page(at: indexPath.section){
            cell.fileImage.image = page.thumbnail(of: CGSize(width: cell.fileImage.frame.width, height: cell.fileImage.frame.height), for: .artBox)
            cell.fileName.text = "Page : \(indexPath.section + 1)"
            
        }
        
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.calendarTextColor.cgColor
        cell.layer.cornerRadius = 10
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        lastSelectFile = indexPath.section
        let end = Date()
        endDateForEachFile[indexPath.section]["\(indexPath.section)"] = end
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        pageCount = indexPath.section
        pdfView.go(to: (document?.page(at: indexPath.section))!)
        let current = Date()
        let start = startDateForEachFile[lastSelectFile]
        let startDate = start["\(lastSelectFile)"] ?? Date()
        let year:Int =  calendar.component(.year, from: startDate)
        if year != current.year{
            startDateForEachFile[lastSelectFile]["\(lastSelectFile)"] = current
        }
    }
    
    
}

@available(iOS 11.0, *)
extension DoctorPDFViewController{
    
    func InsertFilePage(pageNo:Int) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSZ"
        let now = df.string(from: Date())
        let selectedStartDate = Date()
        var parameters = [String:Any]()
        
        parameters["ID"] =  "0"
        parameters["DoctorVisitID"] =  doctorInfo.cityID
        parameters["FileID"] =  planPDF?.fileID
        parameters["PageNo"] =  pageNo
        parameters["StartDate"] = "\(selectedStartDate.year)-\(addZeroIf(number: selectedStartDate.month))-\(addZeroIf(number: selectedStartDate.day))T\(addZeroIf(number: selectedStartDate.hour)):\(addZeroIf(number: selectedStartDate.minute)):\(addZeroIf(number: 0))"
        parameters["EndDate"] =  "\(selectedStartDate.year)-\(addZeroIf(number: selectedStartDate.month))-\(addZeroIf(number: selectedStartDate.day))T\(addZeroIf(number: selectedStartDate.hour)):\(addZeroIf(number: selectedStartDate.minute)):\(addZeroIf(number: 0))"
        parameters["EntryUserID"] = LongTermManager.getUserInfo()?.userData?.id
        parameters["EntryDate"] = "\(now)"
        parameters["ModifyUserID"] = "null"
        parameters["ModifyDate"] = "null"
        
        Helper.showIndicator(view: self.view)
        let endPoint = RequestHandler.post(parmeters: parameters, url: ApiURLs.InsertDoctorVisitFile)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<InsertFile>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        
                        strongSelf.removeSpinnerWithAction {
                            strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                        }
                        
                    }
                    
                    
                case .success(let value):
                    print(value)
                    Helper.hideIndicator(view: strongSelf.view)
                    
                }
            }
            
            
        }
        
    }
}

