//
//  FilesCollectionViewCell.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/18/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class FilesCollectionViewCell: UICollectionViewCell {

    @IBOutlet var fileImage: UIImageView!
    @IBOutlet var fileName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var nib:UINib {
                 return UINib(nibName: identifier, bundle: nil)
             }
             static var identifier: String {
                 return String(describing: self)
             }
         
}
