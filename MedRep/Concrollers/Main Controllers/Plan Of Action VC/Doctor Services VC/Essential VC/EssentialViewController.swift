//
//  EssentialViewController.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/18/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import PDFKit
class EssentialViewController: UIViewController {
    @IBOutlet var doctorName: UILabel!
    @IBOutlet var doctorMAjor: UILabel!
    @IBOutlet var doctorAddress: UILabel!
    @IBOutlet var frequency: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    
    var doctorInfo:PDoctor?
    var planPDF:PlanFiles?
    
    
    var navC: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        frequency.layer.cornerRadius = 20
        frequency.clipsToBounds = true
        doctorName.text = "\(doctorInfo?.nameLast ?? ""), \(doctorInfo?.nameFirst ?? "") \(doctorInfo?.nameSecond ?? "")"
        doctorMAjor.text = doctorInfo?.major?.name ?? ""
        doctorAddress.text = doctorInfo?.city?.name ?? ""
        frequency.text = "\(doctorInfo?.completedVisitsCount ?? 0)"
        
        
        
        getFiles(doctorID: "\(doctorInfo?.id ?? 0)")
    }
    
    
    func setUpCollectionView()  {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(FilesCollectionViewCell.nib, forCellWithReuseIdentifier: FilesCollectionViewCell.identifier)
        let collectionViewLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        collectionViewLayout?.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 20)
        collectionViewLayout?.invalidateLayout()
        collectionViewLayout?.itemSize = CGSize(width: 150, height: collectionView.frame.size.height) //this is for cell size
        
        collectionView.collectionViewLayout = collectionViewLayout ?? UICollectionViewFlowLayout()
    }
    
    func getFiles(doctorID:String) {
        
        
        let parameters = [doctorID]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetDoctorFiles)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<PlanFilesModel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    print(value)
                    strongSelf.planPDF = value.planFiles
                    strongSelf.setUpCollectionView()
                    
                }
            }
            
            
        }
        
    }
    
    @available(iOS 11.0, *)
    func pdfThumbnail(url: URL, width: CGFloat = 240) -> UIImage? {
        guard let data = try? Data(contentsOf: url),
            let page = PDFDocument(data: data)?.page(at: 0) else {
                return nil
        }
        
        let pageSize = page.bounds(for: .trimBox)
        let pdfScale = width / pageSize.width
        
        // Apply if you're displaying the thumbnail on screen
        let scale = UIScreen.main.scale * pdfScale
        let screenSize = CGSize(width: pageSize.width * scale,
                                height: pageSize.height * scale)
        
        return page.thumbnail(of: screenSize, for: .mediaBox)
    }
    
    
    func thumbnailFromPdf(withUrl url:URL, pageNumber:Int, width: CGFloat = 240) -> UIImage? {
        guard let pdf = CGPDFDocument(url as CFURL),
            let page = pdf.page(at: pageNumber)
            else {
                return nil
        }
        
        var pageRect = page.getBoxRect(.mediaBox)
        let pdfScale = width / pageRect.size.width
        pageRect.size = CGSize(width: pageRect.size.width*pdfScale, height: pageRect.size.height*pdfScale)
        pageRect.origin = .zero
        
        UIGraphicsBeginImageContext(pageRect.size)
        let context = UIGraphicsGetCurrentContext()!
        
        // White BG
        context.setFillColor(UIColor.white.cgColor)
        context.fill(pageRect)
        context.saveGState()
        
        // Next 3 lines makes the rotations so that the page look in the right direction
        context.translateBy(x: 0.0, y: pageRect.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.concatenate(page.getDrawingTransform(.mediaBox, rect: pageRect, rotate: 0, preserveAspectRatio: true))
        
        context.drawPDFPage(page)
        context.restoreGState()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    @IBAction func startCallPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Essetial", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "StartCallViewControllerID") as! StartCallViewController
        controller.from = 0
        controller.doctorInfo = doctorInfo
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    
}

extension EssentialViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return planPDF?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilesCollectionViewCell.identifier, for: indexPath) as! FilesCollectionViewCell
        
        cell.fileImage.image = UIImage(named: "pdf icon")
        
        cell.fileName.text = planPDF?[indexPath.section].file?.name
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.cornerRadius = 10
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Helper.alertWithTwoButton(message: "You are going to start the visit are you sure ?", headerTitle: "Alert", confirm: "Ok", cancel: "cancel", mainColor: .mainColor, completion: {
            
        
        let storyboard = UIStoryboard(name: "Essetial", bundle: nil)
        if #available(iOS 11.0, *) {
            let controller = storyboard.instantiateViewController(withIdentifier: "DoctorPDFViewControllerID") as! DoctorPDFViewController
            var stringName = ""
            
            for item in self.jsonPDfHandler().data ?? [PDfData](){
                let trimedString = item.name?.replace(string: " ", replacement: "")
                let removePDF = trimedString?.replace(string: ".pdf", replacement: "")
                let BackEndResponseTrimedName = self.planPDF?[indexPath.section].file?.name?.replace(string: " ", replacement: "")
                if removePDF == BackEndResponseTrimedName {
                    stringName = self.planPDF?[indexPath.section].file?.name ?? ""
                }
            }
            let stringURL = stringName
            print(stringURL)
            let path = Bundle.main.path(forResource: "\(stringURL)", ofType: "pdf")
            controller.url =  path ?? ""
            //self.present(controller, animated: true, completion: nil)
            controller.doctorInfo = self.doctorInfo
            controller.planPDF = self.planPDF?[indexPath.section]
            self.navC?.pushViewController(controller, animated: true)
            
        } else {
            
            
        }
            })

    }
    
    func jsonPDfHandler() -> PDFModel{
        let url = Bundle.main.url(forResource: "PDFFiles", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        let pdfModel = try! JSONDecoder().decode(PDFModel.self, from: data)
        return pdfModel
     
    }
    
    
}



extension String {
   func replace(string:String, replacement:String) -> String {
       return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
   }
    
}
