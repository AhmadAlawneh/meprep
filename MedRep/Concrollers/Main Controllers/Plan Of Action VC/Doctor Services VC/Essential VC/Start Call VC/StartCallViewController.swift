//
//  StartCallViewController.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/19/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import FAPickerView
import DateTimePicker

class StartCallViewController: UIViewController {
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var cahannelLabel: UILabel!
    @IBOutlet var accLabel: UILabel!
    @IBOutlet var cycleLabel: UILabel!
    @IBOutlet var saveAndStart: UIButton!
    @IBOutlet var timmerLabel: UILabel!
    @IBOutlet var saveForLaterOutlet: UIButton!
    @IBOutlet var dateView: UIView!
    @IBOutlet var timeView: UIView!
    @IBOutlet var channelView: UIView!
    @IBOutlet var accView: UIView!
    @IBOutlet var cycleView: UIView!
    @IBOutlet var noteView: UIView!
    @IBOutlet var noteTextView: UITextView!
    @IBOutlet var productLAbel: UILabel!
    @IBOutlet var productView: UIView!
    
    var endDelegate:EndFromVisit!
    var doctorInfo:PDoctor!
    var planPDF:PlanFile?
    
    var selectedTypeItem = FAPickerItem()
    var selectedTypeCycleItem = FAPickerItem()
    var selectedTypeCurrentItem = FAPickerItem()
    var selectedTypeProduct = FAPickerItem()
    var startDate = Date()
    var endDate = Date()
    var items = Array<FAPickerItem>()
    var from:Int?
    var startTimeForEachFile:Array<[String:Date]>!
    var endTimeForEachFile:Array<[String:Date]>!
    var endOfCallTime:Date!
    var startDateFromPDF:Date!
    var timer = Timer()
    var intCounter = 0
    var started = true
    var countOfPages = 0
    var pages = [String]()
    var countForPage = 0
    let calendar = Calendar.current
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let calendar = Calendar.current
        Helper.addBorderView(view: productView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: dateView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: timeView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: channelView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: accView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: cycleView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: saveForLaterOutlet, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: noteView, borderWidth: 1, cornerRadius: 5)
        if from == 1 {
            for (index,item) in startTimeForEachFile.enumerated(){
                let current = Date()
                let start = item
                let startDate = start["\(index)"] ?? Date()
                let year:Int =  calendar.component(.year, from: startDate)
                if  year != current.year{
                    startTimeForEachFile[index].removeValue(forKey: "\(index)")
                }
            }
            
            for (index,item) in endTimeForEachFile.enumerated(){
                let current = Date()
                let end = item
                let endDate = end["\(index)"] ?? Date()
                let year:Int =  calendar.component(.year, from: endDate)
                if  year != current.year{
                    endTimeForEachFile[index].removeValue(forKey: "\(index)")
                }
            }
            
            startTimeForEachFile = startTimeForEachFile.filter { !$0.isEmpty }
            endTimeForEachFile = endTimeForEachFile.filter { !$0.isEmpty }
            countOfPages = startTimeForEachFile.count
            
            for page in startTimeForEachFile{
                let val = Array(page.keys)
                pages.append(val[0])
            }
            saveAndStart.setTitle("Save", for: .normal)
            timmerLabel.text =  "Date : \(endOfCallTime.year)-\(addZeroIf(number: endOfCallTime.month))-\(addZeroIf(number: endOfCallTime.day)) Time :  \(addZeroIf(number: endOfCallTime.hour)):\(addZeroIf(number: endOfCallTime.minute)):\(addZeroIf(number: calendar.component(.second, from: endOfCallTime)))"
            
            dateLabel.text =  "Date : \(startDateFromPDF.year)-\(addZeroIf(number: startDateFromPDF.month))-\(addZeroIf(number: startDateFromPDF.day)) Time : \(addZeroIf(number: startDateFromPDF.hour)):\(addZeroIf(number: startDateFromPDF.minute)):\(addZeroIf(number: calendar.component(.second, from: startDateFromPDF)))"
        }else{
            startDateFromPDF = Date()
            endOfCallTime = Date()
            saveForLaterOutlet.isHidden = true

        }
        
        getAcc(fromStart: true)
        getChannel(fromStart: true)
        getCycle(fromStart: true)
        
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
  
    
    
    //MARK:- Action && Selectoer
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        if from == 1 {
            Helper.alertWithTwoButton(message: "Are you sure you want to cancel, the call is already started ? ", headerTitle: "Alert".localized, confirm: "ok", cancel: "cancel", mainColor: .mainColor, completion: {
                self.dismiss(animated: true, completion: nil)
                self.endDelegate.endFromVisit()
            })
        }else{
            
            if started {
                
                self.dismiss(animated: true, completion: nil)
                
                
            }else{
                Helper.alertWithTwoButton(message: "Are you sure you want to cancel, the call is already started ? ", headerTitle: "Alert".localized, confirm: "ok", cancel: "cancel", mainColor: .mainColor, completion: {
                    self.dismiss(animated: true, completion: nil)
                    
                })
            }
        }
        
    }
    
    
    @IBAction func saveForLaterPressed(_ sender: Any) {
        if from == 0{
            endOfCallTime = Date()
            
        }
        timeLabel.text = timmerLabel.text
        if dateLabel.text?.isEmpty ?? true {
            dateView.shakeView()
            return
        }
        if productLAbel.text?.isEmptyOrWhitespace ?? true {
            productView.shakeView()
            return
        }
        
        
        addvisit(callStatus: "1")
        
    }
    
    @IBAction func SavePressed(_ sender: Any) {
        
        if  from == 1 {
            
            
            
            
            if dateLabel.text?.isEmpty ?? true {
                dateView.shakeView()
                return
            }
            
            if productLAbel.text?.isEmptyOrWhitespace ?? true {
                productView.shakeView()
                return
            }
            
            
            addvisit(callStatus: "2")
            
        }else{

            if started {
                timer.invalidate()
                intCounter = 0
                timmerLabel.isHidden = false
                timmerLabel.text! = "00:00:00"
                started = false
                saveAndStart.setTitle("Save", for: .normal)
                saveForLaterOutlet.isHidden = false
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCountdown), userInfo: nil, repeats: true)
            }else{
                    endOfCallTime = Date()

                timeLabel.text = timmerLabel.text
                
                if dateLabel.text?.isEmpty ?? true {
                    dateView.shakeView()
                    return
                }
                
                if productLAbel.text?.isEmptyOrWhitespace ?? true {
                    productView.shakeView()
                    return
                }
                
                
                addvisit(callStatus: "2")
                
            }
           

        }
    }
    
    
    @objc func updateCountdown() {
        intCounter += 1
        //Set counter in UILabel
        timmerLabel.text! = String(format: "%02d:%02d:%02d", intCounter / 3600, (intCounter % 3600) / 60, (intCounter % 3600) % 60)
    }
    
    @IBAction func ProductPressed(_ sender: Any) {
        
        items.removeAll()
        getProduct()
    }
    
    @IBAction func selectDatePressed(_ sender: Any) {

        openDatePiker(tag: 1)
    }
    
  
    
    @IBAction func selectChannel(_ sender: Any) {
        items.removeAll()
        getChannel(fromStart: false)
    }
    @IBAction func selectAccPressed(_ sender: Any) {
        items.removeAll()
        getAcc(fromStart: false)
    }
    
    
    @IBAction func selectCyclePressed(_ sender: Any) {
        items.removeAll()
        getCycle(fromStart: false)
    }
    
    
    
    func openDatePiker(tag:Int){
        hideKeyboard()
        //        let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
        //        let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
        //        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        
        
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 1)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
//        let picker = DateTimePicker.create()
        picker.tag = tag
        //        picker.dateFormat = "hh:mm:ss aa dd/MM/YYYY"
        picker.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        picker.isDatePickerOnly = true
        picker.includesMonth = true
        picker.includesSecond = true
        picker.is12HourFormat = false
        //        picker.isTimePickerOnly = true
        picker.highlightColor = UIColor(red: 0.30, green: 0.52, blue: 1.00, alpha: 1.00)
        picker.doneButtonTitle = "!! Select Date !!"
        picker.doneBackgroundColor = UIColor(red: 0.30, green: 0.52, blue: 1.00, alpha: 1.00)
        picker.customFontSetting = DateTimePicker.CustomFontSetting(selectedDateLabelFont: .boldSystemFont(ofSize: 30))
        
        
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.title = formatter.string(from: date)
            picker.tag = tag
            let min =   self.minutesBetweenDates(start: self.startDateFromPDF, end: self.endOfCallTime)
            let differenceInSeconds = Int(self.endOfCallTime.timeIntervalSince(self.startDateFromPDF))

            if let outDate = formatter.date(from: picker.selectedDateString) {
                    self.startDateFromPDF = outDate
                self.endOfCallTime = Date(year: self.startDateFromPDF.year, month: self.startDateFromPDF.month, day: self.startDateFromPDF.day, hour: self.startDateFromPDF.hour, minute: self.startDateFromPDF.addingMinute(minute: min).minute, second: self.startDateFromPDF.addingSecond(second: differenceInSeconds).second)

                    self.dateLabel.text = "Date : \(self.startDateFromPDF.year )-\(self.addZeroIf(number:self.startDateFromPDF.month))-\(self.addZeroIf(number:self.startDateFromPDF.day)) Time :  \(self.addZeroIf(number:self.startDateFromPDF.hour)):\(self.addZeroIf(number:self.startDateFromPDF.minute)):\(self.addZeroIf(number: self.calendar.component(.second, from: self.startDateFromPDF) ))"
                    if self.from == 1{
                        self.timmerLabel.text = "Date : \(self.startDateFromPDF.year)-\(self.addZeroIf(number: self.startDateFromPDF.month))-\(self.addZeroIf(number: self.startDateFromPDF.day)) Time : \(self.addZeroIf(number: self.startDateFromPDF.hour)):\(self.addZeroIf(number: self.startDateFromPDF.addingMinute(minute: Int(min)).minute)):\(self.addZeroIf(number: self.calendar.component(.second, from: self.startDateFromPDF.addingSecond(second: differenceInSeconds)) ))"
                    }
                }
                
            
        }
        picker.delegate = self
        picker.show()
    }
    
    
    
    //MARK:- Call Api's
    
    
    
    func getCycle(fromStart:Bool) {
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.Getcycle)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<Cycel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    
                    if fromStart {
                        if !(value.cycles?.isEmpty ?? true) {
                            strongSelf.cycleLabel.text = value.cycles?[0].name
                            strongSelf.selectedTypeCycleItem.id = "\(value.cycles?[0].id ?? 0)"
                        }
                        
                    }else{
                        
                        if value.cycles?.isEmpty ?? true {
                            Helper.alertOneButton(message: "There is no data", alertTitle: "Alert".localized, titleButton: "ok")
                        }else{
                            
                            for val in value.cycles ?? Cycles() {
                                strongSelf.items.append(FAPickerItem(id: "\(val.id ?? 0)", title: val.name))
                            }
                            
                            FAPickerView.setMainColor(UIColor.mainColor)
                            FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                              selectedItem: strongSelf.selectedTypeCycleItem,
                                                              filter: false,
                                                              headerTitle: "Select one item",
                                                              complete: { (item:FAPickerItem?) in
                                                                if let item = item {
                                                                    strongSelf.selectedTypeCycleItem = item
                                                                    strongSelf.cycleLabel.text = item.title
                                                                }
                            }, cancel: {
                                
                            })
                        }
                    }
                }
            }
        }
    }
    
    
    func getProduct() {
        
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetProduct)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<Channel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    
                    if value.channeles?.isEmpty ?? true {
                        Helper.alertOneButton(message: "There is no data", alertTitle: "Alert".localized, titleButton: "ok")
                    }else{
                        for val in value.channeles ?? Channeles() {
                            strongSelf.items.append(FAPickerItem(id: "\(val.id ?? 0)", title: val.name))
                        }
                        
                        FAPickerView.setMainColor(UIColor.mainColor)
                        FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                          selectedItem: strongSelf.selectedTypeProduct,
                                                          filter: false,
                                                          headerTitle: "Select one item",
                                                          complete: { (item:FAPickerItem?) in
                                                            if let item = item {
                                                                strongSelf.selectedTypeProduct = item
                                                                strongSelf.productLAbel.text = item.title
                                                            }
                        }, cancel: {
                            
                        })
                    }
                }
            }
        }
    }
    
    
    
    func getChannel(fromStart:Bool) {
        
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.Getchannel)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<Channel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    
                    if fromStart {
                        if !(value.channeles?.isEmpty ?? true) {
                            strongSelf.cahannelLabel.text = value.channeles?[0].name
                            strongSelf.selectedTypeCurrentItem.id = "\(value.channeles?[0].id ?? 0)"
                            
                        }
                        
                    }else{
                        if value.channeles?.isEmpty ?? true {
                            Helper.alertOneButton(message: "There is no data", alertTitle: "Alert".localized, titleButton: "ok")
                        }else{
                            for val in value.channeles ?? Channeles() {
                                strongSelf.items.append(FAPickerItem(id: "\(val.id ?? 0)", title: val.name))
                            }
                            
                            FAPickerView.setMainColor(UIColor.mainColor)
                            FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                              selectedItem: strongSelf.selectedTypeCurrentItem,
                                                              filter: false,
                                                              headerTitle: "Select one item",
                                                              complete: { (item:FAPickerItem?) in
                                                                if let item = item {
                                                                    strongSelf.selectedTypeCurrentItem = item
                                                                    strongSelf.cahannelLabel.text = item.title
                                                                }
                            }, cancel: {
                                
                            })
                        }
                    }
                    
                }
            }
        }
    }
    
    func getAcc(fromStart:Bool) {
        
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetAccompaniedBy)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<Channel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    
                    if fromStart {
                        if !(value.channeles?.isEmpty ?? true) {
                            strongSelf.accLabel.text = value.channeles?[0].name
                            strongSelf.selectedTypeItem.id = "\(value.channeles?[0].id ?? 0)"
                            
                        }
                        
                    }else{
                        
                        if value.channeles?.isEmpty ?? true {
                            Helper.alertOneButton(message: "There is no data", alertTitle: "Alert".localized, titleButton: "ok")
                        }else{
                            for val in value.channeles ?? Channeles() {
                                strongSelf.items.append(FAPickerItem(id: "\(val.id ?? 0)", title: val.name))
                            }
                            
                            FAPickerView.setMainColor(UIColor.mainColor)
                            FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                              selectedItem: strongSelf.selectedTypeItem,
                                                              filter: false,
                                                              headerTitle: "Select one item",
                                                              complete: { (item:FAPickerItem?) in
                                                                if let item = item {
                                                                    strongSelf.selectedTypeItem = item
                                                                    strongSelf.accLabel.text = item.title
                                                                }
                            }, cancel: {
                                
                            })
                        }
                    }
                }
            }
        }
    }
}


extension StartCallViewController{
    func addvisit(callStatus:String) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSZ"
        let now = df.string(from: Date())
        var parameters = [String:Any]()
        /*
         [12:08 am, 01/10/2020] Ahmad Al-zuhairi: AccompaniedByID
         [12:08 am, 01/10/2020] Ahmad Al-zuhairi: RepresentetaiveUserID
         [12:08 am, 01/10/2020] Ahmad Al-zuhairi: ChannelID
         [12:08 am, 01/10/2020] Ahmad Al-zuhairi: CycleID
         */
        
        parameters["ID"] =  "0"
        parameters["DoctorID"] =  doctorInfo.id
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss"
        dateFormatter.timeZone = TimeZone.current

        let item = timmerLabel.text ?? "00:00:00"
        let date = dateFormatter.date(from: item)
        
        
        
        if selectedTypeItem.id == "-1" {
            parameters["AccompaniedByID"] =  ""
            
        }else{
            parameters["AccompaniedByID"] =  selectedTypeItem.id
            
        }
        parameters["FromDate"] =  "\(startDateFromPDF.year)-\(addZeroIf(number: startDateFromPDF.month))-\(addZeroIf(number: startDateFromPDF.day))T\(addZeroIf(number: startDateFromPDF.hour)):\(addZeroIf(number: startDateFromPDF.minute)):\(addZeroIf(number:00))"
        if from == 0 {
            let dateA = self.startDateFromPDF + 1 * 60

            parameters["ToDate"] =  "\(startDateFromPDF.year)-\(addZeroIf(number: startDateFromPDF.month))-\(addZeroIf(number: startDateFromPDF.day))T\(addZeroIf(number: startDateFromPDF.hour)):\(addZeroIf(number: dateA.minute)):\(addZeroIf(number: self.startDateFromPDF.addingSecond(second: date?.second ?? 0).second))"
        }else{
            parameters["ToDate"] =  "\(startDateFromPDF.year)-\(addZeroIf(number: startDateFromPDF.month))-\(addZeroIf(number: startDateFromPDF.day))T\(addZeroIf(number: startDateFromPDF.hour)):\(addZeroIf(number: endOfCallTime.minute)):\(addZeroIf(number: calendar.component(.second, from: endOfCallTime)))"
        }
            

       
        parameters["Address"] = doctorInfo.city?.name
        parameters["VisitStatusID"] = callStatus
        parameters["IsDeleted"] = false
        parameters["ChannelID"] = selectedTypeCurrentItem.id
        parameters["CycleID"] = selectedTypeCycleItem.id
        parameters["RepresentativeUserID"] = LongTermManager.getUserInfo()?.userData?.id
        parameters["ProductID"] = selectedTypeProduct.id
        parameters["EntryUserID"] = LongTermManager.getUserInfo()?.userData?.id
        parameters["EntryDate"] = "\(now)"
        parameters["ModifyUserID"] = "0"
        parameters["Note"] = noteTextView.text
        parameters["ModifyDate"] = "\(now)"
        
        
        Helper.showIndicator(view: self.view)
        let endPoint = RequestHandler.post(parmeters: parameters, url: ApiURLs.InsertDoctorVisit)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<InsertVisit>) in
            
            guard let strongSelf = self else {return}
            
            DispatchQueue.main.async {
                
                switch response.result {
                    
                case .failure( _):
                    Helper.hideIndicator(view: strongSelf.view)
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                        
                        
                        
                    }else if res?.statusCode == 3{
                        strongSelf.showMsg(title: "Alert".localized, msg: "Invalid Date \n '** The start date is smaller that the end date **'")

                        
                    }else if res?.statusCode == 6{
                        strongSelf.showMsg(title: "Alert".localized, msg: "Invalid Date \n '** There is already visit in this time **'")

                    }
                    
                    else{
                        strongSelf.removeSpinnerWithAction {
                            strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                        }
                        
                    }
                    
                    
                case .success(let value):
                    print(value)
                    
                    strongSelf.timer.invalidate()
                    if strongSelf.from == 0 {
                        Helper.hideIndicator(view: strongSelf.view)
                        
                        Helper.alertWithCompletion(message: "Done From Adding call", alertTitle: "Alert", titleButton: "Ok") {
                            strongSelf.dismiss(animated: true, completion: nil)
                            
                        }
//                        strongSelf.removeSpinnerWithAction {
//                            strongSelf.showMsgithAction(title: "Alert".localized, msg: "Done From Adding call", completion: {
//                                strongSelf.dismiss(animated: true, completion: nil)
//
//
//                            })
//                        }
                    }else{
                        if strongSelf.countOfPages == 0 {
                            
                            Helper.hideIndicator(view: strongSelf.view)
//                            strongSelf.removeSpinnerWithAction {
                                strongSelf.showMsgithAction(title: "Alert".localized, msg: "Done From Adding call", completion: {
                                    strongSelf.dismiss(animated: true, completion: nil)
                                    strongSelf.endDelegate.endFromVisit()
                                    
                                })
//                            }
                        }else{
                            strongSelf.InsertFilePage(pageNo: Int(strongSelf.pages[strongSelf.countForPage]) ?? 0 ,visitId: value.data ?? "0")
                            strongSelf.countForPage = strongSelf.countForPage + 1
                        }
                    }
                }
            }
        }
    }
    
    func InsertFilePage(pageNo:Int,visitId:String) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSZ"
        let now = df.string(from: Date())
        var parameters = [String:Any]()
        
        parameters["ID"] =  "0"
        parameters["DoctorVisitID"] =  visitId
        parameters["FileID"] =  planPDF?.fileID
        parameters["PageNo"] =  pageNo
        
        let time = startTimeForEachFile[countForPage]["\(pageNo)"]
        let endTime = endTimeForEachFile[countForPage]["\(pageNo)"]
        
        parameters["StartDate"] = "\(time?.year ?? 2020)-\(addZeroIf(number: time?.month ?? 0))-\(addZeroIf(number: time?.day ?? 0))T\(addZeroIf(number: time?.hour ?? 0)):\(addZeroIf(number: time?.minute  ?? 0)):\(addZeroIf(number: calendar.component(.second, from: time ?? Date())))"
        
        parameters["EndDate"] =   "\(endTime?.year ?? 2020)-\(addZeroIf(number: endTime?.month ?? 0))-\(addZeroIf(number: endTime?.day ?? 0))T\(addZeroIf(number: endTime?.hour ?? 0)):\(addZeroIf(number: endTime?.minute ?? 0 )):\(addZeroIf(number: calendar.component(.second, from: endTime ?? Date())))"
        parameters["EntryUserID"] = LongTermManager.getUserInfo()?.userData?.id
        parameters["EntryDate"] = "\(now)"
        parameters["ModifyUserID"] = "null"
        parameters["ModifyDate"] = "null"
        
        let endPoint = RequestHandler.post(parmeters: parameters, url: ApiURLs.InsertDoctorVisitFile)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<InsertFile>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.removeSpinnerWithAction {
                            strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                        }
                        
                    }
                    
                case .success(let value):
                    print(value)
                    if strongSelf.countForPage < strongSelf.countOfPages{
                        strongSelf.InsertFilePage(pageNo: Int(strongSelf.pages[strongSelf.countForPage]) ?? 0,visitId: visitId)
                        strongSelf.countForPage = strongSelf.countForPage + 1
                    }else{
                        Helper.hideIndicator(view: strongSelf.view)
                        strongSelf.showMsgithAction(title: "Alert".localized, msg: "Done From Adding call", completion: {
                            strongSelf.dismiss(animated: true, completion: nil)
                            strongSelf.endDelegate.endFromVisit()
                            
                        })
                    }
                }
            }
        }
    }
}


extension StartCallViewController: DateTimePickerDelegate {
    
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if picker.tag == 1 {
            if let outDate = formatter.date(from: picker.selectedDateString) {
                self.startDate = outDate
                print(self.startDate)
                self.dateLabel.text = "\(self.startDate.year )-\(self.addZeroIf(number:self.startDate.month))-\(self.addZeroIf(number:self.startDate.day)) \(self.addZeroIf(number:self.startDate.hour)):\(self.addZeroIf(number:self.startDate.minute))"
                print(self.startDate)
            } }else if picker.tag == 2{
            if let outDate = formatter.date(from: picker.selectedDateString) {
                self.endDate = outDate
                print(self.endDate)
                self.timeLabel.text = "\(self.endDate.year )-\(self.addZeroIf(number:self.endDate.month))-\(self.addZeroIf(number:self.endDate.day)) \(self.addZeroIf(number:self.endDate.hour)):\(self.addZeroIf(number:self.endDate.minute))"
                print(self.startDate)
            }
        }
    }
    
 
    
    func minutesBetweenDates(start: Date, end: Date) -> Int
       {

           let diff = Int(end.timeIntervalSince1970 - start.timeIntervalSince1970)

           let hours = diff / 3600
           let minutes = (diff - hours * 3600) / 60
           return minutes
       }
}

extension Date {
    func addingSecond(second: Int) -> Date {
        return Calendar.current.date(byAdding: .second, value: second, to: self)!
    }
    func addingMinute(minute: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minute, to: self)!
    }
}
