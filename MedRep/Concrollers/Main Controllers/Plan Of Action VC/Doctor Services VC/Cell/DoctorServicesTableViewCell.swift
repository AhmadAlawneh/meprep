//
//  DoctorServicesTableViewCell.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/18/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class DoctorServicesTableViewCell: UITableViewCell {

    @IBOutlet var serviceIcon: UIImageView!
    @IBOutlet var serviceName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib:UINib {
              return UINib(nibName: identifier, bundle: nil)
          }
          static var identifier: String {
              return String(describing: self)
          }
      
    
}
