//
//  IncompleteCallsViewController.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/22/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class IncompleteCallsViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var contannerView: UIView!
    @IBOutlet var doctorInfoView: UIView!
    @IBOutlet var detailsView: UIView!
    @IBOutlet var doctorName: UILabel!
    @IBOutlet var DoctorMajor: UILabel!
    @IBOutlet var doctorAddress: UILabel!
    @IBOutlet var contannerSelectedView: UIView!
    
    @IBOutlet var chaValue: UILabel!
    @IBOutlet var chaLabel: UILabel!
    @IBOutlet var DateTimeLabel: UILabel!
    @IBOutlet var ByLabel: UILabel!
    @IBOutlet var channelLabel: UILabel!
    @IBOutlet var accLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var DateTimeValue: UILabel!
    @IBOutlet var ByValue: UILabel!
    @IBOutlet var channelValue: UILabel!
    @IBOutlet var accValue: UILabel!
    @IBOutlet var statusValue: UILabel!
    
    
    var selectedVisit:DoctorVisit?
    var incomplateCalles:InCalles?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        DateTimeLabel.text = "Start Time - End Time : "
        ByLabel.text = "By : "
        channelLabel.text = "Channel : "
        accLabel.text = "Accompanied By : "
        statusLabel.text = "Status : "
        chaLabel.text = "Cycle : "
        contannerSelectedView.isHidden = true
        SetTableView()
    }
    override func viewDidAppear(_ animated: Bool) {
        getIncomplateVisits()
    }
    
    func SetTableView()  {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(IncomplateVisitsTableViewCell.nib, forCellReuseIdentifier: IncomplateVisitsTableViewCell.identifier)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contannerView.roundedLeftTopBottom()
        
    }
    @IBAction func DeletePressed(_ sender: Any) {
        if selectedVisit != nil {
            
            Helper.alertWithTwoButton(message: "are you sure you want to delete?", headerTitle: "Alert", confirm: "Ok", cancel: "Cancel", mainColor: UIColor.mainColor) {
                self.deleteVisit(doctorID: "\(self.selectedVisit?.id ?? 0)")
            }
            
        }else{
            Helper.alertOneButton(message: "Please Select Visit", alertTitle: "Alert".localized, titleButton: "Ok")
        }
    }
    
    @IBAction func EditePressed(_ sender: Any) {
        if selectedVisit != nil{
            let storyboard = UIStoryboard(name: "IncompleteCalles", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "EditVisitViewControllerID") as! EditVisitViewController
            controller.selectedVisit = selectedVisit
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
        }else{
            Helper.alertOneButton(message: "Please Select Visit", alertTitle: "Alert".localized, titleButton: "Ok")
        }
    }
    
    
    @IBAction func FinishPressed(_ sender: Any) {
        Helper.alertWithTwoButton(message: "Are you sure you want to finish ?", headerTitle: "Alert".localized, confirm: "Yes", cancel: "Cancel", mainColor: .mainColor, completion: {
            if self.selectedVisit != nil {
                self.addvisit()
            }
        })
        
    }
    
    
    func deleteVisit(doctorID:String) {
        
        
        let parameters = [doctorID]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.DeleteDoctorVisit)
        let endPoint = RequestHandler.post(parmeters: [:], url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<DeleteVisit>) in
            
            guard let strongSelf = self else {return}
            Helper.hideIndicator(view: strongSelf.view)
            DispatchQueue.main.async {
                
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        Helper.alertOneButton(message: "wrongConnection".localized, alertTitle: "Alert", titleButton: "Ok")
                    }
                case .success(let value):
                    print(value)
                    strongSelf.removeSpinnerWithAction {
                        strongSelf.showMsgithAction(title: "Alert".localized, msg: "Remove Succssfuly", completion: {
                            strongSelf.getIncomplateVisits()
                        })
                    }
                }
            }
        }
    }
    
    func getIncomplateVisits() {
        
        
        let parameters = [""]
        //        Helper.showIndicator(view: self.view)
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetIncompleteCalls)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<InCalles>) in
            
            guard let strongSelf = self else {return}
            Helper.hideIndicator(view: strongSelf.view)
            DispatchQueue.main.async {
                
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        Helper.alertWithCompletion(message: "No data found", alertTitle: "Alert", titleButton: "Ok") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    
                    
                case .success(let value):
                    print(value)
                    strongSelf.incomplateCalles = value
                    strongSelf.tableView.reloadData()
                    if !(value.incompleteCalls?.isEmpty ?? false){
                        if !(value.incompleteCalls?[0].doctorVisits?.isEmpty ?? true) {
                            strongSelf.contannerSelectedView.isHidden = false
                            
                            strongSelf.selectSectionInTable(tableView: strongSelf.tableView, row: 0)
                            
                        }else{
                            strongSelf.contannerSelectedView.isHidden = true
                        }
                    }else{
                        Helper.alertWithCompletion(message: "There is no incompleted calls right now", alertTitle: "Alert", titleButton: "Ok") {
                            strongSelf.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }
            }
        }
    }
    
    
    func getStringDate(date:String) -> String? {
        let dateFormatter = DateFormatter()
        
        if date.contains(".") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        let dateT = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        //            return dateT
        
        if dateT == nil {
            let splited = date.components(separatedBy: "T")
            return splited[0]
        }else{
        return  dateFormatter.string(from: dateT ?? Date())
        }
    }
    
}


extension IncompleteCallsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let count = incomplateCalles?.incompleteCalls?.count ?? 0
              if count == 0 {
                  tableView.setEmptyView(title: "No Incomplete Calles Found", message: nil)
              }else{
                  tableView.restore()
              }
        return count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return incomplateCalles?.incompleteCalls?[section].doctorVisits?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: IncomplateVisitsTableViewCell.identifier) as! IncomplateVisitsTableViewCell
        
        cell.DoctorNameLabel.text = "Doctor Name : "
        cell.DoctorNameValue.text = "\(incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.nameLast ?? ""),\(incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.nameFirst ?? ""),\(incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.nameSecond ?? "")"
        cell.DoctorLocationLabel.text = "Location : "
        cell.DoctorLocationValue.text = "\(incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.city?.name ?? "")"
        
        cell.visitTimeLabel.text = "Visti Time : "
        var fullTime = "\(incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].fromDate ?? "")"
        if let dotRange = fullTime.range(of: ".") {
            fullTime.removeSubrange(dotRange.lowerBound..<fullTime.endIndex)
        }
        let fullTimeArr = fullTime.split(separator: "T")
        
        cell.visitTimeValue.text = "\(fullTimeArr[0]), \(fullTimeArr[1])"
        cell.doctorNumberLabel.text = "Doctor Number : "
        cell.doctorNumberValue.text = "\(incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.mobileNo ?? "")"
        let row = indexPath.row
        let count = incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?.count ?? 0
        if  row  == count - 1  {
            cell.contentView.roundedBottom()
            //            cell.contentView.clipsToBounds = true
            cell.underLineView.isHidden = true
        }else{
            cell.underLineView.isHidden = false
            
        }
        
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Helper.showIndicator(view: self.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
            self.contannerSelectedView.isHidden = false
            
            self.doctorName.text = "\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.nameLast ?? ""),\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.nameFirst ?? ""),\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.nameSecond ?? "")"
            self.DoctorMajor.text = "\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.major?.name ?? "")"
            self.doctorAddress.text = "\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].doctor?.city?.name ?? "")"
            
            if self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].accompaniedBy?.name ?? ""  == "" {
                self.accValue.text = "None"
            }else{
                self.accValue.text =  "\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].accompaniedBy?.name ?? "")"
            }
            self.DateTimeValue.text = "\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].fromDate ?? "") - \(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].toDate ?? "") "
            self.ByValue.text = "\(LongTermManager.getUserInfo()?.userData?.name ?? "")"
            self.channelValue.text = "\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].channel?.name ?? "")"
            self.statusValue.text = "\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].visitStatus?.name ?? "")"
            self.chaValue.text = "\(self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row].cycle?.name ?? "")"
            self.selectedVisit = self.incomplateCalles?.incompleteCalls?[indexPath.section].doctorVisits?[indexPath.row]
            Helper.hideIndicator(view: self.view)
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        headerView.backgroundColor = UIColor.IncomplateColor
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        let visitDate = incomplateCalles?.incompleteCalls?[section].visitDate ?? ""
        label.text = getStringDate(date: visitDate)
        label.font = UIFont(name: "Roboto-Bold", size: 17)
        label.textColor = .white // my custom colour
        label.textAlignment = .center
        
        headerView.roundedTop()
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
}


extension IncompleteCallsViewController{
    
    func addvisit() {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSZ"
        let now = df.string(from: Date())
        var parameters = [String:Any]()
        
        parameters["ID"] =  selectedVisit?.id
        parameters["DoctorID"] =  selectedVisit?.doctorID
        parameters["AccompaniedByID"] =  "1"
        parameters["FromDate"] =  "\(selectedVisit?.fromDate ?? "")"
        parameters["ToDate"] =  "\(selectedVisit?.toDate ?? "")"
        parameters["Address"] = selectedVisit?.doctor?.city?.name ?? ""
        parameters["VisitStatusID"] = "2"
        parameters["IsDeleted"] = false
        parameters["CycleID"] = selectedVisit?.cycleID
        parameters["ChannelID"] = selectedVisit?.channel?.id
        parameters["RepresentativeUserID"] = LongTermManager.getUserInfo()?.userData?.id
        parameters["ProductID"] = selectedVisit?.product?.id
        parameters["EntryUserID"] = LongTermManager.getUserInfo()?.userData?.id
        parameters["Note"] = selectedVisit?.note
        parameters["EntryDate"] = "\(now)"
        parameters["ModifyUserID"] = LongTermManager.getUserInfo()?.userData?.id
        parameters["ModifyDate"] = "\(now)"
        
        
        parameters["ID"] =  selectedVisit?.id
         parameters["DoctorID"] = selectedVisit?.doctorID
         
        if selectedVisit?.accompaniedBy?.id == -1 {
             parameters["AccompaniedByID"] =  ""

         }else{
             parameters["AccompaniedByID"] =  selectedVisit?.accompaniedBy?.id

         }
         
        
        
        Helper.showIndicator(view: self.view)
        let endPoint = RequestHandler.post(parmeters: parameters, url: ApiURLs.UpdateDoctorVisit)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<InsertVisit>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)

                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    
                    if res?.statusCode == 1 {
                        Helper.singOutCallApi()
                    }else if res?.statusCode == 3 {
                        Helper.alertWithCompletion(message: "Invalid Entered Date. \n The start date is greater OR equal than the end date , \n Please enter a valid date".localized, alertTitle: "Warning", titleButton: "Ok") {}
                    }else if res?.statusCode == 6 {
                        strongSelf.showMsg(title: "Alert".localized, msg: "There is already visit or user activity in this timer \n please update the visit date or visit time")
                        
                    }else{
                        strongSelf.removeSpinnerWithAction {
                            strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                        }
                    }
                case .success(let value):
                    print(value)
                    Helper.alertWithCompletion(message: "Done from adding visit", alertTitle: "Alert", titleButton: "Ok", completion: {
                        strongSelf.getIncomplateVisits()
                    })
                    
                }
            }
        }
    }
}
