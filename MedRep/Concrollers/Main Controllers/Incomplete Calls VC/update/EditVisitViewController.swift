//
//  EditVisitViewController.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/24/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import FAPickerView
import DateTimePicker

class EditVisitViewController: UIViewController {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var cahannelLabel: UILabel!
    @IBOutlet var accLabel: UILabel!
    @IBOutlet var cycleLabel: UILabel!
    @IBOutlet var saveAndStart: UIButton!
    @IBOutlet var timmerLabel: UILabel!
    @IBOutlet var dateView: UIView!
    @IBOutlet var timeView: UIView!
    @IBOutlet var channelView: UIView!
    @IBOutlet var accView: UIView!
    @IBOutlet var cycleView: UIView!
    @IBOutlet var noteView: UIView!
    @IBOutlet var noteTextView: UITextView!
    @IBOutlet var productLabel: UILabel!
    @IBOutlet var productView: UIView!
    
    var selectedVisit:DoctorVisit?
    
    
    var startDate = Date()
    var endDate = Date()
    
    var selectedTypeItem = FAPickerItem()
    var selectedTypeCycleItem = FAPickerItem()
    var selectedTypeCurrentItem = FAPickerItem()
    var selectedTypeProduct = FAPickerItem()
    
    var items = Array<FAPickerItem>()
    var startTimeForEachFile:Array<[String:Date]>!
    var endTimeForEachFile:Array<[String:Date]>!
    var timer = Timer()
    var intCounter = 0
    var countOfPages = 0
    var pages = [String]()
    var countForPage = 0
    let calendar = Calendar.current
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        Helper.addBorderView(view: productView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: dateView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: timeView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: channelView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: accView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: cycleView, borderWidth: 1, cornerRadius: 5)
        Helper.addBorderView(view: noteView, borderWidth: 1, cornerRadius: 5)
        let startArr = selectedVisit?.fromDate?.components(separatedBy: "T")
        let endArr = selectedVisit?.toDate?.components(separatedBy: "T")
        
        dateLabel.text = "Date: \(startArr?[0] ?? "") Time: \(startArr?[1] ?? "")"
        noteTextView.text = selectedVisit?.note
        
        if selectedVisit?.accompaniedBy?.name == nil || selectedVisit?.accompaniedBy?.name == "" {
            accLabel.text = "None"
        }else{
            accLabel.text = selectedVisit?.accompaniedBy?.name
        }
        
        cycleLabel.text = selectedVisit?.cycle?.name
        timmerLabel.text = "Date: \(endArr?[0] ?? "") Time: \(endArr?[1] ?? "")"
        productLabel.text = selectedVisit?.product?.name
        noteTextView.text = selectedVisit?.note
        cahannelLabel.text = selectedVisit?.channel?.name
        //        getAcc(fromStart: true)
        //        getChannel(fromStart: true)
        //        getCycle(fromStart: true)
    }
    
    
    
    
    
    //MARK:- Action && Selectoer
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        
        Helper.alertWithTwoButton(message: "Are you sure you want to cancel, the edited data? ", headerTitle: "Alert".localized, confirm: "ok", cancel: "cancel", mainColor: .mainColor, completion: {
            self.dismiss(animated: true, completion: nil)
            
        })
        
        
    }
    
    
    @IBAction func ProductPressed(_ sender: Any) {
        items.removeAll()
        getProduct()
    }
    func getProduct() {
        
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetProduct)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<Channel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    
                case .success(let value):
                    
                    if value.channeles?.isEmpty ?? true {
                        Helper.alertOneButton(message: "There is no data", alertTitle: "Alert".localized, titleButton: "ok")
                    }else{
                        for val in value.channeles ?? Channeles() {
                            strongSelf.items.append(FAPickerItem(id: "\(val.id ?? 0)", title: val.name))
                        }
                        
                        FAPickerView.setMainColor(UIColor.mainColor)
                        FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                          selectedItem: strongSelf.selectedTypeProduct,
                                                          filter: false,
                                                          headerTitle: "Select one item",
                                                          complete: { (item:FAPickerItem?) in
                                                            if let item = item {
                                                                strongSelf.selectedTypeProduct = item
                                                                strongSelf.productLabel.text = item.title
                                                            }
                        }, cancel: {
                            
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func SavePressed(_ sender: Any) {
        
        
        
        
        if dateLabel.text?.isEmpty ?? true {
            dateView.shakeView()
            return
        }
        if cahannelLabel.text?.isEmpty ?? true  {
            channelView.shakeView()
            return
        }
        if accLabel.text?.isEmpty ?? true  {
            accView.shakeView()
            return
        }
        if cycleLabel.text?.isEmpty ?? true  {
            cycleView.shakeView()
            return
        }
        
        
        Helper.alertWithTwoButton(message: "Are you sure you want to update visit ?", headerTitle: "Alert".localized, confirm: "Yes", cancel: "Cancel", mainColor: .mainColor, completion: {
            self.addvisit(callStatus: "2")
            
        })
        
        
    }
    
    
    
    @IBAction func selectDatePressed(_ sender: Any) {
        openDatePiker(tag: 1)
    }
    @IBAction func selectEndDate(_ sender: Any) {
        openDatePiker(tag: 2)
        
    }
    
    func openDatePiker(tag:Int){
        hideKeyboard()
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 1)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        //        let picker = DateTimePicker.create()
        picker.tag = tag
        //        picker.dateFormat = "hh:mm:ss aa dd/MM/YYYY"
        picker.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        picker.isDatePickerOnly = true
        picker.includesMonth = true
        picker.includesSecond = true
        picker.is12HourFormat = false
        //        picker.isTimePickerOnly = true
        picker.highlightColor = UIColor(red: 0.30, green: 0.52, blue: 1.00, alpha: 1.00)
        picker.doneButtonTitle = "!! Select Date !!"
        picker.doneBackgroundColor = UIColor(red: 0.30, green: 0.52, blue: 1.00, alpha: 1.00)
        picker.customFontSetting = DateTimePicker.CustomFontSetting(selectedDateLabelFont: .boldSystemFont(ofSize: 30))
        
        
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.title = formatter.string(from: date)
            picker.tag = tag
            
            if picker.tag == 1 {
                if let outDate = formatter.date(from: picker.selectedDateString) {
                    self.startDate = outDate
                    print(self.startDate)
                    self.dateLabel.text = "Date: \(self.startDate.year )-\(self.addZeroIf(number:self.startDate.month))-\(self.addZeroIf(number:self.startDate.day)) Time: \(self.addZeroIf(number:self.startDate.hour)):\(self.addZeroIf(number:self.startDate.minute))"
                    print(self.startDate)
                }
                
            }else if picker.tag == 2{
                if let outDate = formatter.date(from: picker.selectedDateString) {
                    self.endDate = outDate
                    print(self.endDate)
                    self.timmerLabel.text = "Date: \(self.endDate.year )-\(self.addZeroIf(number:self.endDate.month))-\(self.addZeroIf(number:self.endDate.day)) Time: \(self.addZeroIf(number:self.endDate.hour)):\(self.addZeroIf(number:self.endDate.minute))"
                    print(self.startDate)
                }
            }
        }
        picker.delegate = self
        picker.show()
    }
    
    
    
    
    
    @IBAction func selectChannel(_ sender: Any) {
        items.removeAll()
        getChannel(fromStart: false)
    }
    @IBAction func selectAccPressed(_ sender: Any) {
        items.removeAll()
        getAcc(fromStart: false)
    }
    
    
    @IBAction func selectCyclePressed(_ sender: Any) {
        items.removeAll()
        getCycle(fromStart: false)
    }
    
    
    
    //MARK:- Call Api's
    
    
    func getCycle(fromStart:Bool) {
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.Getcycle)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<Cycel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    
                case .success(let value):
                    if fromStart {
                        if !(value.cycles?.isEmpty ?? true) {
                            strongSelf.cycleLabel.text = value.cycles?[0].name
                        }
                        
                    }else{
                        
                        if value.cycles?.isEmpty ?? true {
                            Helper.alertOneButton(message: "There is no data", alertTitle: "Alert".localized, titleButton: "ok")
                        }else{
                            
                            for val in value.cycles ?? Cycles() {
                                strongSelf.items.append(FAPickerItem(id: "\(val.id ?? 0)", title: val.name))
                            }
                            
                            FAPickerView.setMainColor(UIColor.mainColor)
                            FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                              selectedItem: strongSelf.selectedTypeCycleItem,
                                                              filter: false,
                                                              headerTitle: "Select one item",
                                                              complete: { (item:FAPickerItem?) in
                                                                if let item = item {
                                                                    strongSelf.selectedTypeCycleItem = item
                                                                    strongSelf.cycleLabel.text = item.title
                                                                }
                            }, cancel: {
                                
                            })
                            
                        }}
                }
            }
        }
    }
    
    
    func getChannel(fromStart:Bool) {
        
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.Getchannel)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<Channel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    
                case .success(let value):
                    if fromStart {
                        if !(value.channeles?.isEmpty ?? true) {
                            strongSelf.cahannelLabel.text = value.channeles?[0].name
                        }
                        
                    }else{
                        if value.channeles?.isEmpty ?? true {
                            Helper.alertOneButton(message: "There is no data", alertTitle: "Alert".localized, titleButton: "ok")
                        }else{
                            for val in value.channeles ?? Channeles() {
                                strongSelf.items.append(FAPickerItem(id: "\(val.id ?? 0)", title: val.name))
                            }
                            
                            FAPickerView.setMainColor(UIColor.mainColor)
                            FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                              selectedItem: strongSelf.selectedTypeCurrentItem,
                                                              filter: false,
                                                              headerTitle: "Select one item",
                                                              complete: { (item:FAPickerItem?) in
                                                                if let item = item {
                                                                    strongSelf.selectedTypeCurrentItem = item
                                                                    strongSelf.cahannelLabel.text = item.title
                                                                }
                            }, cancel: {
                                
                            })
                        }
                    }
                }
            }
        }
    }
    
    func getAcc(fromStart:Bool) {
        
        
        let parameters = [""]
        Helper.showIndicator(view: self.view)
        let url = conectURL(parameters: parameters, url: ApiURLs.GetAccompaniedBy)
        let endPoint = RequestHandler.get(url: url)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<Channel>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    if res?.statusCode == 1{
                        Helper.singOutCallApi()
                        
                    }else{
                        strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    }
                    strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                    
                case .success(let value):
                    
                    if fromStart {
                        if !(value.channeles?.isEmpty ?? true) {
                            strongSelf.accLabel.text = value.channeles?[0].name
                        }
                        
                    }else{
                        if value.channeles?.isEmpty ?? true {
                            Helper.alertOneButton(message: "There is no data", alertTitle: "Alert".localized, titleButton: "ok")
                        }else{
                            for val in value.channeles ?? Channeles() {
                                strongSelf.items.append(FAPickerItem(id: "\(val.id ?? 0)", title: val.name))
                            }
                            
                            FAPickerView.setMainColor(UIColor.mainColor)
                            FAPickerView.showSingleSelectItem(items: NSMutableArray(array: strongSelf.items),
                                                              selectedItem: strongSelf.selectedTypeItem,
                                                              filter: false,
                                                              headerTitle: "Select one item",
                                                              complete: { (item:FAPickerItem?) in
                                                                if let item = item {
                                                                    strongSelf.selectedTypeItem = item
                                                                    strongSelf.accLabel.text = item.title
                                                                }
                            }, cancel: {
                                
                            })
                        }}
                }
            }
        }
    }
}
/*
 parameters["ID"] =  "0"
 parameters["DoctorID"] =  doctorInfo.id
 parameters["AccompaniedByID"] =  selectedTypeItem.id
 parameters["FromDate"] =  "\(selectedStartDate.year)-\(addZeroIf(number: selectedStartDate.month))-\(addZeroIf(number: selectedStartDate.day))T\(addZeroIf(number: selectedStartDate.hour)):\(addZeroIf(number: selectedStartDate.minute)):\(addZeroIf(number: calendar.component(.second, from: selectedStartDate)))"
 parameters["ToDate"] =  "\(endOfCallTime.year)-\(addZeroIf(number: endOfCallTime.month))-\(addZeroIf(number: endOfCallTime.day))T\(addZeroIf(number: endOfCallTime.hour)):\(addZeroIf(number: endOfCallTime.minute)):\(addZeroIf(number: calendar.component(.second, from: endOfCallTime)))"
 parameters["Address"] = doctorInfo.city?.name
 parameters["VisitStatusID"] = callStatus
 parameters["IsDeleted"] = false
 parameters["ChannelID"] = selectedTypeCurrentItem.id
 parameters["CycleID"] = selectedTypeCycleItem.id
 parameters["RepresentetaiveUserID"] = LongTermManager.getUserInfo()?.userData?.id
 parameters["ProductID"] = selectedTypeProduct.id
 parameters["EntryUserID"] = LongTermManager.getUserInfo()?.userData?.id
 parameters["EntryDate"] = "\(now)"
 parameters["ModifyUserID"] = "0"
 parameters["Note"] = noteTextView.text
 parameters["ModifyDate"] = "\(now)"
 */

extension EditVisitViewController{
    func addvisit(callStatus:String) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSZ"
        let now = df.string(from: Date())
        var parameters = [String:Any]()
        
        
        parameters["ID"] =  selectedVisit?.id
        parameters["DoctorID"] =  selectedVisit?.doctorID
        parameters["FromDate"] =  "\(startDate.year)-\(addZeroIf(number: startDate.month))-\(addZeroIf(number: startDate.day))T\(addZeroIf(number: startDate.hour)):\(addZeroIf(number: startDate.minute)):\(addZeroIf(number: calendar.component(.second, from: startDate)))"
        parameters["ToDate"] =  "\(endDate.year)-\(addZeroIf(number: endDate.month))-\(addZeroIf(number: endDate.day))T\(addZeroIf(number: endDate.hour)):\(addZeroIf(number: endDate.minute)):\(addZeroIf(number: calendar.component(.second, from: endDate)))"
        parameters["Address"] = selectedVisit?.address
        parameters["VisitStatusID"] = "1"
        parameters["IsDeleted"] = false
        parameters["RepresentativeUserID"] = LongTermManager.getUserInfo()?.userData?.id
        if selectedTypeCurrentItem.id == nil {
            parameters["ChannelID"] = selectedVisit?.channel?.id
            
        }else{
            parameters["ChannelID"] = selectedTypeCurrentItem.id
            
        }
        if selectedTypeItem.id == nil {
            parameters["AccompaniedByID"] =  selectedVisit?.accompaniedBy?.id
            
        }else{
            if selectedTypeItem.id == "-1" {
                parameters["AccompaniedByID"] =  ""
                
            }else{
                parameters["AccompaniedByID"] =  selectedTypeItem.id
                
            }
            
        }
        if selectedTypeCycleItem.id == nil{
            parameters["CycleID"] = selectedVisit?.cycleID
            
        }else{
            parameters["CycleID"] = selectedTypeCycleItem.id
            
        }
        if selectedTypeProduct.id == nil{
            parameters["ProductID"] = selectedVisit?.product?.id
            
        }else{
            parameters["ProductID"] = selectedTypeProduct.id
            
        }
        
        parameters["EntryUserID"] = LongTermManager.getUserInfo()?.userData?.id
        parameters["EntryDate"] = "\(now)"
        parameters["ModifyUserID"] = "0"
        parameters["Note"] = noteTextView.text
        parameters["ModifyDate"] = "\(now)"
        
        
        Helper.showIndicator(view: self.view)
        let endPoint = RequestHandler.post(parmeters: parameters, url: ApiURLs.UpdateDoctorVisit)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<InsertVisit>) in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                
                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                    
                case .failure( _):
                    
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                    
                    if res?.statusCode == 1 {
                        Helper.singOutCallApi()
                    }else if res?.statusCode == 3 {
                        Helper.alertWithCompletion(message: "Invalid Entered Date. \n The start date is greater OR equal than the end date , \n Please enter a valid date".localized, alertTitle: "Warning", titleButton: "Ok") {}
                    }else if res?.statusCode == 6 {
                        strongSelf.showMsg(title: "Alert".localized, msg: "There is already visit or user activity in this timer \n please update the visit date or visit time")
                        
                    }else{
                        strongSelf.removeSpinnerWithAction {
                            strongSelf.showMsg(title: "Alert".localized, msg: "wrongConnection".localized)
                        }
                    }
                case .success(let value):
                    print(value)
                    
                    strongSelf.timer.invalidate()
                    
                    Helper.alertWithCompletion(message: "Done From Adding call", alertTitle: "Alert", titleButton: "Ok") {
                        strongSelf.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
}



extension EditVisitViewController: DateTimePickerDelegate {
    
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if picker.tag == 1 {
            if let outDate = formatter.date(from: picker.selectedDateString) {
                self.startDate = outDate
                print(self.startDate)
                self.dateLabel.text = "\(self.startDate.year )-\(self.addZeroIf(number:self.startDate.month))-\(self.addZeroIf(number:self.startDate.day)) \(self.addZeroIf(number:self.startDate.hour)):\(self.addZeroIf(number:self.startDate.minute))"
                print(self.startDate)
            } }else if picker.tag == 2{
            if let outDate = formatter.date(from: picker.selectedDateString) {
                self.endDate = outDate
                print(self.endDate)
                self.timmerLabel.text = "\(self.endDate.year )-\(self.addZeroIf(number:self.endDate.month))-\(self.addZeroIf(number:self.endDate.day)) \(self.addZeroIf(number:self.endDate.hour)):\(self.addZeroIf(number:self.endDate.minute))"
                print(self.startDate)
            }
        }
    }
}
