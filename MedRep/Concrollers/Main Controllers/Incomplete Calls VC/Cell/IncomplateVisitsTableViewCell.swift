//
//  IncomplateVisitsTableViewCell.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/22/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit

class IncomplateVisitsTableViewCell: UITableViewCell {
    @IBOutlet var DoctorNameLabel: UILabel!
    @IBOutlet var DoctorLocationLabel: UILabel!
    @IBOutlet var visitTimeLabel: UILabel!
    @IBOutlet var doctorNumberLabel: UILabel!
    @IBOutlet var DoctorLocationValue: UILabel!
    @IBOutlet var visitTimeValue: UILabel!
    @IBOutlet var doctorNumberValue: UILabel!
    @IBOutlet var underLineView: UIView!
    
    @IBOutlet var DoctorNameValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib:UINib {
                 return UINib(nibName: identifier, bundle: nil)
             }
             static var identifier: String {
                 return String(describing: self)
             }
    
}
