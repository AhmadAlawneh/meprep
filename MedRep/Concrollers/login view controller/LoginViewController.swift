//
//  LoginViewController.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/5/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import MOLH
import Firebase

class LoginViewController: UIViewController {
    
    // MARK: - UI outlets
    @IBOutlet var passwordText: UITextField!
    @IBOutlet var userNameText: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNameText.attributedPlaceholder = NSAttributedString(string: "userName".localized,
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderColor ,NSAttributedString.Key.font :UIFont(name: "Roboto-Regular", size: 22)! ])
        
        passwordText.attributedPlaceholder = NSAttributedString(string: "password".localized,
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderColor ,NSAttributedString.Key.font :UIFont(name: "Roboto-Regular", size: 22)!])
        loginButton.setTitle("login".localized, for: .normal)
        userNameText.RTLSupport()
        passwordText.RTLSupport()
        loginButton.RTLSupport()
        
    }
    
    
    // MARK: - Login Button
    @IBAction func loginPressed(_ sender: Any) {
        
        if userNameText.text?.isEmptyOrWhitespace ?? true || passwordText.text?.isEmptyOrWhitespace ?? true {
            showMsg(title: "alert".localized, msg: "empty".localized)
        }else{
            login()
        }
    }
    
    // MARK: - Login Request
    func login()  {
        
      Helper.showIndicator(view: self.view)
//        Helper.showIndicator(view: self.view)
        //TODO: Add device Id (in Future Use)
        // let id = UIDevice.current.identifierForVendor?.uuidString
        
        var parameters = [String:Any]()
        
        
        parameters["Username"] = userNameText.text ?? ""
        parameters["Password"] = passwordText.text ?? ""
        parameters["DeviceID"] = "123"
        let token = Messaging.messaging().fcmToken

        if let token = Messaging.messaging().fcmToken {
            parameters["Token"] = token
            print(token)
        }
//        parameters["Token"] = "dCeSC1V9a2UJUpN9PVH-psr:APA91bGf6Lnsqe5RgQqXMlEBYSt39rW8XE5OKzuOiYY4ZUfwv-ztDvl4sfAhhDGhGu6x133tNrC2uRWpmbWhiLFfsTIrTEs-rpm3oLZ3bYsl9--9_3k4ygNm_gV-BOObUZDAXCzPdIM1"
        parameters["LanguageID"] = 1
        
        
        let endPoint = LoginEndPoint.login(headerParams: parameters)
        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<UserInfo>) in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
             Helper.hideIndicator(view: strongSelf.view)
//                Helper.hideIndicator(view: strongSelf.view)
                switch response.result {
                case .success(let value):
                    print(value)
                    LongTermManager.setUserInfo(userObject: value, key: Keys.userInfo)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let doctorsVc = storyboard.instantiateViewController(withIdentifier: "navbar")
                    doctorsVc.modalPresentationStyle = .fullScreen
                    strongSelf.present(doctorsVc, animated: true, completion: nil)

                case .failure(let error):
                    print(error.localizedDescription)
                    
                    let res =   try? JSONDecoder().decode(InvalidRequestHandler.self, from: response.data ?? Data())
                     if res?.statusCode == 1{
                       
                        Helper.alertOneButton(message: "wrongConnection".localized, alertTitle: "Alert", titleButton: "Ok")
                     }else if res?.statusCode == 5{
                        Helper.alertOneButton(message: "Wrong Username or Password", alertTitle: "Alert", titleButton: "Ok")

                     }else if res == nil{
                        Helper.alertOneButton(message: "wrongConnection".localized, alertTitle: "Alert", titleButton: "Ok")

                     }
                }
            }
        }
    }
}





//         = ["Username" : userNameText.text ?? "", "Password" : passwordText.text ?? "" , "DeviceID" : "123" , "Token":"dCeSC1V9a2UJUpN9PVH-psr:APA91bGf6Lnsqe5RgQqXMlEBYSt39rW8XE5OKzuOiYY4ZUfwv-ztDvl4sfAhhDGhGu6x133tNrC2uRWpmbWhiLFfsTIrTEs-rpm3oLZ3bYsl9--9_3k4ygNm_gV-BOObUZDAXCzPdIM1","LanguageID" :1]
//        let endPoint = RequestHandler.post(parmeters: parameters , url: ApiURLs.login)
//        APIHandler.request(endpoint: endPoint) { [weak self](response: Response<UserInfo>) in
//
//            guard let strongSelf = self else {return}
//            DispatchQueue.main.async {
//
//                switch response.result {
//
//                case .failure(let error):
//                    strongSelf.removeSpinnerWithAction {
//                        strongSelf.showMsg(title: "alert".localized, msg: "wrongConnection".localized)
//                        print(error.localizedDescription)
//
//                    }
//
//                case .success(let value):
//                    print(value)
//                    Helper.hideIndicator(view: strongSelf.view)
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let doctorsVc = storyboard.instantiateViewController(withIdentifier: "navbar")
//                    doctorsVc.modalPresentationStyle = .fullScreen
//                    strongSelf.present(doctorsVc, animated: true, completion: nil)
//                }
//            }
//        }
