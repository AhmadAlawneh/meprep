//
//  AppDelegate.swift
//  MedRep
//
//  Created by Yousef Alselawe on 9/5/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import UIKit
import MOLH
import FAFont
import IQKeyboardManager
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        
        MOLHFont.shared.arabic = UIFont(name: "Courier", size: 13)!
        MOLHLanguage.setDefaultLanguage("en")
        MOLH.shared.activate(true)
        MOLH.shared.specialKeyWords = ["Cancel","Done"]
        IQKeyboardManager.shared().isEnabled = true
        FirebaseApp.configure()
        if let ifLogin = LongTermManager.getUserInfo()?.isSuccess {
            if ifLogin == true {
                print(true)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "navbar")
                window?.rootViewController = viewController
            }
        }
        
//      UIFont.printFonts()
        UIFont.setBoldFontName("Roboto-Bold")
        UIFont.setRegularFontName("Roboto-Italic")
        UIFont.setItalicFontName("Roboto-Regular")
        UIFont.setLightFontName("Roboto-Light")
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.shadowImage = UIImage()
        navigationBarAppearace.isTranslucent = false
        navigationBarAppearace.tintColor = UIColor.white
        navigationBarAppearace.barTintColor = UIColor.mainColor
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]

        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}


extension AppDelegate: MOLHResetable {
    
    func reset() {
        guard let window = self.window else {return}
        DispatchQueue.main.async {
            let storyBoard = UIStoryboard(name: "Login", bundle: nil)
            let homeVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewControllerID")
            window.rootViewController = homeVC
        }
    }
}

