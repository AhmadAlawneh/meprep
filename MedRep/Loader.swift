//
//  Loader.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/7/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
import UIKit

var vSpinner : UIView?
 
extension UIViewController {
    func showIndicator(view:UIView) {
        let spinnerView = UIView.init(frame: self.view.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            self.view.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
    func removeSpinnerWithAction(completion: () -> ()) {
        DispatchQueue.main.async {
                   vSpinner?.removeFromSuperview()
                   vSpinner = nil
               }
        completion()
    }
}
