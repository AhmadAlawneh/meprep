//
//  Helper.swift
//  MedRep
//
//  Created by Ahmad Alawneh on 9/5/20.
//  Copyright © 2020 Yousef Alselawe. All rights reserved.
//

import Foundation
import UIKit
import MOLH
import MBProgressHUD
import FAPickerView



struct Helper {

    //MARK:- Show OR Hide Indicator
    
    static func showIndicator(view: UIView) {
        let hud:MBProgressHUD = MBProgressHUD.showAdded(to: view, animated: true)
//        hud.bezelView.color = UIColor.mainColor
        hud.animationType = .fade
        hud.detailsLabel.text = "loading ..."
        hud.detailsLabel.font = UIFont(name: "Roboto-Bold", size: 20)
        hud.backgroundView.color = UIColor.black.withAlphaComponent(0.5)
//        hud.mode = .determinate
        //annularDeterminate
    }
    
    static func alertWithTwoButton(message: String, headerTitle: String , confirm: String , cancel: String , mainColor:UIColor,completion: @escaping() -> ()){
        FAPickerView.setMainColor(mainColor)
        FAPickerView.showAlert(message: message,
                               headerTitle: headerTitle,
                               confirmTitle: confirm,
                               cancelTitle: cancel) { (button:FAPickerAlertButton) in
                                if button.rawValue == 1 {
                                    completion()

                                }else{
                                    print("cancel")
                                }
        }
    }
    
    static func hideIndicator(view: UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    static func addBorderView(view:UIView , borderWidth:CGFloat){
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = UIColor.mainColor.cgColor
    }
    
    static func addBorderView(view:UIView , borderWidth:CGFloat , cornerRadius:CGFloat){
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.cornerRadius = cornerRadius
        view.clipsToBounds = true
    }
    
    static func MakeRoundedView(view:UIView , borderWidth: CGFloat){
        view.layer.cornerRadius = view.frame.size.width/2
        view.clipsToBounds = true
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = borderWidth
    }
    
    //MARK:- Alerts Controllers
    
    static func alertOneButton(message:String,alertTitle:String,titleButton:String){
        FAPickerView.setMainColor(UIColor.mainColor)
        FAPickerView.showAlert(message: message,
                               headerTitle: alertTitle,
                               confirmTitle: titleButton) { (button:FAPickerAlertButton) in
                                
        }
    }
//    completion: @escaping() -> ()
    
    
    static func alertWithCompletion(message:String,alertTitle:String,titleButton:String ,completion: @escaping() -> ()){
        FAPickerView.setMainColor(UIColor.mainColor)
        FAPickerView.showAlert(message: message,
                               headerTitle: alertTitle,
                               confirmTitle: titleButton) { (button:FAPickerAlertButton) in
                               completion()
        }
    }
    
    //MARK:- To Make Corner Radius To Any View
    
    static func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
          let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
          let mask = CAShapeLayer()
          mask.path = path.cgPath
          view.layer.mask = mask
      }
    
    
    //MARK:- Push View Controller
    
    static func pushViewController(storyboardName:String , viewControllerId:String, strongSelf:UIViewController , isFrom:String?){
        let storyboardName = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboardName.instantiateViewController(withIdentifier: viewControllerId)
        strongSelf.navigationController?.pushViewController(vc, animated: true)
        print("***-- Push Is From \(isFrom ?? "") View Controller -----***" )
    }
    
    static var jwtToken = "dCeSC1V9a2UJUpN9PVH-psr:APA91bGf6Lnsqe5RgQqXMlEBYSt39rW8XE5OKzuOiYY4ZUfwv-ztDvl4sfAhhDGhGu6x133tNrC2uRWpmbWhiLFfsTIrTEs-rpm3oLZ3bYsl9--9_3k4ygNm_gV-BOObUZDAXCzPdIM1"
    
    
    
    
    
    static func singOutMedRepApp(){
        
        FAPickerView.setMainColor(UIColor.mainColor)
        FAPickerView.showAlert(message: "Are you sure about that",
                               headerTitle: "Alert",
                               confirmTitle: "Logout",
                               cancelTitle: "Cancel") { (button:FAPickerAlertButton) in
                                if button.rawValue == 1 {
                                    var rootVC : UIViewController?
                                    rootVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewControllerID")
                                    UserDefaults.standard.removeObject(forKey: Keys.userInfo)
                                    UserDefaults.standard.removeObject(forKey: Keys.jwtTokenFromLogin)
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = rootVC
                                }else{
                                    print("cancel")
                                }
        }
    }
    
    
    static func singOutCallApi(){
        
        FAPickerView.setMainColor(UIColor.mainColor)
        FAPickerView.showAlert(message: "your session expired please login again",
                               headerTitle: "Alert",
                               confirmTitle: "Ok") { (button:FAPickerAlertButton) in
                                var rootVC : UIViewController?
                                rootVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewControllerID")
                                UserDefaults.standard.removeObject(forKey: Keys.userInfo)
                                UserDefaults.standard.removeObject(forKey: Keys.jwtTokenFromLogin)
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = rootVC
        }
    }
}

extension UIViewController{
    
    func showMsg(title : String,msg : String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    func selectRowInTable(tableView:UITableView,row:Int){
        let indexPath = IndexPath(row:0 , section: row)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        tableView.delegate?.tableView!(tableView, didSelectRowAt: indexPath)
    }
    func selectSectionInTable(tableView:UITableView,row:Int){
          let indexPath = IndexPath(row:row , section: 0)
          tableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
          tableView.delegate?.tableView!(tableView, didSelectRowAt: indexPath)
      }
      
    
    func showMsgithAction(title : String,msg : String,completion: @escaping() -> ()) {
       let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
                 
                 alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {
                     UIAlertAction in
                     
                    completion()
                 }))
                 
                 self.present(alert, animated: true)

       }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
          if let data = text.data(using: .utf8) {
              do {
                  let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                  return json
              } catch {
                  print("Something went wrong")
              }
          }
          return nil
      }
}

extension UIColor{
    static let placeholderColor = UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0)
    static let borderViews = UIColor(red: 0.22, green: 0.68, blue: 0.91, alpha: 1.00)
    static let textColor = UIColor(red: 252.0/255.0, green: 252.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    static let calendarBackGround = UIColor(red: 242.0/255.0, green: 236.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let calendarTextColor = UIColor(red: 76.0/255.0, green: 132.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let calendarSelectedTextColor = UIColor(red: 255.0/255.0, green: 215.0/255.0, blue: 20.0/255.0, alpha: 1.0)
    static let IncomplateColor = UIColor(red: 0.44, green: 0.56, blue: 0.88, alpha: 1.00)

}



extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
extension String {
    
    var isEmptyOrWhitespace: Bool {
        if(self.isEmpty) {
            return true
        }
        return (self.trimmingCharacters(in: NSCharacterSet.whitespaces) == "")
    }
}


extension UITextField{
    func RTLSupport()  {
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.textAlignment = .left
        }else{
            self.textAlignment = .right
        }
    }
}

extension UITextView{
    func RTLSupport()  {
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.textAlignment = .left
        }else{
            self.textAlignment = .right
        }
    }
}

extension UILabel{
    func RTLSupport()  {
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.textAlignment = .left
        }else{
            self.textAlignment = .right
        }
    }
}

extension UIButton{
    func RTLSupport()  {
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.titleLabel?.textAlignment = .left
        }else{
            self.titleLabel?.textAlignment = .right
        }
    }
}


extension UIView {
    
    // MARK:- Shaking effect to display some errors
    
    func shakeView() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}


//MARK:- Dismiss KeyBoard

extension UIViewController {
    
    func dismissKey(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer( target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
}

extension UITableView {
    
    func setEmptyView(title: String, message: String?) {
        
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "Roboto-Bold", size: 20)
        emptyView.addSubview(titleLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        titleLabel.text = title
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
    
}


extension UICollectionView {
    
    
    func setEmptyView(title: String, message: String) {
        
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.borderViews
        titleLabel.font = UIFont(name: "OpenSans-Bold", size: 17)
        emptyView.addSubview(titleLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        titleLabel.text = title
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        self.backgroundView = emptyView
    }
    
    func restore() {
        self.backgroundView = nil
    }
    
}

extension UIView {
func roundedLeftTopBottom(){
    self.clipsToBounds = true
    let maskPath1 = UIBezierPath(roundedRect: bounds,
                                 byRoundingCorners: [.topRight , .bottomRight],
                                 cornerRadii: CGSize(width: 10 , height:10 ))
    let maskLayer1 = CAShapeLayer()
    maskLayer1.frame = bounds
    maskLayer1.path = maskPath1.cgPath
    layer.mask = maskLayer1
}
    func roundedTop(){
        self.clipsToBounds = true
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft , .topRight],
                                     cornerRadii: CGSize(width: 10 , height:10 ))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    
    func roundedBottom(){
          self.clipsToBounds = true
          let maskPath1 = UIBezierPath(roundedRect: bounds,
                                       byRoundingCorners: [.bottomLeft , .bottomRight],
                                       cornerRadii: CGSize(width: 10 , height:10 ))
          let maskLayer1 = CAShapeLayer()
          maskLayer1.frame = bounds
          maskLayer1.path = maskPath1.cgPath
          layer.mask = maskLayer1
      }
}

extension UIColor {
    static func hexStringToColor(hex: String) -> UIColor {
        var cString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count != 6 {
            return .gray
        }
        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                       green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                       blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                       alpha: 1.0)
    }
}



extension UIViewController{
    func conectURL(parameters:[String],url:String) -> String {
         var concatenateURL = ""
         for value in parameters {
             concatenateURL = concatenateURL + value + "/"
         }
         concatenateURL.removeLast()
         let fullURL = url + concatenateURL
         
         return fullURL
     }
}

extension UIViewController{
    func addZeroIf(number:Int) -> String {
      var str = "\(number)"
      if (number < 10) {
        str = "0\(number)"
        return str
      }else{
        return str
        }
    }
}
//"yyyy-MM-dd'T'HH:mm:ss.SSS"
extension Date{
    var day: Int {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("dd")
        return Int(df.string(from: self)) ?? 0

    }
    var month: Int {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MM")
        return Int(df.string(from: self)) ?? 0
    }
    var year: Int {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("yyyy")
        return Int(df.string(from: self)) ?? 0
    }
    var hour: Int {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("HH")
        return Int(df.string(from: self)) ?? 0
    }
    var minute: Int {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("mm")
        return Int(df.string(from: self)) ?? 0
    }
    var second: Int {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("ss")
        return Int(df.string(from: self)) ?? 0
    }
    
}
